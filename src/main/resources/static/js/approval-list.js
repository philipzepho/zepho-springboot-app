
var infoList = [];
var userRole = "";

$(document).ready(function() {


	   var app = angular.module('approvalList', []);

	       app.controller('ApprovalListController',
	       				['$scope',
	       				'$window',
	       				'$location',
	       				'$http', 
	       				function($scope,$window, $location, $http) {
	       					
	       					
	       					$scope.approve=function(id,index){
	       						console.log(id);
	       						
	       						/*userInfo={
	       								"userUid":id
	       						}*/
	       						userInfo = infoList[id];	       						
	       						
	       						if(userRole == "Admin"){
	       							userInfo.approvedByAdmin = true;
	       						}
	       						if(userRole == "SuperAdmin"){
	       							userInfo.approvedBySuAdmin = true;
	       						}	       						
	       						
	       						var info = encodeData(JSON.stringify(userInfo));
	       						
	       						var response = $http.post(baseUrl+approvalListEndPoint, info, headConfig);
	       						response.success(function(data, status,
										headers, config) {
	       							data = decodeData(data);
	       							if(data.Success){
	       								var itemHtml = $(document.getElementById('add_approval_list'));
	       								$(itemHtml).empty();
	       								$scope.init();
	       								/*if(userInfo.approvedByAdmin == true && userInfo.approvedBySuAdmin == true){
	       									$('#'+index+'').hide();
	       									$('#infoTable'+index+'').hide();
	       								}
	       								if(userInfo.approvedByAdmin == false){
	       									var testDivContainer = $(document.getElementById('status'+index));
	       									$(testDivContainer).empty();
	       									var appendData = '<span class="pull-right">';	       									
	       									appendData+=' Waiting for Admin\'s Approval';
	       									appendData+=' </span>';
	       									$(testDivContainer).append(appendData);
	       								}
	       								if(userInfo.approvedBySuAdmin == false){
	       									var testDivContainer = $(document.getElementById('status'+index));
	       									$(testDivContainer).empty();
	       									var appendData = '<span class="pull-right">';	       									
	       									appendData+=' Waiting for Super Admin\'s Approval';
	       									appendData+=' </span>';
	       									$(testDivContainer).append(appendData);
	       								}*/
	       								//
	       							}

									console.log(data);
								});
	       						
								response.error(function(data, status,
										headers, config) {
									console.log(data);
								});
	       						
	       						
	       					}
	       					$scope.reject=function(id,index){
	       						console.log(id);
	       						
	       						userInfo={
	       								"userUid":id
	       						}
	       						var info = encodeData(JSON.stringify(userInfo));
	       						
	       						var response = $http.post(baseUrl+approvalServiceRejectEndPoint, info, headConfig);
	       						response.success(function(data, status,
										headers, config) {
	       							data = decodeData(data);
	       							if(data.Success){
	       								$('#'+index+'').hide();
	       								$('#infoTable'+index+'').hide();
	       							}
								});
	       						
								response.error(function(data, status,
										headers, config) {
									console.log(data);
								});
	       						
	       						
	       					}	
	       					$scope.init=function(){
	       						
	       						console.log(JSON.parse(localStorage.getItem("Zepho_User")));
	       						var organizationInfo={
	       								"organization":JSON.parse(localStorage.getItem("Zepho_User")).organization,
	       								"role":JSON.parse(localStorage.getItem("Zepho_User")).role,
	       						        "userUid":JSON.parse(localStorage.getItem("Zepho_User")).userUid
	       						}
	       						
	       						userRole = JSON.parse(localStorage.getItem("Zepho_User")).role;	       						
	       						
	       			
	       						var info = encodeData(JSON.stringify(organizationInfo));
	       						var response = $http.post(baseUrl+approvalServiceEndPoint,info, headConfig);
	       					        	   
	       						
	       						response.success(function(data, status,
	   									headers, config) {
	       							data = decodeData(data);
	       							console.log(data);
	   							var arrLen=data.length;
	       				        var itemHtml = document.getElementById('add_approval_list');

	       				        for(let i = 0; i <arrLen; i++) {
	       				        	var info = {
	       							        approvedByAdmin:data[i].approvedByAdmin,
	       							        approvedBySuAdmin:data[i].approvedBySuAdmin,
	       							        userUid:data[i].userUid,
	       							        role: data[i].role
	       							    }
	       							infoList[data[i].userUid] = info;
	       				        	
	       				            var data_for="item"+i;
	       				            var requestId=i;
	       				        var item='<div class="row user-row " id="'+requestId+'">'
	       				        item+='<div class="col-xs-3 col-sm-2 col-md-1 col-lg-1">';
	       				        item+= '<img class="img-circle" src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=50"  alt="User Pic">';
	       				        item+= '</div>';
	       				        item+= '<div class="col-xs-8 col-sm-9 col-md-10 col-lg-10">';
	       				        item+= '<div class="list-name" id="list-name"> <strong>Name: '+ data[i].fullName+'</strong></div><br>';
	       				        item+= ' <div class="text-muted" name="role">Role: '+ data[i].role+'</div>';
	       				        item+=  '</div>';
	       				        item+= '<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 dropdown-user" data-for=".'+data_for+'">';
	       				        item+=' <i class="glyphicon glyphicon-chevron-down text-muted"></i>';
	       				        item+= ' </div>';
	       				        item+= ' </div>';
	       				        item+= ' <div class="row user-infos '+data_for+'" id="infoTable'+requestId+'">';
	       				        item+= ' <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10 col-xs-offset-0 col-sm-offset-0 col-md-offset-1 col-lg-offset-1">';
	       				        item+= ' <div class="panel panel-primary">';
	       				        item+=' <div class="panel-heading">';
	       				        item+=' <h3 class="panel-title">User information</h3>';
	       				        item+=' </div>';
	       				        item+=' <div class="panel-body">';
	       				        item+=' <div class="row">';
	       				        item+=' <div class="col-md-3 col-lg-3 hidden-xs hidden-sm">';
	       				        item+=' <img class="img-circle" src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=100" alt="User Pic"> ';
	       				        item+=' </div>';
	       				        item+=' <div class="col-xs-2 col-sm-2 hidden-md hidden-lg">';
	       				        item+=' <img class="img-circle" src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=50" alt="User Pic">';
	       				        item+=' </div>';
	       				       
	       				        item+=' <div class=" col-md-9 col-lg-9 hidden-xs hidden-sm">';
	       				        //item+=' <strong>Name</strong><br>';
	       				        item+=' <table class="table table-user-information">';
	       				        item+=' <tbody>';
	       				     item+=' <tr>';
		     			        item+=' <td>Father Name:</td>';
		     			        item+=' <td>'+ data[i].fatherName+'</td>';
		     			        item+=' </tr>';
		     			        
		     			       item+=' <tr>';
		     			        item+=' <td>Date of Birth:</td>';
		     			        item+=' <td>'+ data[i].dobString+'</td>';
		     			        item+=' </tr>';  
		     			        
	       				     item+=' <tr>';
	     			        item+=' <td>Email :</td>';
	     			        item+=' <td>'+ data[i].email+'</td>';
	     			        item+=' </tr>';
	     			        item+=' <tr>';
	     			        item+=' <td>Contact Number:</td>';
	     			        item+=' <td>'+ data[i].mobileNum+'</td>';
	     			        item+=' </tr>';
	     			        	     			        
	     			       item+=' <tr>';
	     			        item+=' <td>Address:</td>';
	     			        item+=' <td>'+ data[i].address+'</td>';
	     			        item+=' </tr>';
	     			        
	     			       item+=' <tr>';
	     			        item+=' <td>City:</td>';
	     			        item+=' <td>'+ data[i].city+'</td>';
	     			        item+=' </tr>';
	     			        
	     			       item+=' <tr>';
	     			        item+=' <td>District:</td>';
	     			        item+=' <td>'+ data[i].district+'</td>';
	     			        item+=' </tr>';
	     			        
	     			       item+=' <tr>';
	     			        item+=' <td>State:</td>';
	     			        item+=' <td>'+ data[i].state+'</td>';
	     			        item+=' </tr>';
	     			        
	     			        item+=' <tr>';
	     			        item+=' <td>Added By</td>';
	     			        item+=' <td>'+ data[i].addedBy+'</td>';
	     			        item+=' </tr>';
	     			        
	     			        item+=' <tr>';
	     			        item+=' <td>Role Opted</td>';
	     			        item+=' <td>'+ data[i].role+'</td>';
	     			        item+=' </tr>';
	     			        
	     			        //update role
	     			        item+=' <tr>';
	     			        item+=' <td>Alter Role</td>';
	     			        item+=' <td><select id="updateRole'+i+'" class="form-control">';
	     			        item+=' <option value="">No Change</option>';
	     			        item+=' <option value="Missionary">Missionary</option>';
	     			        item+=' <option value="Rescuer">Rescuer</option>';
	     			        item+=' <option value="Public">Public</option>';
	     			        item+=' <option value="ChurchMember">ChurchMember</option>';
	     			        item+='</select></td>';
	     			        item+=' </tr>';
	     			        
	     			       item+=' <tr>';
	     			        item+=' <td>Aadhaar Numar:</td>';
	     			        item+=' <td>'+ data[i].aadhaarNumber+'</td>';
	     			        item+=' </tr>';
	     			        
	     			       item+=' <tr>';
	     			        item+=' <td>Church Name:</td>';
	     			        item+=' <td>'+ data[i].nameOfChurch+'</td>';
	     			        item+=' </tr>';
	     			        
	     			       item+=' <tr>';
	     			        item+=' <td>Pastor Name:</td>';
	     			        item+=' <td>'+ data[i].pastorName+'</td>';
	     			        item+=' </tr>';
	     			        
	     			       item+=' <tr>';
	     			        item+=' <td>Pastor Contact Number:</td>';
	     			        item+=' <td>'+ data[i].pastorOrgContact+'</td>';
	     			        item+=' </tr>';
	     			        
	     			       item+=' <tr>';
	     			        item+=' <td>Member since:</td>';
	     			        item+=' <td>'+ data[i].memberSince+'</td>';
	     			        item+=' </tr>';
	     			        
	       				        item+=' </tbody>';
	       				        item+=' </table>';
	       				        item+=' </div>';
	       				        item+=' </div>';
	       				        item+=' </div>';
	       				    
	       				  		item+=' <div class="panel-footer" id="status'+i+'">';
	       				          
	       				          
	       				          var userUID=data[i].userUid;
	       				          
	       				          if((userRole == "Admin" && data[i].approvedByAdmin == false) ||
	       				        		(userRole == "SuperAdmin" && data[i].approvedBySuAdmin == false)){
		       				        	item+=' <button class="btn btn-sm" type="button"  data-original-title="Send message to user"><i class="glyphicon glyphicon-circle-arrow-right"></i></button>';
			       				        item+=' <span class="pull-right">';
			       				        item+=' <button class="btn btn-sm btn-success" type="button" data-toggle="tooltip" id="'+userUID+'" onClick="approve(this.id,'+i+')" data-original-title="Edit this user"><i class="glyphicon glyphicon-ok-sign"></i>&nbsp;&nbsp;Approve</button>';
			       				        item+=' <button class="btn btn-sm btn-danger" type="button" data-toggle="tooltip" id="'+userUID+'" onClick="reject(this.id,'+i+')" data-original-title="Remove this user"><i class="glyphicon glyphicon-remove-sign"></i>&nbsp;&nbsp;Reject</button>';
			       				        item+=' </span>';
	       				          }
	       				      
	       				          if(userRole == "Admin" && data[i].approvedByAdmin == true){
	       				        	item+=' <span class="pull-right">';
	       				        	item+=' Waiting for Super Admin\'s Approval';
	       				        	item+=' </span>';
	       				          }
	       				          if(userRole == "SuperAdmin" && data[i].approvedBySuAdmin == true){
	       				        	item+=' <span class="pull-right">';
	       				        	item+=' Waiting for Admin\'s Approval';
	       				        	item+=' </span>';
	       				          }
	       				          
	       				          item+=' </div>';
	       				        item+=' </div>';
	       				        item+=' </div>';
	       				        item+=' </div>';
	       				     
	       				        
	       				     	
	       				      
	       				        itemHtml.innerHTML+=item;
	       				        
	       				       
	       				        
	       				    }
	       				        var panels = $('.user-infos');
	       				        
	       				        panels.hide();
	       				        var panelsButton = $('.dropdown-user');
	       				        //Click dropdown
	       				        panelsButton.click(function() {
	       				            //get data-for attribute
	       				            var dataFor = $(this).attr('data-for');
	       				            var idFor = $(dataFor);

	       				            //current button
	       				            var currentButton = $(this);
	       				            idFor.slideToggle(400, function() {
	       				                //Completed slidetoggle
	       				                if(idFor.is(':visible'))
	       				                {
	       				                    currentButton.html('<i class="glyphicon glyphicon-chevron-up text-muted"></i>');
	       				                }
	       				                else
	       				                {
	       				                    currentButton.html('<i class="glyphicon glyphicon-chevron-down text-muted"></i>');
	       				                }
	       				            })
	       				        });

	       						});
	       						
	       						response.error(function(data, status,
	   									headers, config) {
	   								alert("Exception details: "
	   										+ JSON.stringify({
	   											data : data
	   										}));
	   							});
	       					}
	       				}			
	       			
	       							
	       				
	       							

	       							
	       						 ]);

	    
	       

	       
	
	
	 
    var panels = $('.user-infos');
    var panelsButton = $('.dropdown-user');
    panels.hide();

    //Click dropdown
    panelsButton.click(function() {
        //get data-for attribute
        var dataFor = $(this).attr('data-for');
        var idFor = $(dataFor);

        //current button
        var currentButton = $(this);
        idFor.slideToggle(400, function() {
            //Completed slidetoggle
            if(idFor.is(':visible'))
            {
                currentButton.html('<i class="glyphicon glyphicon-chevron-up text-muted"></i>');
            }
            else
            {
                currentButton.html('<i class="glyphicon glyphicon-chevron-down text-muted"></i>');
            }
        })
    });


    $('[data-toggle="tooltip"]').tooltip();

    $('button').click(function(e) {
        e.preventDefault();
        alert("This is a demo.\n :-)");
    });
    
  
});


function approve(clicked_id,index){
	console.log(index);
	console.log("Approve Clicked");
		
		var alterRole = document.getElementById('updateRole'+index).value;
		if(alterRole != ""){
			infoList[clicked_id].role = alterRole;
		}	
	angular.element('#ApprovalListController').scope().$apply();
	angular.element('#ApprovalListController').scope().approve(clicked_id,index);
	
	//console.log(clicked_id);
}

function reject(clicked_id,index){
	console.log("Reject Clicked");
	angular.element('#ApprovalListController').scope().$apply();
	angular.element('#ApprovalListController').scope().reject(clicked_id,index);
	
	//console.log(clicked_id);
}