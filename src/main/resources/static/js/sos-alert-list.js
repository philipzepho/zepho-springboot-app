   
$(document).ready(function() {
	
	 var app = angular.module('sosAlertList', []);

     app.controller('SosAlertListController',
     				['$scope',
     				'$window',
     				'$location',
     				'$http', 
     				function($scope,$window, $location, $http) {
     					
     					
     					
     					$scope.clearSosAlert=function(uuid,index){
     						console.log(uuid);
     						
     						sosAlertItemInfo={
     								"userUid":JSON.parse(localStorage.getItem("Zepho_User")).userUid,
     								"uuid":uuid
     						}
     						var info = encodeData(JSON.stringify(sosAlertItemInfo));
     						var response = $http.post(baseUrl+sosWatchedEndPoint,info, headConfig);
     						response.success(function(data, status,
									headers, config) {
     							data = decodeData(data);
     							if(data.Success=="true"){
     								console.log("TODO://");
     								$('#'+index+'').hide();
     							}
     							
							});
     						
							response.error(function(data, status,
									headers, config) {
								console.log(data);
							});
     						
     						
     					}	
     					$scope.init=function(){
     						
     					
     						var userInfo={
     								"userUid":JSON.parse(localStorage.getItem("Zepho_User")).userUid
     						}
     			
     						var info = encodeData(JSON.stringify(userInfo));
     						var response = $http.post(baseUrl+sosAlertListServiceEndPoint,info, headConfig);
     					        	   
     						
     						response.success(function(data, status,
 									headers, config) {

 									
     							data = decodeData(data);
     						var sosAlertList=data['sosAlertList'];
     						console.log(sosAlertList);
 							var arrLen=sosAlertList.length;
     				        
     				        console.log(arrLen);
     				  var itemHtml = document.getElementById('sos-alert-list');
     				  for(let i = 0; i <arrLen; i++) {
     			        var collapse="collapseItem"+i;
     			        var item='  <div class="panel panel-default widget" id="'+i+'">';
     			        item+=' <div class="panel-heading">';
     			        item+=' <i class="fa fa-bell" aria-hidden="true"></i>';
     			        item+=' <h3 class="panel-title">';
     			        item+=' SOS Alerts</h3>';
     			        let uuid=sosAlertList[i].uuid;
     			      
     			        item+='<button class="btn btn-sm btn-danger" type="button" id="'+uuid+'" onClick="clearSosItem(this.id,'+i+')"  style="float:right;margin-top:-0.3em;"><span class="glyphicon glyphicon-remove"></span></i></button>';
     			        item+=' </div>';
     			        item+=' <div class="panel-body">';
     			        item+=' <ul class="list-group">';
     			        item+=' <li class="list-group-item">';
     			        item+=' <div class="row">';
     			        item+=' <div class="col-xs-2 col-md-1">';
     			        item+=' <img src="assets/images/sos-user.png" class="img-circle img-responsive" alt="" />';
     			        item+=' </div>';
     			        item+=' <div class="col-xs-10 col-md-11">';
     			        item+=' <div class="user-name">';                             
     			        item+=' Name: '+sosAlertList[i].fromUser+'';
     			        item+=' </div>';
     			        item+=' <div class="mic-info">';
     			        item+=' Time <span class="time-date">'+ new Date(sosAlertList[i].createdAt)+'</span>';
     			        item+=' </div>';
     			        item+=' <div class="comment-text">';
     			        item+=' <div class="accordion" id="accordion2">';
     			        item+=' <div class="accordion-group">';
     			        item+=' <div class="accordion-heading">';
     			        item+=' <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#'+collapse+'">';
     			        item+=' <button class="btn btn-primary" onclick="myFunction('+sosAlertList[i].latitude+','+sosAlertList[i].longitude+')">Show Map</button>';
     			        item+=' </a>';
     			        item+=' </div>';
     			        item+=' <div id="'+collapse+'" class="accordion-body collapse">';
     			        item+=' <div class="accordion-inner">';
//     			        item+=' <center>';
//     			        item+=' <div id="'+gmap+'">';
//     			        item+=' </div>';
//     			        item+=' </center>';  
     			        item+=' </div>';
     			        item+=' </div>';
     			        item+=' </div>';
     			        item+=' </div>';
     			        item+=' </div>';
     			        item+=' </div>';
     			        item+=' </div>';
     			        item+=' </div>';
     			        item+=' </li>';
     			        item+=' </ul>';
     			        item+=' </div>';
     			      
     			        itemHtml.innerHTML+=item;
     			         }
     						});
     			     

     				        
     				       
     						response.error(function(data, status,
 									headers, config) {
 								alert("Exception details: "
 										+ JSON.stringify({
 											data : data
 										}));
 							});
     					}
     				}			
		
     		]);

  
	
});


function clearSosItem(input,index){
     	console.log(input);
     	angular.element('#SosAlertListController').scope().$apply();
     	angular.element('#SosAlertListController').scope().clearSosAlert(input,index);	

     }


// gogle map--window load --/
function myFunction(lat,lng) {
	console.log(lat+" :: "+lng);
	localStorage.setItem("Zepho_lat",lat);
	localStorage.setItem("Zepho_lng",lng);
var myWindow = window.open("alertMap", "", "width=800,height=550");
  }


