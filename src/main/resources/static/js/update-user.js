var users;    
var rescuers;
var clickedUserUid;
var userRole;
$(document).ready(function() {

	var app = angular.module('updateUser', []);

    app.controller('updateUserController',
    				['$scope',
    				'$window',
    				'$location',
    				'$http', 
    						function($scope, $window, $location, $http) {
    							//console.log("Load Users");

    							
    							$scope.init=function(){
    								var org=JSON.parse(localStorage.getItem("Zepho_User")).organization
    								var info = encodeData(org);
    								var response = $http.post(baseUrl+'/getRescuerList',info, headConfig);
    								response.success(function(data, status,
    										headers, config) {
    									data = decodeData(data);
    									rescuers=data.RescuerList;
    									console.log(rescuers);
    									autoCompleteRescuers(rescuers);
    									
    										

    								});
    								response.error(function(data, status,
    										headers, config) {
    									console.log("Exception details: "
    											+ JSON.stringify({
    												data : data
    											}));
    								});
    	       						
    		       					
    	       						var formData={
        									"userUid":JSON.parse(localStorage.getItem("Zepho_User")).userUid,
        									"role":JSON.parse(localStorage.getItem("Zepho_User")).role,
        									"organization":JSON.parse(localStorage.getItem("Zepho_User")).organization,
        							}
    	       			
    	       						console.log(formData);
    	       						var info = encodeData(JSON.stringify(formData));
    	       						var response = $http.post(baseUrl+'/GetUsersList',info, headConfig);
    	       					        	   
    	       						
    	       						response.success(function(data, status,
    	   									headers, config) {

    	       							data = decodeData(data);
    	       							
    	   							var arrLen=data.UsersList.length;
    	   							
    	   							users=data.UsersList;
    	   							
    	   							
    	   						 var itemHtml = document.getElementById('myUL');
    	   				         for(let i = 0; i < arrLen; i++) {
    	   				              
    	   				               
    	   				          
    	   				          	var userUid=users[i].userUid;
    	   				              var item=' <li class="list-group-item" id="'+userUid+'" onClick="loadFormData(this.id,'+i+')" data-toggle="modal" data-target="#edit_view">';
    	   				              item+=' <label>'+users[i].fullName+'</label>';
    	   				              item+=' <div class="pull-right action-buttons" >';
    	   				              item+=' <a href="#"><span class="glyphicon glyphicon-pencil"></span></a>';
    	   				              item+=' </div>';
    	   				              item+=' </li>';
    	   				              
    	   				    
    	   				        itemHtml.innerHTML+=item;
    	   				         } 
    	       				        
    	       				    })
    	       				       
    	       						response.error(function(data, status,
    	   									headers, config) {
    	   								alert("Exception details: "
    	   										+ JSON.stringify({
    	   											data : data
    	   										}));
    	   							});
    	       					
    							}
    							$scope.updateUser= function(formData){
    								console.log("Inside UpdateUser");
    								
    								var info = encodeData(JSON.stringify(formData));
    								var response = $http.post(baseUrl+'/updateUser',
    										info, headConfig);
    								response.success(function(data, status,
    										headers, config) {

    										data = decodeData(data);
											alert(data.info);
											var modal = document.getElementById('edit_view');
										
											//TODO:
											//Close the model and refresh the page because
											if(data.info=="Successfully updated"){
												window.location.replace(window.location.href);
											}
											
											
											
    										

    								});
    								response.error(function(data, status,
    										headers, config) {
    									console.log("Exception details: "
    											+ JSON.stringify({
    												data : data
    											}));
    									alert("Error occured while updating user.");
    								});
    							}
    							
    							
    							
    							
    							}
    						 ]);
    
    $('[data-toggle="offcanvas"]').click(function(){
        $("#navigation").toggleClass("hidden-xs");
    });
    
 
});

function loadFormData(userUid,index){
	
	clickedUserUid=userUid;
	
	userRole=users[index].role;
	
	console.log(users[index]);
	
	$('#passDiv').hide();
	$("#new_password").val("");
	$("#confirm_password").val("");
     $("#userEmail").val(users[index].email);
     $("#userMobileNumber").val(users[index].mobileNum);
     $("#userEmergencyNumber").val(users[index].emergencyNum);
     if(!users[index].approved){
    	 $("#disableUser").prop('checked', true); 
     }      
     
     
     if(users[index].rescuer1!=""){
    	 
    	 $("#rescuer1").val ((filterIt(rescuers,users[index].rescuer1)[0].label));
    	 //$("#rescuer1").val('TEST');
    	 rescuer1=users[index].rescuer1;
     }else{
    	 $("#rescuer1").val ("");
     }
     
     if(users[index].rescuer2!=""){
    	 
    	 $("#rescuer2").val ((filterIt(rescuers,users[index].rescuer2)[0].label));
    	 rescuer2=users[index].rescuer2;
     }else{
    	 $("#rescuer2").val ("");
     }
     
     if(users[index].rescuer3!=""){
    	 $("#rescuer3").val ((filterIt(rescuers,users[index].rescuer3)[0].label));
    	 rescuer3=users[index].rescuer3;
     }else{
    	 $("#rescuer3").val ("");
     }
     if(users[index].rescuer4!=""){
    	 $("#rescuer4").val ((filterIt(rescuers,users[index].rescuer4)[0].label));
    	 rescuer4=users[index].rescuer4;
     }else{
    	 $("#rescuer4").val ("");
     }
     
}

function filterIt(rescuers,searchKey) {
	//console.log("Search Key : "+ searchKey);

	var rescuer = [];
	$.each(rescuers, function(){
	    if (this.userUid==searchKey) {
	    	rescuer.push(this);
	    }
	});
	
	//console.log(homes);
	return rescuer;
	}


function saveData(){
	if($("#userEmail").val().trim()==""){
		alert("Please enter Email");
		return;
	}
	if($("#userMobileNumber").val().trim()==""){
		alert("Please enter Mobile Number");
		return;
	}
	
	if($("#userEmergencyNumber").val().trim()==""){
		alert("Please enter Emergency Number");
		return;
	}
	
	
	if($("#new_password").val()!=$("#confirm_password").val()){
		alert("Password doesn't match");
		return;
		
	}
	
	if($("#rescuer1").val()==""){
		rescuer1="";
	}
	if($("#rescuer2").val()==""){
		rescuer2="";
	}
	if($("#rescuer3").val()==""){
		rescuer3="";
	}
	if($("#rescuer4").val()==""){
		rescuer4="";
	}
	var disableUser = $('#disableUser').is(':checked');	
	var formData={
			"email":$("#userEmail").val(),
			"userUid":clickedUserUid,
			"mobileNum":$("#userMobileNumber").val(),
			"emergencyNum":$("#userEmergencyNumber").val(),
			"rescuer1":rescuer1,
			"rescuer2":rescuer2,
			"rescuer3":rescuer3,
			"rescuer4":rescuer4,
			"password":$("#new_password").val(),
			"role":userRole,
			"approved":!disableUser,
			"organization":JSON.parse(localStorage.getItem("Zepho_User")).organization,
			
	}
	
	console.log(formData);
	
	angular.element('#updateUserController').scope().$apply();
	angular.element('#updateUserController').scope().updateUser(formData);
	
}