  

$(document).ready(function() {
	 
	 var app = angular.module('rescuer', []);

	     app.controller('rescuerController',
	     				['$scope',
	     				'$window',
	     				'$location',
	     				'$http', 
	     				function($scope,$window, $location, $http) {
	     					
	     					
	     					
	     					$scope.sosClick=function(userInfo){
	     						console.log(userInfo);
	     						var info = encodeData(JSON.stringify(userInfo));
	     						var response = $http.post(baseUrl+sosEndPoint,info, headConfig);
	     						response.success(function(data, status,
										headers, config) {
	     							data = decodeData(data);
	     							console.log(data);
	     							
								});
	     						
								response.error(function(data, status,
										headers, config) {
									console.log(data);
								});
	     						
	     						
	     					}	
	     					
	     					
	     				}			
			
	     		]);

	
  
	
	
});


function sosBtnClicked(){
	
	console.log("SOS Clicked");
	sendData();
	
	//console.log(clicked_id);
	 var x = document.getElementById("snackbar")
	    x.className = "show";
	    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 700);
}

function sendData(){
	getLocation();
	var currentDate = new Date();
	console.log(currentDate);

	
}

  

function getLocation() {
	    if (navigator.geolocation) {
	        navigator.geolocation.getCurrentPosition(sendPosition);
	    } else { 
	        console.log( "Geolocation is not supported by this browser.");
	    }
	}

function sendPosition(position) {
	userInfo={
			"createdAt":Date.now(),
			"latitude":position.coords.latitude,
			"longitude":position.coords.longitude,
			"fromUser":JSON.parse(localStorage.getItem("Zepho_User")).fullName,
			"userUid":JSON.parse(localStorage.getItem("Zepho_User")).userUid,
			"type" : "Rescuer"
	}
	    
	
	angular.element('#rescuerController').scope().$apply();
	angular.element('#rescuerController').scope().sosClick(userInfo);
	
	    
}