// production Main
var baseUrl = "https://web.zepho.org/zepho-test";


//var baseUrl = "http://localhost:8080";

// Local Test
//var baseUrl = "http://localhost:8080/zepho-test";




var pageUrls = baseUrl+"/WebUI/pages";
var loginUrl = baseUrl+"/WebUI";
var sosEndPoint = "/sos";

var approvalListEndPoint = "/approveUser";

var approvalServiceRejectEndPoint= "/rejectUser";

var approvalServiceEndPoint="/doApprovalService";

var loginHtml ="/login";

var loginEndPoint ="/loginSubmit";

var runningTripEndPont = "/checkRunningTrip";

var TripEndHtml = "/trip-end";

var addTripEndPoint = "/addTrip";

var endTripService= "/endTrip";

var missionaryHtml = "/missionary";

var sosWatchedEndPoint ="/watched";

var sosAlertListServiceEndPoint = "/sosList";
