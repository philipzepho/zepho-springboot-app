

function myFunction() {
    var input, filter, ul, li, label, i;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    ul = document.getElementById("myUL");
    li = ul.getElementsByTagName("li");
    console.log("No.of Items: "+li.length);
    for (i = 0; i < li.length; i++) {
    	label = li[i].getElementsByTagName("label")[0];
        if (label.innerHTML.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";

        }
    }
}



function checkPass()
{
    //Store the password field objects into variables ...
    var new_password = document.getElementById('new_password');
    var confirm_password = document.getElementById('confirm_password');
    //Store the Confimation Message Object ...
    var message = document.getElementById('confirmMessage');
    //Set the colors we will be using ...
    var goodColor = "#66cc66";
    var badColor = "#ff6666";
    //Compare the values in the password field 
    //and the confirmation field
    if(new_password.value == confirm_password.value){
        //The passwords match. 
        //Set the color to the good color and inform
        //the user that they have entered the correct password 
    	confirm_password.style.borderColor = goodColor;
        message.style.color = goodColor;
        message.innerHTML = "Passwords Match!"
    }else{
        //The passwords do not match.
        //Set the color to the bad color and
        //notify the user.
    	confirm_password.style.borderColor = badColor;
        message.style.color = badColor;
        message.innerHTML = "Passwords Do Not Match!"
    }
}  


$(document).ready(function(){
	
	$('#togglePassword').click(function(){
		//alert($("#passDiv").is(':visible'))
		$('#passDiv').show();
	});
	// myFunction();
})


    