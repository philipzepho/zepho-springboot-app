var role="";
var rescuer1="";
var rescuer2="";
var rescuer3="";
var rescuer4="";

$(document).ready(function(){

	var enteredUrl = window.location.href;
	

	var missionaryPages = [	baseUrl+"/login",
								baseUrl+"/missionary",
								baseUrl+"/trip",
								baseUrl+"/trip-end",
							];
	var superAdminPages = [baseUrl+"/login",
							baseUrl+"/superAdmin",
							baseUrl+"/createNewAccount",
							baseUrl+"/approvalList",
							baseUrl+"/saUsersList"
							];
	var adminPages = [baseUrl+"/login",
						baseUrl+"/admin",
						baseUrl+"/createNewAccount",
						baseUrl+"/adminUsersList",
						baseUrl+"/adminApprovalList"
						];
	var rescuerPages = [baseUrl+"/login",
						baseUrl+"/rescuer",
						baseUrl+"/sosAlertList",
						baseUrl+"/alertMap"];
	
    var userData=JSON.parse(localStorage.getItem("Zepho_User"));
    
   

    if(userData==null){
    	window.location=baseUrl+"/login";
    }else{
    	var role=userData.role;
    	if(role=="Missionary"){
    		if (missionaryPages.indexOf(enteredUrl) === -1) {
    			window.location=baseUrl+"/login";
    		}
    	}
    	if(role=="Rescuer"){
    		if (rescuerPages.indexOf(enteredUrl) === -1) {
    			window.location=baseUrl+"/login";
    		}
    	}
    	if(role=="Admin"){
    		if (adminPages.indexOf(enteredUrl) === -1) {
    			window.location=baseUrl+"/login";
    		}
    	}
    	if(role=="SuperAdmin"){
    		if (superAdminPages.indexOf(enteredUrl) === -1) {
    			window.location=baseUrl+"/login";
    		}
    	}
    }
			
	
	$('#addMissionary').click(function(){
		localStorage.setItem("User_role","Missionary");
		
	});
	$('#addRescuer').click(function(){
		localStorage.setItem("User_role","Rescuer");
		
	});
	$('#addAdmin').click(function(){
		localStorage.setItem("User_role","Admin");
		
	});
	$('#addSuperAdmin').click(function(){
		localStorage.setItem("User_role","SuperAdmin");
		
	});
})

$(document).ready(function(){
	$('#addMissionary').click(function(){
		localStorage.setItem("User_role","Missionary");
		
	});
	$('#addRescuer').click(function(){
		localStorage.setItem("User_role","Rescuer");
		
	});
	$('#addAdmin').click(function(){
		localStorage.setItem("User_role","Admin");
		
	});
	
	var regRole=localStorage.getItem("User_role");
	
	if(regRole=='Missionary' || regRole=='Rescuer'){
		$('#address_state').show();
		$('#address_city').show();
		$('#address_division').show();
	}
	if(regRole=='Rescuer'){
		
		$('#rescuertype').show();
	}
})

function autoCompleteRescuers(rescuers){
        	console.log("INSIDE autoCompleteRescuers");
        	$('#rescuer1').autocomplete({
        		appendTo: "#edit_view",
                source:rescuers,
            select: function( event, ui ) {
            	$(this).val(ui.item.label);
            	rescuer1=ui.item.userUid;
            	//console.log("Label: "+ui.item.userUid);
            }
                
            });
    		$('#rescuer2').autocomplete({
    		            
    			appendTo: "#edit_view",        
    			source:rescuers,
    		        select: function( event, ui ) {
    		        	$(this).val(ui.item.label);
    		        	rescuer2=ui.item.userUid;
    		        	//console.log("Label: "+ui.item.userUid);
    		        }
    		            
    		        });
    		$('#rescuer3').autocomplete({
    		    
    			appendTo: "#edit_view",
    		    source:rescuers,
    		select: function( event, ui ) {
    			$(this).val(ui.item.label);
    			rescuer3=ui.item.userUid;
    			//console.log("Label: "+ui.item.userUid);
    		}
    		    
    		});
    		$('#rescuer4').autocomplete({
    		    
    			appendTo: "#edit_view",
    		    source:rescuers,
    		select: function( event, ui ) {
    			$(this).val(ui.item.label);
    			rescuer4=ui.item.userUid;
    			//console.log("Label: "+ui.item.userUid);
    		}
    		    
    		});
        }

function logout(){
    localStorage.removeItem("Zepho_User");
    localStorage.removeItem("Zepho_Url");
    localStorage.removeItem("Zepho_Rescuers");
    localStorage.removeItem("x-auth-token");    
    
    window.location.replace(baseUrl+loginHtml);
}




var token=localStorage.getItem("x-auth-token");
var headConfig = {
         headers: {
        	'Content-Type' :'application/json',
        	'Accept': 'text/plain',
        	'x-auth-token': token
         }
       };

function decodeData(info){
	var decode = this.removeRandom(info);
	var res =  atob(decode);
	res = res.split("").reverse().join("");
	res = atob(res);
	res = JSON.parse(res);
	return res;
}

function encodeData(info){
	var res =  btoa(info);	
	res = res.split("").reverse().join("");
	res = btoa(res);
	res = this.randomString() + res;
	return res;
}


function randomString() {
	var text = "";
	var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	for (var i = 0; i < 7; i++) {
		text += possible.charAt(Math.floor(Math.random() * possible.length));
	}
	return text;
}

function removeRandom(data){
	return data.substr(7);
}
