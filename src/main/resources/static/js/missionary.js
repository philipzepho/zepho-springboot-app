  

$(document).ready(function() {
	 
	 var app = angular.module('missionary', []);
	 
	     app.controller('missionaryController',
	     				['$scope',
	     				'$window',
	     				'$location',
	     				'$http', 
	     				function($scope,$window, $location, $http) {
	     					
	     					
	     					$scope.init=function(){
	     						var userInfo = {
	     						   		"userUid":JSON.parse(localStorage.getItem("Zepho_User")).userUid
	     						   	};
	     						var info = encodeData(JSON.stringify(userInfo));
	     						var response = $http.post(baseUrl+runningTripEndPont,info, headConfig);
	     						response.success(function(data, status,
										headers, config) {
	     								
	     							data = decodeData(data);
	     							if(data.isFinished=="false"){
	     								window.location.replace(baseUrl+TripEndHtml);
	     		                      }
	     							
								});
	     						
								response.error(function(data, status,
										headers, config) {
									console.log(data);
								});
	     					}
	     					
	     					$scope.sosClick=function(userInfo){
	     						console.log(userInfo);
	     						var info = encodeData(JSON.stringify(userInfo));
	     						var response = $http.post(baseUrl+sosEndPoint,info, headConfig);
	     						response.success(function(data, status,
										headers, config) {
	     							data = decodeData(data);
	     							console.log(data);
	     							
								});
	     						
								response.error(function(data, status,
										headers, config) {
									console.log(data);
								});
	     						
	     						
	     					}	
	     					
	     					$scope.sendTripInfo=function(tripInfo){
	     						console.log(tripInfo);
	     						var info = encodeData(JSON.stringify(tripInfo));
	     						var response = $http.post(baseUrl+addTripEndPoint,info, headConfig);
	     						response.success(function(data, status,
										headers, config) {
	     							data = decodeData(data);
	     							window.location.replace(baseUrl+TripEndHtml);
	     							console.log(data);
	     							
								});
	     						
								response.error(function(data, status,
										headers, config) {
									console.log(data);
								});
								
								
	     						
	     					}	
	     					
	     					$scope.endTripInfo=function(tripInfo){
	     			     		
	     						var info = encodeData(JSON.stringify(tripInfo));
	     						var response = $http.post(baseUrl+endTripService,info, headConfig);
	     						response.success(function(data, status,headers, config) {
	     							data = decodeData(data);
	     							console.log(data);
	     							window.location.replace(baseUrl+missionaryHtml);
	     							
								});
	     						
								response.error(function(data, status,
										headers, config) {
									console.log(data);
								});
	     					}
	     					
	     					
	     				}			
			
	     		]);
	
});


function sosBtnClicked(){
	
	console.log("SOS Clicked");
	sendData();
	
	
	//console.log(clicked_id);
	 var x = document.getElementById("snackbar")
	    x.className = "show";
	    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 700);
}

function sendData(){
	getLocation();
	var currentDate = new Date();
	console.log(currentDate);

	
}

  

function getLocation() {
	    if (navigator.geolocation) {
	        navigator.geolocation.getCurrentPosition(sendPosition);
	    } else { 
	        console.log( "Geolocation is not supported by this browser.");
	    }
	}

function sendPosition(position) {
	userInfo={
			"createdAt":Date.now(),
			"latitude":position.coords.latitude,
			"longitude":position.coords.longitude,
			"fromUser":JSON.parse(localStorage.getItem("Zepho_User")).fullName,
			"userUid":JSON.parse(localStorage.getItem("Zepho_User")).userUid,
			"type" : "Missionary"
	}
	    
	
	angular.element('#missionaryController').scope().$apply();
	angular.element('#missionaryController').scope().sosClick(userInfo);
	
	    
}

function submitTripInfo(input){
	if(!input.hasOwnProperty('userUid')){
		alert("Please click on Get Directions..");
	}
	
	angular.element('#missionaryController').scope().$apply();
	angular.element('#missionaryController').scope().sendTripInfo(input);
}

function endCurrentTrip(){
	
	   	var userInfo = {
	   		"userUid":JSON.parse(localStorage.getItem("Zepho_User")).userUid
	   	};
	   	console.log(userInfo);
	   	angular.element('#missionaryController').scope().$apply();
	   	angular.element('#missionaryController').scope().endTripInfo(userInfo);
	   	
	  
}