    var app = angular.module('formSubmit', []);

    app.controller('FormSubmitController',
    				['$scope',
    				'$window',
    				'$location',
    				'$http', 
    						function($scope, $window, $location, $http) {
    							console.log("TEST");
    							$scope.rescuertype='Advocate';
    							$scope.division='EAST';
    							$scope.gender='Male';
    							$scope.organization='PR';

    							$scope.list = [];
    							$scope.headerText = 'AngularJS Post Form Spring MVC example: Submit below form';
    							
    							 console.log("LOAD RESCUERS2");
								 var org=JSON.parse(localStorage.getItem("Zepho_User")).organization
								 var info = encodeData(org);
    							var response = $http.post(baseUrl+'/getRescuerList',info, headConfig);
								response.success(function(data, status,
										headers, config) {
									data = decodeData(data);
									
									//localStorage.setItem("Zepho_Rescuers",JSON.stringify(data.RescuerList));
									
									rescuers=data.RescuerList;
									console.log(rescuers);
									autoCompleteRescuers(rescuers);
									
										

								});
								response.error(function(data, status,
										headers, config) {
									alert("Exception details: "
											+ JSON.stringify({
												data : data
											}));
								});
								
								$scope.getStateList= function(){
									var stateList = $http.post(baseUrl+'/getStateNames', "", headConfig);
									stateList.success(function(data, status, headers, config) {
										data = decodeData(data);
										console.log(data.states);
										$("#stateId").empty();		        	
										var states = data.states;
										if(states.length > 0){
											$("#stateId").append(
													$("<option></option>").val('select').html('Select State'));
											$.each(states, function(index, value) {
												$("#stateId").append(
														$("<option></option>").val(value.stateId).html(
																value.stateName));
											});
										}
										$("#districtId").empty();
										$("#districtId").append(
												$("<option></option>").val('select').html('Select District'));
	    								
		    						});
									stateList.error(function(data, status,
		    								headers, config) {
		    							alert("Exception details: "
		    									+ JSON.stringify({
		    										data : data
		    							}));
		    						});
								};
								
								
								$scope.getDistricts= function(){
									$("#districtId").empty();
									$("#districtId").append(
											$("<option></option>").val('select').html('Select District'));
									$("#cityId").empty();
									$("#cityId").append(
											$("<option></option>").val('select').html('Select City'));
									var getStateId = $("#stateId").val();
									if (getStateId!="select"){
										var info = encodeData(getStateId);
										var distList = $http.post(baseUrl+'/getDistrictNames',info, headConfig);
										distList.success(function(data, status, headers, config) {
											data = decodeData(data);
											console.log(data.district);
											$("#districtId").empty();		        	
											var districts = data.district;
											if(districts.length > 0){
												$("#districtId").append(
														$("<option></option>").val('select').html('Select District'));
												$.each(districts, function(index, value) {
													$("#districtId").append(
															$("<option></option>").val(value.districtId).html(
																	value.districtName));
												});
											}
		    								
			    						});
										distList.error(function(data, status,
			    								headers, config) {
			    							alert("Exception details: "
			    									+ JSON.stringify({
			    										data : data
			    							}));
			    						});
									}								
								};
								
								$scope.getCity= function(){
									$("#cityId").empty();
									$("#cityId").append(
											$("<option></option>").val('select').html('Select City'));
									var getDistId = $("#districtId").val();
									if (getDistId!="select district"){
										var info = encodeData(getDistId);
										var cityList = $http.post(baseUrl+'/getCityNames',info, headConfig);
										cityList.success(function(data, status, headers, config) {
											data = decodeData(data);
											console.log(data.city);
											$("#cityId").empty();		        	
											var cities = data.city;
											if(cities.length > 0){
												$("#cityId").append(
														$("<option></option>").val('select').html('Select City'));
												$.each(cities, function(index, value) {
													$("#cityId").append(
															$("<option></option>").val(value.cityId).html(
																	value.cityName));
												});
											} 
		    								
			    						});
										cityList.error(function(data, status,
			    								headers, config) {
			    							alert("Exception details: "
			    									+ JSON.stringify({
			    										data : data
			    							}));
			    						});
									}									
								};
								$scope.sendPin = function(pincode){
									var dist = $http.get("http://postalpincode.in/api/pincode/"+this.pincode);
								      dist.success(function (data, status, headers, config){
								    	  var postOffice = data.PostOffice;
								    	  $scope.locationName = [];
								          for (var i=0; i<postOffice.length; i++){
								        	  $scope.locationName = (postOffice);
								        	 }
									});
								};
								
    							$scope.submit = function() {
    								
    								var isApproved=false;
    								var role=JSON.parse(localStorage.getItem("Zepho_User")).role;
    								var addedBy=JSON.parse(localStorage.getItem("Zepho_User")).fullName;
    								var addedById=JSON.parse(localStorage.getItem("Zepho_User")).userUid;
    								var state = $( "#stateId option:selected" ).text();
    								var district = $( "#districtId option:selected" ).text();
    								var city = $( "#cityId option:selected" ).text();
    								var knownLocation = $( "#gottenDistrictId option:selected" ).text();    								
    								var knownState = $("#knownState").val();
    								var knownDistrict = $("#knownDistrict").val();
    								var knownCity = $("#knownCity").val();
    								var pincode = $("#pincode").val();
    								if(role=="SuperAdmin"){
    									isApproved=true;
    								}
    								if (state=="Select State"){
    									alert("Please select a State");
    									return;
    								}
    								if (district=="Select District"){
    									alert("Please select a District");
    									return;
    								}
    								if (city=="Select City"){
    									alert("Please select a City");
    									return;
    								}
    								if (state=="" || state==undefined){
    									if (knownState!="" || knownState!= undefined){
    										state=knownState;
    									}
    								}
    								if (city=="" || city==undefined){
    									if (knownCity!="" || knownCity!= undefined){
    										city=knownCity;
    									}
    								}
    								if (district=="" || district==undefined){
    									if (knownDistrict!="" || knownDistrict!= undefined){
    										district=knownDistrict;
    									}
    								}
    							

    								var formData = {
    										
    										"firstName":$scope.firstName,
    			                              "lastName":$scope.lastName,
    			                              "fullName":$scope.firstName+ " "+ $scope.lastName,
    			                              "mobileNum":$scope.mobileNum,
    			                              "emergencyNum":$scope.emergencyNum,
    			                              "email":$scope.email,
    			                              "role":localStorage.getItem("User_role"),
    			                              "organization":$scope.organization,
    			                              "userName":$scope.userName,
    			                              "password":$scope.password,
    			                             "rescuer1":rescuer1,
    			                              "rescuer2":rescuer2,
    			                              "rescuer3":rescuer3,
    			                              "rescuer4":rescuer4,
    			                              "isRejected":false,
    			                              "isApproved":isApproved,
    			                              "addedBy": addedBy,
    			                              "addedById":addedById,
    			                              "state":state,
    			                              "city":city,
    			                              "division":$scope.division,
    			                              "rescuertype":$scope.rescuertype,
    			                              "middleName":$scope.middleName,
    			                              "gender":$scope.gender,
    			                              "address":$scope.address,
    			                              "area":$scope.area,
    			                              "pincode":pincode,
    			                              "alternateEmail":$scope.alternateEmail,
    			                              "telephone":$scope.telephoneNumber,
    			                              "whatsappNumber":$scope.whatsappNumber,
    			                              "confirmPassword":$scope.confirmPassword,
    			                              "emergencyNum":$scope.emergencyNum,
    			                              "aadhaarNumber":$scope.aadharNumber,
    			                              "contactNotOwn":$scope.contactNum,
    			                              "secondaryContact":$scope.secondaryContactNum,
    			                              "churchDetails":$scope.churchDetails,
    			                              "nameOfChurch":$scope.churchName,
    			                              "memberSince":$scope.memberSince,
    			                              "pastorName":$scope.pastorName,
    			                              "pastorOrgContact":$scope.pastorOrgNumber,
    			                              "district":district,
    			                              "willingToHelp":$scope.willingToHelp
    			                              
    								}  								
    									
    								if(formData.mobileNum==formData.emergencyNum){
    									alert('Mobile number and Emergency Contact numbers should not be same.');
    									return;
    								}
    								if(formData.password!=formData.confirmPassword){
    									alert('Password and confirm password Do not match');
    									return;
    								}
    								var info = encodeData(JSON.stringify(formData));
    									var response = $http.post(baseUrl+'/registration',
        										info, headConfig);
        								response.success(function(data, status,
        										headers, config) {
        									data = decodeData(data);	
        									alert(data.info);
        								});
        								response.error(function(data, status,
        										headers, config) {
        									alert("Exception details: "
        											+ JSON.stringify({
        												data : data
        											}));
        								});
    								};   
    							}
    						 ]);