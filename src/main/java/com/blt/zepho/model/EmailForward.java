package com.blt.zepho.model;

public class EmailForward {

	private long createdAt;
	private double latitude;
	private double longitude;
	private String fromUser;
	private String uuid;
	private String userUid;
	private String type;
	private String locationName;
	private boolean onBehalf;
	private String obUserName;
	private String obUserUid;
	private String rescuerName;
	private String emailId;
	
	private String locality;
	private String subLocality;
	private String thoroughfare;
	private String subThoroughfare;
	
	
	
	
	
	public String getLocality() {
		return locality;
	}
	public void setLocality(String locality) {
		this.locality = locality;
	}
	public String getSubLocality() {
		return subLocality;
	}
	public void setSubLocality(String subLocality) {
		this.subLocality = subLocality;
	}
	public String getThoroughfare() {
		return thoroughfare;
	}
	public void setThoroughfare(String thoroughfare) {
		this.thoroughfare = thoroughfare;
	}
	public String getSubThoroughfare() {
		return subThoroughfare;
	}
	public void setSubThoroughfare(String subThoroughfare) {
		this.subThoroughfare = subThoroughfare;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public long getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(long createdAt) {
		this.createdAt = createdAt;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public String getFromUser() {
		return fromUser;
	}
	public void setFromUser(String fromUser) {
		this.fromUser = fromUser;
	}
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public String getUserUid() {
		return userUid;
	}
	public void setUserUid(String userUid) {
		this.userUid = userUid;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getLocationName() {
		return locationName;
	}
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	public boolean isOnBehalf() {
		return onBehalf;
	}
	public void setOnBehalf(boolean onBehalf) {
		this.onBehalf = onBehalf;
	}
	public String getObUserName() {
		return obUserName;
	}
	public void setObUserName(String obUserName) {
		this.obUserName = obUserName;
	}
	public String getObUserUid() {
		return obUserUid;
	}
	public void setObUserUid(String obUserUid) {
		this.obUserUid = obUserUid;
	}
	public String getRescuerName() {
		return rescuerName;
	}
	public void setRescuerName(String rescuerName) {
		this.rescuerName = rescuerName;
	}
}
