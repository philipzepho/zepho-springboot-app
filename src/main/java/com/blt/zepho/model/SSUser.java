package com.blt.zepho.model;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;


public class SSUser implements UserDetails {

    
    private Long id;

   
   private String username;
   private String password;
   
   public String getUsername() {
        return username;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

   
   
    public String getPassword() {
        return password;
    }

   
    public void setPassword(String password) {
        this.password = password;
    }


   
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Collection<GrantedAuthority> authorities = new ArrayList<>();        
        if(this.getRoleName() != null) {
            SimpleGrantedAuthority authority = new SimpleGrantedAuthority(this.getRoleName());
            authorities.add(authority);
        }
        return authorities;
    }

   
    public boolean isAccountNonExpired() {
        return true;
    }

  
    public boolean isAccountNonLocked() {
        return true;
    }

   
    public boolean isCredentialsNonExpired() {
        return true;
    }

    
    public boolean isEnabled() {
        return true;
    }


    
    private String roleName;

   
    public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null)
            return false;

        if (o instanceof SSUser) {
            final SSUser other = (SSUser) o;
            return Objects.equals(getId(), other.getId())
                    && Objects.equals(getUsername(), other.getUsername())
                    && Objects.equals(getPassword(), other.getPassword())
                    && Objects.equals(getRoleName(), other.getRoleName())
                    && Objects.equals(isEnabled(), other.isEnabled());
        }
        return false;
    }

   

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getUsername(), getPassword(), getRoleName(), isEnabled());
    }
}