package com.blt.zepho.model;




import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

import javax.validation.constraints.Size;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.fasterxml.jackson.annotation.JsonProperty;


public final class UserDTO {

	private static final String ROLE_ADMIN = "ADMIN";
	private static final String ROLE_USER = "USER";
	private static final String ADMIN = "admin";
	//private static final String USER = "user";

    private final String username;
    @Size(min = 8, max = 100)
    private final String password;
    //private final String email;

    public UserDTO(@JsonProperty("username")String username,
    		@JsonProperty("password")String password){
                   //String name) {
        this.username = username;
        this.password = password;
        //this.name = name;
    }

    public Optional<String> getUsername() {
        return Optional.ofNullable(username);
    }

    public Optional<String> getEncodedPassword() {
        return Optional.ofNullable(password).map(p -> new BCryptPasswordEncoder().encode(p));
    }

   /* public Optional<String> getName() {
        return Optional.ofNullable(name);
    }
*/
    public SSUser toUser() {
        SSUser user = new SSUser();
        user.setUsername(username);
        if(username.equalsIgnoreCase(ADMIN))
        	user.setRoleName(ROLE_ADMIN);
        else        	
        	user.setRoleName(ROLE_USER);
        
        user.setPassword(new BCryptPasswordEncoder().encode(password));
        
        return user;
    }

    public UsernamePasswordAuthenticationToken toAuthenticationToken() {
        return new UsernamePasswordAuthenticationToken(username, password, getAuthorities());
    }

    public Collection<? extends GrantedAuthority> getAuthorities() {
    	
    	if(username.equalsIgnoreCase(ADMIN))
    		return Collections.singleton(() -> ROLE_ADMIN);
    	else
    		return Collections.singleton(() -> ROLE_USER);    }

}