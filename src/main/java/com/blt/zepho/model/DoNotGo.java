package com.blt.zepho.model;

import java.util.ArrayList;

public class DoNotGo {
	
	private String uuid;
	
	private String fromUserName;
	private String fromUserUid;
	private String message;
	private ArrayList<Missionary> selectedMembers;
	private long createdAt;
	private String fromUserRole;
	private String state;
	private String org;
	private String imageData;
	private String fileName;
	
	
	public String getFromUserRole() {
		return fromUserRole;
	}
	public void setFromUserRole(String fromUserRole) {
		this.fromUserRole = fromUserRole;
	}
	public long getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(long createdAt) {
		this.createdAt = createdAt;
	}
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public String getFromUserName() {
		return fromUserName;
	}
	public void setFromUserName(String fromUserName) {
		this.fromUserName = fromUserName;
	}
	public String getFromUserUid() {
		return fromUserUid;
	}
	public void setFromUserUid(String fromUserUid) {
		this.fromUserUid = fromUserUid;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public ArrayList<Missionary> getSelectedMembers() {
		return selectedMembers;
	}
	public void setSelectedMembers(ArrayList<Missionary> selectedMembers) {
		this.selectedMembers = selectedMembers;
	}
	
	public String toString(){
		
		return "From User : "+getFromUserName() +" \n " + "FromUserUid :"+getFromUserUid()+" \n " + "Selected Members : "+getSelectedMembers().size();
	}
	
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getOrg() {
		return org;
	}
	public void setOrg(String org) {
		this.org = org;
	}
	public String getImageData() {
		return imageData;
	}
	public void setImageData(String imageData) {
		this.imageData = imageData;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
}
