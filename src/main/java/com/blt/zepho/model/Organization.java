package com.blt.zepho.model;

//create table Organization(orgId int NOT NULL AUTO_INCREMENT, orgFullName varchar(60), orgShortName varchar(40), PRIMARY KEY(orgId));

public class Organization {
	
	private String orgFullName;
	private String orgShortName;
	private String inviteCode;
	
	public String getOrgFullName() {
		return orgFullName;
	}
	public void setOrgFullName(String orgFullName) {
		this.orgFullName = orgFullName;
	}
	public String getOrgShortName() {
		return orgShortName;
	}
	public void setOrgShortName(String orgShortName) {
		this.orgShortName = orgShortName;
	}
	public String getInviteCode() {
		return inviteCode;
	}
	public void setInviteCode(String inviteCode) {
		this.inviteCode = inviteCode;
	}

}
