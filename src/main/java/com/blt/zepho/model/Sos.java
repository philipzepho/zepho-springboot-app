package com.blt.zepho.model;

import java.util.Comparator;

public class Sos  {

	private long createdAt;
	private double latitude;
	private double longitude;
	private String fromUser;
	private String uuid;
	private String userUid;
	private String type;
	private String locationName;
	private boolean onBehalf;
	private String obUserName;
	private String obUserUid;

	private String locality;
	private String postalCode;
	private String subLocality;
	private String subThoroughfare;
	private String thoroughfare;
	private String administrativeArea;
	private String callOrSos;
	private int userPincode;
	private String userOrg;
	
	
	public String getLocality() {
		return locality;
	}

	public void setLocality(String locality) {
		this.locality = locality;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getSubLocality() {
		return subLocality;
	}

	public void setSubLocality(String subLocality) {
		this.subLocality = subLocality;
	}

	public String getSubThoroughfare() {
		return subThoroughfare;
	}

	public void setSubThoroughfare(String subThoroughfare) {
		this.subThoroughfare = subThoroughfare;
	}

	public String getThoroughfare() {
		return thoroughfare;
	}

	public void setThoroughfare(String thoroughfare) {
		this.thoroughfare = thoroughfare;
	}


	public boolean isOnBehalf() {
		return onBehalf;
	}

	public void setOnBehalf(boolean onBehalf) {
		this.onBehalf = onBehalf;
	}

	public String getObUserName() {
		return obUserName;
	}

	public void setObUserName(String obUserName) {
		this.obUserName = obUserName;
	}

	public String getObUserUid() {
		return obUserUid;
	}

	public void setObUserUid(String obUserUid) {
		this.obUserUid = obUserUid;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public long getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(long createdAt) {
		this.createdAt = createdAt;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getUserUid() {
		return userUid;
	}

	public void setUserUid(String userUid) {
		this.userUid = userUid;
	}


	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public String getFromUser() {
		return fromUser;
	}

	public void setFromUser(String fromUser) {
		this.fromUser = fromUser;
	}

	public String getAdministrativeArea() {
		return administrativeArea;
	}

	public void setAdministrativeArea(String administrativeArea) {
		this.administrativeArea = administrativeArea;
	}

	public String getCallOrSos() {
		return callOrSos;
	}

	public void setCallOrSos(String callOrSos) {
		this.callOrSos = callOrSos;
	}

	public int getUserPincode() {
		return userPincode;
	}

	public void setUserPincode(int userPincode) {
		this.userPincode = userPincode;
	}

	public String getUserOrg() {
		return userOrg;
	}

	public void setUserOrg(String userOrg) {
		this.userOrg = userOrg;
	}
}
