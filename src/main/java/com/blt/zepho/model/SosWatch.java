package com.blt.zepho.model;

public class SosWatch {
	
	private String uuid;
	private String userUid;
	private boolean isWatched;
	
	
	
	
	public SosWatch(String uuid, String userUid, boolean isWatched) {
		super();
		this.uuid = uuid;
		this.userUid = userUid;
		this.isWatched = isWatched;
	}
	
	
	public String getUuid() {
		return uuid;
	}


	public void setUuid(String uuid) {
		this.uuid = uuid;
	}


	public String getUserUid() {
		return userUid;
	}
	public void setUserUid(String userUid) {
		this.userUid = userUid;
	}
	public boolean isWatched() {
		return isWatched;
	}
	public void setWatched(boolean isWatched) {
		this.isWatched = isWatched;
	}
	
	

}
