package com.blt.zepho.model;

import java.util.ArrayList;
import java.util.Date;

import javax.persistence.Transient;

public class RegistrationList {
	   @SuppressWarnings("unused")
	private static final long serialVersionUID = 1L; 
	   
	public ArrayList<Registration> regList;
	private boolean approvedByAdmin;
	private boolean approvedBySuAdmin;
	
	public ArrayList<Registration> getRegList() {
		return regList;
	}

	public void setRegList(ArrayList<Registration> regList) {
		this.regList = regList;
	}
	
	public boolean isApprovedByAdmin() {
		return approvedByAdmin;
	}

	public void setApprovedByAdmin(boolean approvedByAdmin) {
		this.approvedByAdmin = approvedByAdmin;
	}

	public boolean isApprovedBySuAdmin() {
		return approvedBySuAdmin;
	}

	public void setApprovedBySuAdmin(boolean approvedBySuAdmin) {
		this.approvedBySuAdmin = approvedBySuAdmin;
	}

}
