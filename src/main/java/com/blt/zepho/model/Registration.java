package com.blt.zepho.model;

import java.util.Date;

import javax.persistence.Transient;

public class Registration {
	   @SuppressWarnings("unused")
	private static final long serialVersionUID = 1L; 
	   
	    private int id;
		private String firstName;
		private String lastName;
		private String fullName;
		private String mobileNum;
		private String email;
		private String password;
		private String role;
		private boolean isApproved;
		private boolean isRejected;
		private String addedBy;
		private String organization;
		private String deviceId;
		private String rescuer1;
		private String rescuer2;
		private String rescuer3;
		private String rescuer4;
		private String rescuer5;
		private String extraField1;
		private String extraField2;
		private String extraField3;
		private String userUid;
		private String userName;
		
	
		private String addedById;
		
		private String state;
		private String city;
		private String division;
		private String rescuertype;
		private String emergencyNum;
		
		private String middleName;
		private String gender;
		private String address;
		private String area;
		private String alternateEmail;
		private String telephone;
		private String whatsappNumber;
		private String aadhaarNumber;
		private String profession;
		private String contactNotOwn;
		private String secondaryContact;
		private String bloodGroup;
		private String churchDetails;
		private String nameOfChurch;
		private String memberSince;
		private String pastorName;
		private String pastorOrgContact;
		private int pincode;
		private String district;
		private boolean approvedByAdmin;
		private boolean approvedBySuAdmin;
		private Date dob;
		private boolean emailVerified;
		private String fatherName;
		private String primaryRelation;
		private String secondaryRelation;
		private String willingToHelp;
		private String approveByAdminUid;
		private boolean completeReg;
		private String primaryName;
		private String secondaryContactName;
		@Transient
		private String updateRole;
		private String dobString;
		
		private String newPassword;
		private String reEnteredPassword;
		@Transient
		private int salt;
		private int loginAttempts;		
		
		
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public void setApproved(boolean isApproved) {
			this.isApproved = isApproved;
		}
		public void setRejected(boolean isRejected) {
			this.isRejected = isRejected;
		}
		public String getMiddleName() {
			return middleName;
		}
		public void setMiddleName(String middleName) {
			this.middleName = middleName;
		}
		public String getGender() {
			return gender;
		}
		public void setGender(String gender) {
			this.gender = gender;
		}
		public String getAddress() {
			return address;
		}
		public void setAddress(String address) {
			this.address = address;
		}
		public String getArea() {
			return area;
		}
		public void setArea(String area) {
			this.area = area;
		}
		public String getAlternateEmail() {
			return alternateEmail;
		}
		public void setAlternateEmail(String alternateEmail) {
			this.alternateEmail = alternateEmail;
		}
		public String getTelephone() {
			return telephone;
		}
		public void setTelephone(String telephone) {
			this.telephone = telephone;
		}
		public String getWhatsappNumber() {
			return whatsappNumber;
		}
		public void setWhatsappNumber(String whatsappNumber) {
			this.whatsappNumber = whatsappNumber;
		}
		public String getAadhaarNumber() {
			return aadhaarNumber;
		}
		public void setAadhaarNumber(String aadhaarNumber) {
			this.aadhaarNumber = aadhaarNumber;
		}
		public String getProfession() {
			return profession;
		}
		public void setProfession(String profession) {
			this.profession = profession;
		}
		public String getContactNotOwn() {
			return contactNotOwn;
		}
		public void setContactNotOwn(String contactNotOwn) {
			this.contactNotOwn = contactNotOwn;
		}
		public String getSecondaryContact() {
			return secondaryContact;
		}
		public void setSecondaryContact(String secondaryContact) {
			this.secondaryContact = secondaryContact;
		}
		public String getBloodGroup() {
			return bloodGroup;
		}
		public void setBloodGroup(String bloodGroup) {
			this.bloodGroup = bloodGroup;
		}
		public String getChurchDetails() {
			return churchDetails;
		}
		public void setChurchDetails(String churchDetails) {
			this.churchDetails = churchDetails;
		}
		public String getNameOfChurch() {
			return nameOfChurch;
		}
		public void setNameOfChurch(String nameOfChurch) {
			this.nameOfChurch = nameOfChurch;
		}
		public String getMemberSince() {
			return memberSince;
		}
		public void setMemberSince(String memberSince) {
			this.memberSince = memberSince;
		}
		public String getPastorName() {
			return pastorName;
		}
		public void setPastorName(String pastorName) {
			this.pastorName = pastorName;
		}
		public String getPastorOrgContact() {
			return pastorOrgContact;
		}
		public void setPastorOrgContact(String pastorOrgContact) {
			this.pastorOrgContact = pastorOrgContact;
		}		
		
		public String getEmergencyNum() {
			return emergencyNum;
		}
		public void setEmergencyNum(String emergencyNum) {
			this.emergencyNum = emergencyNum;
		}
		public String getState() {
			return state;
		}
		public void setState(String state) {
			this.state = state;
		}
		public String getCity() {
			return city;
		}
		public void setCity(String city) {
			this.city = city;
		}
		public String getDivision() {
			return division;
		}
		public void setDivision(String division) {
			this.division = division;
		}
		public String getRescuertype() {
			return rescuertype;
		}
		public void setRescuertype(String rescuertype) {
			this.rescuertype = rescuertype;
		}
		public String getAddedById() {
			return addedById;
		}
		public void setAddedById(String addedById) {
			this.addedById = addedById;
		}
		public String getUserName() {
			return userName;
		}
		public void setUserName(String userName) {
			this.userName = userName;
		}
		public String getUserUid() {
			return userUid;
		}
		public void setUserUid(String userUid) {
			this.userUid = userUid;
		}
		/**
		 * @return the firstName
		 */
		public String getFirstName() {
			return firstName;
		}
		/**
		 * @param firstName the firstName to set
		 */
		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}
		/**
		 * @return the lastName
		 */
		public String getLastName() {
			return lastName;
		}
		/**
		 * @param lastName the lastName to set
		 */
		public void setLastName(String lastName) {
			this.lastName = lastName;
		}
		/**
		 * @return the fullName
		 */
		public String getFullName() {
			return fullName;
		}
		/**
		 * @param fullName the fullName to set
		 */
		public void setFullName(String fullName) {
			this.fullName = fullName;
		}
		/**
		 * @return the mobileNum
		 */
		public String getMobileNum() {
			return mobileNum;
		}
		/**
		 * @param mobileNum the mobileNum to set
		 */
		public void setMobileNum(String mobileNum) {
			this.mobileNum = mobileNum;
		}
		/**
		 * @return the email
		 */
		public String getEmail() {
			return email;
		}
		/**
		 * @param email the email to set
		 */
		public void setEmail(String email) {
			this.email = email;
		}
		/**
		 * @return the password
		 */
		public String getPassword() {
			return password;
		}
		/**
		 * @param password the password to set
		 */
		public void setPassword(String password) {
			this.password = password;
		}
		/**
		 * @return the role
		 */
		public String getRole() {
			return role;
		}
		/**
		 * @param role the role to set
		 */
		public void setRole(String role) {
			this.role = role;
		}
		/**
		 * @return the isApproved
		 */
		public boolean getIsApproved() {
			return isApproved;
		}
		/**
		 * @param isApproved the isApproved to set
		 */
		public void setIsApproved(boolean isApproved) {
			this.isApproved = isApproved;
		}
		/**
		 * @return the isRejected
		 */
		public boolean getIsRejected() {
			return isRejected;
		}
		/**
		 * @param isRejected the isRejected to set
		 */
		public void setIsRejected(boolean isRejected) {
			this.isRejected = isRejected;
		}
		/**
		 * @return the addedBy
		 */
		public String getAddedBy() {
			return addedBy;
		}
		/**
		 * @param addedBy the addedBy to set
		 */
		public void setAddedBy(String addedBy) {
			this.addedBy = addedBy;
		}
		/**
		 * @return the organization
		 */
		public String getOrganization() {
			return organization;
		}
		/**
		 * @param organization the organization to set
		 */
		public void setOrganization(String organization) {
			this.organization = organization;
		}
		/**
		 * @return the deviceId
		 */
		public String getDeviceId() {
			return deviceId;
		}
		/**
		 * @param deviceId the deviceId to set
		 */
		public void setDeviceId(String deviceId) {
			this.deviceId = deviceId;
		}
		/**
		 * @return the rescuer1
		 */
		public String getRescuer1() {
			return rescuer1;
		}
		/**
		 * @param rescuer1 the rescuer1 to set
		 */
		public void setRescuer1(String rescuer1) {
			this.rescuer1 = rescuer1;
		}
		/**
		 * @return the rescuer2
		 */
		public String getRescuer2() {
			return rescuer2;
		}
		/**
		 * @param rescuer2 the rescuer2 to set
		 */
		public void setRescuer2(String rescuer2) {
			this.rescuer2 = rescuer2;
		}
		/**
		 * @return the rescuer3
		 */
		public String getRescuer3() {
			return rescuer3;
		}
		/**
		 * @param rescuer3 the rescuer3 to set
		 */
		public void setRescuer3(String rescuer3) {
			this.rescuer3 = rescuer3;
		}
		/**
		 * @return the rescuer4
		 */
		public String getRescuer4() {
			return rescuer4;
		}
		/**
		 * @param rescuer4 the rescuer4 to set
		 */
		public void setRescuer4(String rescuer4) {
			this.rescuer4 = rescuer4;
		}
		/**
		 * @return the rescuer5
		 */
		public String getRescuer5() {
			return rescuer5;
		}
		/**
		 * @param rescuer5 the rescuer5 to set
		 */
		public void setRescuer5(String rescuer5) {
			this.rescuer5 = rescuer5;
		}
		/**
		 * @return the extraField1
		 */
		public String getExtraField1() {
			return extraField1;
		}
		/**
		 * @param extraField1 the extraField1 to set
		 */
		public void setExtraField1(String extraField1) {
			this.extraField1 = extraField1;
		}
		/**
		 * @return the extraField2
		 */
		public String getExtraField2() {
			return extraField2;
		}
		/**
		 * @param extraField2 the extraField2 to set
		 */
		public void setExtraField2(String extraField2) {
			this.extraField2 = extraField2;
		}
		/**
		 * @return the extraField3
		 */
		public String getExtraField3() {
			return extraField3;
		}
		/**
		 * @param extraField3 the extraField3 to set
		 */
		public void setExtraField3(String extraField3) {
			this.extraField3 = extraField3;
		}
		
		/**
		 * @param this is used to update role as admin
		 */
		@Transient
		public String getUpdateRole() {
			return updateRole;
		}
		public void setUpdateRole(String updateRole) {
			this.updateRole = updateRole;
		}
		public String getDistrict() {
			return district;
		}
		public void setDistrict(String district) {
			this.district = district;
		}
		public boolean isApprovedByAdmin() {
			return approvedByAdmin;
		}
		public void setApprovedByAdmin(boolean approvedByAdmin) {
			this.approvedByAdmin = approvedByAdmin;
		}
		public boolean isApprovedBySuAdmin() {
			return approvedBySuAdmin;
		}
		public void setApprovedBySuAdmin(boolean approvedBySuAdmin) {
			this.approvedBySuAdmin = approvedBySuAdmin;
		}
		public Date getDob() {
			return dob;
		}
		public void setDob(Date dob) {
			this.dob = dob;
		}
		
		@Transient
		public String getDobString() {
			return dobString;
		}
		public void setDobString(String dobString) {
			this.dobString = dobString;
		}
		public boolean isEmailVerified() {
			return emailVerified;
		}
		public void setEmailVerified(boolean emailVerified) {
			this.emailVerified = emailVerified;
		}
		public String getFatherName() {
			return fatherName;
		}
		public void setFatherName(String fatherName) {
			this.fatherName = fatherName;
		}
		public String getPrimaryRelation() {
			return primaryRelation;
		}
		public void setPrimaryRelation(String primaryRelation) {
			this.primaryRelation = primaryRelation;
		}
		public String getSecondaryRelation() {
			return secondaryRelation;
		}
		public void setSecondaryRelation(String secondaryRelation) {
			this.secondaryRelation = secondaryRelation;
		}
		public String getWillingToHelp() {
			return willingToHelp;
		}
		public void setWillingToHelp(String willingToHelp) {
			this.willingToHelp = willingToHelp;
		}
		public int getPincode() {
			return pincode;
		}
		public void setPincode(int pincode) {
			this.pincode = pincode;
		}
		public String getApproveByAdminUid() {
			return approveByAdminUid;
		}
		public void setApproveByAdminUid(String approveByAdminUid) {
			this.approveByAdminUid = approveByAdminUid;
		}
		public boolean isCompleteReg() {
			return completeReg;
		}
		public void setCompleteReg(boolean completeReg) {
			this.completeReg = completeReg;
		}
		/**
		 * @return the secondaryContactName
		 */
		public String getSecondaryContactName() {
			return secondaryContactName;
		}
		/**
		 * @param secondaryContactName the secondaryContactName to set
		 */
		public void setSecondaryContactName(String secondaryContactName) {
			this.secondaryContactName = secondaryContactName;
		}
		/**
		 * @return the primaryName
		 */
		public String getPrimaryName() {
			return primaryName;
		}
		/**
		 * @param primaryName the primaryName to set
		 */
		public void setPrimaryName(String primaryName) {
			this.primaryName = primaryName;
		}
		public String getNewPassword() {
			return newPassword;
		}
		public void setNewPassword(String newPassword) {
			this.newPassword = newPassword;
		}
		public String getReEnteredPassword() {
			return reEnteredPassword;
		}
		public void setReEnteredPassword(String reEnteredPassword) {
			this.reEnteredPassword = reEnteredPassword;
		}
		public int getSalt() {
			return salt;
		}
		public void setSalt(int salt) {
			this.salt = salt;
		}

		public int getLoginAttempts() {
			return loginAttempts;
		}
		public void setLoginAttempts(int loginAttempts) {
			this.loginAttempts = loginAttempts;
		}
}
