package com.blt.zepho.model;

import java.util.Date;

public class UserLog {
	
	private int logId;
	private int userId;
	private String previousVal;
	private String updatedVal;
	private String moduleName;
	private String comment;
	private Date previousDate;
	private Date changedDate;
	private String changedBy;
	private String status;
	
	
	public UserLog() {
		super();
	}
	
	public int getLogId() {
		return logId;
	}	
	public void setLogId(int logId) {
		this.logId = logId;
	}	
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getPreviousVal() {
		return previousVal;
	}
	public void setPreviousVal(String previousVal) {
		this.previousVal = previousVal;
	}	
	public String getUpdatedVal() {
		return updatedVal;
	}
	public void setUpdatedVal(String updatedVal) {
		this.updatedVal = updatedVal;
	}
	public String getModuleName() {
		return moduleName;
	}
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	public Date getPreviousDate() {
		return previousDate;
	}
	public void setPreviousDate(Date previousDate) {
		this.previousDate = previousDate;
	}
	public Date getChangedDate() {
		return changedDate;
	}
	public void setChangedDate(Date changedDate) {
		this.changedDate = changedDate;
	}
	public String getChangedBy() {
		return changedBy;
	}
	public void setChangedBy(String changedBy) {
		this.changedBy = changedBy;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}	
	
}
