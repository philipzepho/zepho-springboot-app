package com.blt.zepho.model;

public class UserDetails {
	
	
	private String mobileNum;
	private String emergencyNum;
	private String email;
	private String rescuer1;
	private String rescuer2;
	private String rescuer3;
	private String rescuer4;
	private String fullName;	
	private String userUid;
	private String label; //for UI  rescuer autoloading purpose 
	private String password;
	private String role;
	private boolean approved;
	private String organization;
	
	
	
	public String getEmergencyNum() {
		return emergencyNum;
	}
	public void setEmergencyNum(String emergencyNum) {
		this.emergencyNum = emergencyNum;
	}
	
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getMobileNum() {
		return mobileNum;
	}
	public void setMobileNum(String mobileNum) {
		this.mobileNum = mobileNum;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getRescuer1() {
		return rescuer1;
	}
	public void setRescuer1(String rescuer1) {
		this.rescuer1 = rescuer1;
	}
	public String getRescuer2() {
		return rescuer2;
	}
	public void setRescuer2(String rescuer2) {
		this.rescuer2 = rescuer2;
	}
	public String getRescuer3() {
		return rescuer3;
	}
	public void setRescuer3(String rescuer3) {
		this.rescuer3 = rescuer3;
	}
	public String getRescuer4() {
		return rescuer4;
	}
	public void setRescuer4(String rescuer4) {
		this.rescuer4 = rescuer4;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getUserUid() {
		return userUid;
	}
	public void setUserUid(String userUid) {
		this.userUid = userUid;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public boolean isApproved() {
		return approved;
	}
	public void setApproved(boolean approved) {
		this.approved = approved;
	}
	public String getOrganization() {
		return organization;
	}
	public void setOrganization(String organization) {
		this.organization = organization;
	}
	

}
