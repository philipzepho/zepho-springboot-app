package com.blt.zepho.model;

public class NewTrip {
	private String userUid;
	private String tripId;
	
	private String createdAt;
	private double sourceLatitude;
	private double sourceLongitude;
	private double destinationLatitude;
	private double destinaionLongitude;
	private String sourcePlaceName ;
	private String destinationPlaceName ;
	private boolean isFinished;
	
	public String getTripId() {
		return tripId;
	}
	public void setTripId(String tripId) {
		this.tripId = tripId;
	}
	public String getUserUid() {
		return userUid;
	}
	public void setUserUid(String userUid) {
		this.userUid = userUid;
	}
	public String getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	public double getSourceLatitude() {
		return sourceLatitude;
	}
	public void setSourceLatitude(double sourceLatitude) {
		this.sourceLatitude = sourceLatitude;
	}
	public double getSourceLongitude() {
		return sourceLongitude;
	}
	public void setSourceLongitude(double sourceLongitude) {
		this.sourceLongitude = sourceLongitude;
	}
	public double getDestinationLatitude() {
		return destinationLatitude;
	}
	public void setDestinationLatitude(double destinationLatitude) {
		this.destinationLatitude = destinationLatitude;
	}
	public double getDestinaionLongitude() {
		return destinaionLongitude;
	}
	public void setDestinaionLongitude(double destinaionLongitude) {
		this.destinaionLongitude = destinaionLongitude;
	}
	public String getSourcePlaceName() {
		return sourcePlaceName;
	}
	public void setSourcePlaceName(String sourcePlaceName) {
		this.sourcePlaceName = sourcePlaceName;
	}
	public String getDestinationPlaceName() {
		return destinationPlaceName;
	}
	public void setDestinationPlaceName(String destinationPlaceName) {
		this.destinationPlaceName = destinationPlaceName;
	}
	public boolean isFinished() {
		return isFinished;
	}
	public void setFinished(boolean isFinished) {
		this.isFinished = isFinished;
	}
	
	

}
