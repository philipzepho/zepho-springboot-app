package com.blt.zepho.model;
// create table Rescuer(userUid varchar(50), fullName varchar(40),deviceId varchar(100), email varchar(30));
public class Rescuer {
String userUid;
String fullName; 
String deviceId; 
String email;
String label;//for UI  rescuer autoloading purpose 
String org;
String mobile;

public String getLabel() {
	return label;
}
public void setLabel(String label) {
	this.label = label;
}
public String getUserUid() {
	return userUid;
}
public void setUserUid(String userUid) {
	this.userUid = userUid;
}
public String getFullName() {
	return fullName;
}
public void setFullName(String fullName) {
	this.fullName = fullName;
}
public String getDeviceId() {
	return deviceId;
}
public void setDeviceId(String deviceId) {
	this.deviceId = deviceId;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public String getOrg() {
	return org;
}
public void setOrg(String org) {
	this.org = org;
}
public String getMobile() {
	return mobile;
}
public void setMobile(String mobile) {
	this.mobile = mobile;
}



}
