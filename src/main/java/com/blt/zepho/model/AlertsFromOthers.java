package com.blt.zepho.model;

public class AlertsFromOthers {
	
	private String uuid;
	private String fromUserName;
	private String fromUserUid;
	private String toUserUid;
	private boolean isWatched;
	private long createdAt;
	private String message;
	private String fromUserRole;
	private String attachmentPath;
	private String imageData;
	
	
	
	public AlertsFromOthers(String uuid, String fromUserName, String fromUserUid, String toUserUid, boolean isWatched,
			long createdAt, String message,String fromUserRole) {
		super();
		this.uuid = uuid;
		this.fromUserName = fromUserName;
		this.fromUserUid = fromUserUid;
		this.toUserUid = toUserUid;
		this.isWatched = isWatched;
		this.createdAt = createdAt;
		this.message = message;
		this.fromUserRole=fromUserRole;
	}
	
	public String getFromUserRole() {
		return fromUserRole;
	}
	public void setFromUserRole(String fromUserRole) {
		this.fromUserRole = fromUserRole;
	}
	public AlertsFromOthers(){
		
	}
	
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public String getFromUserName() {
		return fromUserName;
	}
	public void setFromUserName(String fromUserName) {
		this.fromUserName = fromUserName;
	}
	public String getFromUserUid() {
		return fromUserUid;
	}
	public void setFromUserUid(String fromUserUid) {
		this.fromUserUid = fromUserUid;
	}
	public String getToUserUid() {
		return toUserUid;
	}
	public void setToUserUid(String toUserUid) {
		this.toUserUid = toUserUid;
	}
	public boolean isWatched() {
		return isWatched;
	}
	public void setWatched(boolean isWatched) {
		this.isWatched = isWatched;
	}
	public long getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(long createdAt) {
		this.createdAt = createdAt;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

	public String getAttachmentPath() {
		return attachmentPath;
	}

	public void setAttachmentPath(String attachmentPath) {
		this.attachmentPath = attachmentPath;
	}

	public String getImageData() {
		return imageData;
	}

	public void setImageData(String imageData) {
		this.imageData = imageData;
	}
	
	
	
}
