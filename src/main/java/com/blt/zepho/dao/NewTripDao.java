package com.blt.zepho.dao;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.blt.zepho.jdbc.CreateJDBCConnection;
import com.blt.zepho.model.NewTrip;

@Repository
public class NewTripDao {
	
	static Logger log = Logger.getLogger(NewTripDao.class.getName());
	
	@Autowired
	CreateJDBCConnection createJDBCConnection;
	
	public String addNewTrip(NewTrip newTrip){
		

		
		
		Connection connection = null;
	       try {           
	           connection = createJDBCConnection.getConnection();
	           
	           
	           // the mysql insert statement
	           String query = "insert into trips (userUid, createdAt, sourceLatitude, sourceLongitude, destinationLatitude, destinaionLongitude, sourcePlaceName, destinationPlaceName,isFinished,tripId) "+ " values(?,?,?,?,?,?,?,?,?,?)" ;

	           // create the mysql insert preparedstatement
	           java.sql.PreparedStatement preparedStmt =  connection.prepareStatement(query);
	           preparedStmt.setString(1, newTrip.getUserUid());
	           preparedStmt.setString(2, newTrip.getCreatedAt());
	           preparedStmt.setDouble(3, newTrip.getSourceLatitude());
	           preparedStmt.setDouble(4, newTrip.getSourceLongitude());
	           preparedStmt.setDouble(5, newTrip.getDestinationLatitude());
	           preparedStmt.setDouble(6, newTrip.getDestinaionLongitude());
	           preparedStmt.setString(7, newTrip.getSourcePlaceName());
	           preparedStmt.setString(8, newTrip.getDestinationPlaceName());
	           preparedStmt.setBoolean(9, newTrip.isFinished());
	           preparedStmt.setString(10, newTrip.getTripId());
	           
	           preparedStmt.executeUpdate();
     
	           connection.close();
	           
	           
	       } catch (SQLException e) {
	           log.fatal(e.getMessage());
	           log.debug("FAILED TO ADD NEW TRIP");
	           return "FAILED TO ADD NEW TRIP";
	       } finally {
	           if (connection != null) {
	               try {
	                   connection.close();
	               } catch (SQLException e) {
	                   e.printStackTrace();
	               }
	           }
	       }
	       log.debug("NEW TRIP ADDED");
		
		return "NEW TRIP ADDED";
	}
	
	public String addCurrentTrip(NewTrip newTrip){
		

		Connection connection = null;
		
	      
	       try {           
	           connection = createJDBCConnection.getConnection();
	           
	           
	           if(!checkRunningTrip(newTrip.getUserUid())){
	        	   
	        	   log.debug("FIRST TIME IN CURRENT TRIPS");
	        	   
	        	   try{
	        		   String query = "insert into currenttrips (userUid, tripId,isFinished) "+ " values(?,?,?)" ;

			           // create the mysql insert preparedstatement
			           java.sql.PreparedStatement preparedStmt =  connection.prepareStatement(query);
			           preparedStmt.setString(1, newTrip.getUserUid());
			           preparedStmt.setString(2, newTrip.getTripId());
			           preparedStmt.setBoolean(3, newTrip.isFinished());
			           
			           preparedStmt.executeUpdate();
	        		   
	        	   }catch(Exception e){
	        		   e.printStackTrace();
	        	   }
	        	  
	           
	           }else{
	        	   
	        	   log.debug("UPDATING IN CURRENT TRIPS");
	        	   
	        	   String query = "update currenttrips SET tripId=? , isFinished=false where  userUid= ? " ;

		           // create the mysql insert preparedstatement
		           java.sql.PreparedStatement preparedStmt =  connection.prepareStatement(query);
		           preparedStmt.setString(1,newTrip.getTripId());
		           preparedStmt.setString(2,newTrip.getUserUid());
		                 	           		           
		           preparedStmt.executeUpdate();
	        	   
	           }
	           
	           // the mysql insert statement
	          
  
	           connection.close();
	           
	           
	       } catch (SQLException e) {
	           log.fatal(e.getMessage());
	           log.debug("FAILED TO ADD CURRENT RUNNING TRIP");
	           return "FAILED TO ADD CURRENT RUNNING TRIP";
	       } finally {
	           if (connection != null) {
	               try {
	                   connection.close();
	               } catch (SQLException e) {
	                   e.printStackTrace();
	               }
	           }
	       }
		
		log.debug("RUNNING TRIP UPDATED");
		return "RUNNING TRIP UPDATED";
	}
	
	
	public boolean checkCurrentTrip(String userUid){
		Connection connection = null;
		ResultSet resultSet=null;
		boolean result=true;
		
		 try {           
	           connection = createJDBCConnection.getConnection();
	           
	           
	           // the mysql insert statement
	           String query = "select * from currenttrips where userUid=?" ;

	           // create the mysql insert preparedstatement
	           java.sql.PreparedStatement preparedStmt =  connection.prepareStatement(query);
	           preparedStmt.setString(1, userUid);
	           
	           
	            resultSet=preparedStmt.executeQuery();
	            
	           
	            while (resultSet.next()) {
	            	result=resultSet.getBoolean("isFinished");
					
				}

	           
	           connection.close();
	          log.debug("CURRENT RUNNING TRIP SUCCESS");
	         return result;
	        	 
	        	
	           }catch (SQLException e) {
	           
	        	  log.fatal(e.getMessage());
		    	   log.debug("CURRENT RUNNING TRIP ERROR");
		           return true;
		           
	           } finally {
	           if (connection != null) {
	               try {
	                   connection.close();
	               } catch (SQLException e) {
	                   e.printStackTrace();
	               }
	           }
	       }
		
		
		
	}
	
	public boolean checkRunningTrip(String userUid){
		Connection connection = null;
		ResultSet resultSet= null;
		boolean result=false;
		
		 try {           
	           connection = createJDBCConnection.getConnection();
	           
	           
	           // the mysql insert statement
	           String query = "select * from currenttrips where userUid=?" ;

	           // create the mysql insert preparedstatement
	           java.sql.PreparedStatement preparedStmt =  connection.prepareStatement(query);
	           preparedStmt.setString(1, userUid);
	           
	           
	           
	           
	           
	           resultSet=preparedStmt.executeQuery();
	            
	           
	            while (resultSet.next()) {
	            	result=true;
					
				}

	           
	           connection.close();
	      
	           
	           return result;
  
	           
	           
	           
	           
	       } catch (SQLException e) {
	           
	           return false;
	       } finally {
	           if (connection != null) {
	               try {
	                   connection.close();
	               } catch (SQLException e) {
	                   e.printStackTrace();
	               }
	           }
	       }
		
		
		
	}
	
	
	
	public String endCurrentTrip(String userUid){
		Connection connection = null;
		log.debug("userUid " + userUid);
		 try {           
	           connection = createJDBCConnection.getConnection();
	           
	           
	           // the mysql insert statement
	           String query = "update currenttrips SET isFinished=true where userUid= ? ";

	           // create the mysql insert preparedstatement
	           java.sql.PreparedStatement preparedStmt =  connection.prepareStatement(query);
	           
	           preparedStmt.setString(1, userUid);
	           
	           preparedStmt.executeUpdate();
	           
	           connection.close();
	           
	           log.debug("TRIP ENDED SUCCESSFULLY");
	          return "TRIP ENDED SUCCESSFULLY";
   
	           
	           
	           
	           
	       } catch (SQLException e) {
	    	   e.printStackTrace();
	           log.debug("END TRIP FAILED");
	           return "CANNOT ABLE TO END TRIP";
	       } finally {
	           if (connection != null) {
	               try {
	                   connection.close();
	               } catch (SQLException e) {
	                   e.printStackTrace();
	               }
	           }
	       }
		
		
		
	}

}


