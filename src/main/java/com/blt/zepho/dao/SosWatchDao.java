package com.blt.zepho.dao;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.blt.zepho.jdbc.CreateJDBCConnection;
import com.blt.zepho.model.SosWatch;

@Repository
public class SosWatchDao {
	static Logger log = Logger.getLogger(SosWatchDao.class.getName());
	
	@Autowired
	CreateJDBCConnection createJDBCConnection;
	
	public String addSosWatchData(SosWatch sosWatch) {
		
		
	       Connection connection = null;
	
	      
	       try {           
	           connection = createJDBCConnection.getConnection();
	       
	           // the mysql insert statement
	           String query = "insert into soswatch (uuid,userUid, isWatched) "+ " values(?,?,?)" ;

	           // create the mysql insert preparedstatement
	           java.sql.PreparedStatement preparedStmt =  connection.prepareStatement(query);
	           preparedStmt.setString(1, sosWatch.getUuid());
	           preparedStmt.setString(2, sosWatch.getUserUid());
	           preparedStmt.setBoolean(3, sosWatch.isWatched());
	           

	           // execute the preparedstatement
	           
	           preparedStmt.executeUpdate();
        
	           connection.close();
	           
	           
	       } catch (SQLException e) {
	           log.fatal(e.getMessage());
	           log.debug("INSERTION_FIAL");
	           return "INSERTION_FIAL";
	       } finally {
	           if (connection != null) {
	               try {
	                   connection.close();
	               } catch (SQLException e) {
	                   e.printStackTrace();
	               }
	           }
	       }
		return "SUCCESS";
	}

}
