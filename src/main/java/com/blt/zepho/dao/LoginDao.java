package com.blt.zepho.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.blt.zepho.jdbc.CreateJDBCConnection;
import com.blt.zepho.services.Password;
import com.blt.zepho.model.Registration;

@Repository
public class LoginDao 
{
	static Logger log = Logger.getLogger(LoginDao.class.getName());
	@Autowired
	CreateJDBCConnection createJDBCConnection;
	JdbcTemplate jdbcTemplate;
	
	@Autowired
	public LoginDao(DataSource dataSource) {
		jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	public Registration loginUser(Registration reg) {
			
		// to get the data from database
		ResultSet rs = null;
		//database connection
		Connection connection = null;
		//passing the data to query
		PreparedStatement myStmt = null;

		Registration userzz =null;
		try {
			String mob = reg.getMobileNum();
			String email = reg.getEmail();
			String query = "";
			String emailOrMob = "";
			
			if(!email.equalsIgnoreCase("")){
				query = " email=? ";
				emailOrMob = email;
			}else if(!mob.equalsIgnoreCase("0")){
				query = " mobileNum=? ";
				emailOrMob = mob;
			}

			connection = createJDBCConnection.getConnection();

			myStmt = connection.prepareStatement("select * from registration where ("+query+" and isApproved=true) "
							+ " or ("+query+")");
		
			//passing the values
			myStmt.setString(1, emailOrMob);
			myStmt.setString(2, emailOrMob);
			
			
		
			rs = myStmt.executeQuery();
			//getting the values
			while (rs.next()) {
				userzz= new Registration();
				userzz.setFirstName(rs.getString("firstName"));
				userzz.setLastName(rs.getString("lastName"));
				/*userzz.setRescuer1(rs.getString("Rescuer1"));
				userzz.setRescuer2(rs.getString("Rescuer1"));
				userzz.setRescuer3(rs.getString("Rescuer3"));
				userzz.setRescuer4(rs.getString("Rescuer4"));*/
				userzz.setRole(rs.getString("role"));
				userzz.setEmail(rs.getString("email"));
				userzz.setFullName(rs.getString("fullName"));
				userzz.setOrganization(rs.getString("organization"));
				userzz.setUserUid(rs.getString("userUid"));
				userzz.setEmergencyNum(rs.getString("emergencyNum"));
				userzz.setEmailVerified(rs.getBoolean("emailVerified"));
				userzz.setIsApproved(rs.getBoolean("isApproved"));
				userzz.setCompleteReg(rs.getBoolean("completeReg"));
				userzz.setApproveByAdminUid(rs.getString("approveByAdminUid"));
				userzz.setPincode(rs.getInt("pincode"));
				userzz.setLoginAttempts(rs.getInt("loginAttempts"));
				
				boolean oldUsersPassword=reg.getPassword().equals(rs.getString("password"));
				if(oldUsersPassword){
					log.error("Password Matched");
				}else{
					try{
						boolean isPasswordCorrect=Password.checkPassword(reg.getPassword(), rs.getString("password"));
						if(isPasswordCorrect){
							log.debug("Password Matched");
						}else{
							userzz=null;
						}
					}catch(Exception e){
						log.fatal(e.getMessage());
						userzz=null;						
					}					
				}				
			}			
			connection.close();

		} catch (SQLException e) {
				log.fatal("Error Message: " + e.getMessage());
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					log.fatal(e.getMessage());
				}
			}
		}
		return userzz;

	}

	public Registration verifySecurityQuestions(Registration reg) {
		ResultSet rs = null;
		Connection connection = null;
		PreparedStatement myStmt = null;

		Registration userzz =null;
		try {
			String mobileNum = reg.getMobileNum();
			String email = reg.getEmail();			
		/*  String nameOfChurch = reg.getNameOfChurch();
			Date dob = reg.getDob();	*/	
						
			if(mobileNum.equalsIgnoreCase("0")){
				return userzz;
			}
			if(email.equalsIgnoreCase("")){
				return userzz;
			}
			
		/*	if(!nameOfChurch.equalsIgnoreCase("")){
				query += " nameOfChurch='"+nameOfChurch+"' and ";
			}
			if(dob != null){
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");  
	            String strDate = dateFormat.format(dob);  
				query += " dob='"+strDate+"'";
			}*/

			connection = createJDBCConnection.getConnection();
			myStmt = connection.prepareStatement("select * from registration where mobileNum=? and email=?");	
			myStmt.setString(1, mobileNum);
			myStmt.setString(2, email);
			rs = myStmt.executeQuery();
			 if (rs.next()) {
		            do {
		            	userzz= new Registration();	
						userzz.setUserUid(rs.getString("userUid"));
						userzz.setMobileNum(rs.getString("mobileNum"));	            
					} while (rs.next());
		        } else {
		        	userzz = null;
		        } 			
			connection.close();
		} catch (SQLException e) {
			log.fatal("Error Message: " + e.getMessage());
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					log.fatal(e.getMessage());
				}
			}
		}
		return userzz;

	}
	
	public Registration getUserDetailsByUid(Registration reg) {
		ResultSet rs = null;
		Connection connection = null;
		PreparedStatement myStmt = null;

		Registration userzz =null;
		try {
			
			connection = createJDBCConnection.getConnection();
			myStmt = connection.prepareStatement("select * from registration where userUid=?");
			myStmt.setString(1,  reg.getUserUid());
			rs = myStmt.executeQuery();
			 if (rs.next()) {
		            do {
		            	userzz= new Registration();
		            	userzz.setId(rs.getInt("id"));
		            	userzz.setPassword(rs.getString("password"));
						userzz.setFirstName(rs.getString("firstName"));
						userzz.setLastName(rs.getString("lastName"));
						userzz.setRescuer1(rs.getString("Rescuer1"));
						userzz.setRescuer2(rs.getString("Rescuer1"));
						userzz.setRescuer3(rs.getString("Rescuer3"));
						userzz.setRescuer4(rs.getString("Rescuer4"));
						userzz.setPassword(rs.getString("password"));
						userzz.setRole(rs.getString("role"));
						userzz.setEmail(rs.getString("email"));
						userzz.setFullName(rs.getString("fullName"));
						userzz.setOrganization(rs.getString("organization"));
						userzz.setUserUid(rs.getString("userUid"));
						userzz.setMobileNum(rs.getString("mobileNum"));
						userzz.setEmergencyNum(rs.getString("emergencyNum"));
						userzz.setEmailVerified(rs.getBoolean("emailVerified"));
						userzz.setIsApproved(rs.getBoolean("isApproved"));
						userzz.setCompleteReg(rs.getBoolean("completeReg"));		            
					} while (rs.next());
		        } else {
		        	userzz = null;
		        } 			
			connection.close();
		} catch (SQLException e) {
			log.fatal("Error Message: " + e.getMessage());
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					log.fatal(e.getMessage());
				}
			}
		}
		return userzz;

	}
	
	public Registration verifyUser(Registration reg) {
		log.debug("EMAIL:  " +reg.getEmail());		
		// to get the data from database
		ResultSet rs = null;
		//database connection
		Connection connection = null;
		//passing the data to query
		PreparedStatement myStmt = null;

		Registration userzz =null;
		try {
			String mob = reg.getMobileNum();
			String query = "";
			
			if(mob.equalsIgnoreCase("0")){
				return userzz; 
			}

			connection = createJDBCConnection.getConnection();
			myStmt = connection.prepareStatement("select * from registration where (mobileNum=? and isApproved=true) "
							+ " or (mobileNum=?)");		
			myStmt.setString(1, mob);
			myStmt.setString(2, mob);
			rs = myStmt.executeQuery();
			 if (rs.next()) {
		            do {
		            	userzz= new Registration();						
						userzz.setMobileNum(rs.getString("mobileNum"));	            
					} while (rs.next());
		        } else {
		        	userzz = null;
		        } 			
			connection.close();
		} catch (SQLException e) {
			log.fatal("Error Message: " + e.getMessage());
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					log.fatal(e.getMessage());
				}
			}
		}
		return userzz;

	}
	
	public Registration verifyUserByMobile(Registration reg) {
		log.debug("EMAIL:  " +reg.getEmail());		
		// to get the data from database
		ResultSet rs = null;
		//database connection
		Connection connection = null;
		//passing the data to query
		PreparedStatement myStmt = null;

		Registration userzz =null;
		try {
			String mob = reg.getMobileNum();
			String query = "";
			
			if(mob.equalsIgnoreCase("0")){
				return userzz;
			}

			connection = createJDBCConnection.getConnection();
			myStmt = connection.prepareStatement("select * from registration where (mobileNum=? and isApproved=true) "
							+ " or (mobileNum=?)");	
			myStmt.setString(1, mob);
			myStmt.setString(2, mob);
			rs = myStmt.executeQuery();
			 if (rs.next()) {
		            do {
		            	userzz= new Registration();						
						userzz.setMobileNum(rs.getString("mobileNum"));		            
					} while (rs.next());
		        } else {
		        	userzz = null;
		        } 			
			connection.close();
		} catch (SQLException e) {
			log.fatal("Error Message: " + e.getMessage());
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					log.fatal(e.getMessage());
				}
			}
		}
		return userzz;

	}
	
	public Registration loginAuthByUserName(String username) { // username might be mobile number or email
		
		// to get the data from database
		ResultSet rs = null;
		//database connection
		Connection connection = null;
		//passing the data to query
		PreparedStatement myStmt = null;

		Registration userzz =null;
		try {			

			boolean isNum = isNumeric(username);
			connection = createJDBCConnection.getConnection();
			if(isNum){
				myStmt = connection.prepareStatement("select * from registration where mobileNum=?");	
			}else{
				myStmt = connection.prepareStatement("select * from registration where email=?");
			}			
		
			//passing the values
			myStmt.setString(1, username);
			//myStmt.setString(2, username);
			
			
		
			rs = myStmt.executeQuery();
			//getting the values
			while (rs.next()) {
				userzz= new Registration();
				userzz.setPassword(rs.getString("password"));
				userzz.setRole(rs.getString("role"));
				userzz.setEmail(rs.getString("email"));
				userzz.setIsApproved(rs.getBoolean("isApproved"));				
							
			}			
			connection.close();

		} catch (SQLException e) {
				log.fatal("Error Message: " + e.getMessage());
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					log.fatal(e.getMessage());
				}
			}
		}
		return userzz;

	}
	
	private static boolean isNumeric(String str){
	    for (char c : str.toCharArray())
	    {
	        if (!Character.isDigit(c)) return false;
	    }
	    return true;
	}

	public void updateLoginAttempts(String username) {
		// to get the data from database
		ResultSet rs = null;
		//database connection
		Connection connection = null;
		//passing the data to query
		PreparedStatement myStmt = null;

		Registration userzz =null;
		try {			

			boolean isNum = isNumeric(username);
			connection = createJDBCConnection.getConnection();
			if(isNum){
				myStmt = connection.prepareStatement("select * from registration where mobileNum=? and loginAttempts < 3");	
			}else{
				myStmt = connection.prepareStatement("select * from registration where email=? and loginAttempts < 3");
			}					
			//passing the values
			myStmt.setString(1, username);		
		
			rs = myStmt.executeQuery();
			//getting the values
			while (rs.next()) {
				userzz= new Registration();
				userzz.setUserUid(rs.getString("userUid"));
				userzz.setLoginAttempts(rs.getInt("loginAttempts") + 1);
			}			
			
			if(userzz != null && userzz.getUserUid() != null){
				String update = "update registration SET loginAttempts=? where userUid=?"; 

				// create the mysql insert preparedstatement
				java.sql.PreparedStatement preparedStmt = connection.prepareStatement(update);
				preparedStmt.setInt(1, userzz.getLoginAttempts());
				preparedStmt.setString(2, userzz.getUserUid());		

				// execute the preparedstatement
				preparedStmt.execute();
			}
			
			connection.close();

		} catch (SQLException e) {
				log.fatal("Error Message: " + e.getMessage());
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					log.fatal(e.getMessage());
				}
			}
		}
		
	}

}
