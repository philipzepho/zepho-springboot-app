package com.blt.zepho.dao;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.blt.zepho.jdbc.CreateJDBCConnection;
import com.blt.zepho.model.Registration;
import com.blt.zepho.model.UserDetails;

@Repository
public class RescuerDao {

	static Logger log = Logger.getLogger(RescuerDao.class.getName());
	
	@Autowired
	CreateJDBCConnection createJDBCConnection;
	
	public int addRescuer(Registration reg) {
		// ResultSet rs = null;
		Connection connection = null;

		try {
			connection = createJDBCConnection.getConnection();

			// the mysql insert statement
			String query = "insert into rescuer (userUid, fullName, deviceId, email,organization) " + " values(?,?,?,?,?)";

			// create the mysql insert preparedstatement
			java.sql.PreparedStatement preparedStmt = connection.prepareStatement(query);
			preparedStmt.setString(1, reg.getUserUid());
			preparedStmt.setString(2, reg.getFullName());
			preparedStmt.setString(3, reg.getDeviceId());
			preparedStmt.setString(4, reg.getEmail());
			preparedStmt.setString(5, reg.getOrganization());
			

			// execute the preparedstatement
			preparedStmt.execute();

			connection.close();

		} catch (SQLException e) {
			log.fatal(e.getMessage());
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					log.fatal(e.getMessage());
				}
			}
		}
		return 1;
	}
	
	public int updateRescuer(UserDetails userDetails) {
		
		
		log.debug("INSIDE UPDATE RESCUER INFO");
		Connection connection = null;

		try {
			connection = createJDBCConnection.getConnection();

			
			
			// the mysql insert statement
			String query = "update rescuer SET email=? where userUid=? "; 

			// create the mysql insert preparedstatement
			java.sql.PreparedStatement preparedStmt = connection.prepareStatement(query);
			preparedStmt.setString(1, userDetails.getEmail());
			preparedStmt.setString(2, userDetails.getUserUid());
			
			

			// execute the preparedstatement
			preparedStmt.execute();

			connection.close();

		} catch (SQLException e) {
			log.fatal(e.getMessage());
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					log.fatal(e.getMessage());
				}
			}
		}
		return 1;
	}
}
