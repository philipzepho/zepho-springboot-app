package com.blt.zepho.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.blt.zepho.jdbc.CreateJDBCConnection;

@Repository
public class ListRescuerDeviceIdDao {
	String deviceId;
	static Logger log = Logger.getLogger(ListRescuerDeviceIdDao.class.getName());
	
	@Autowired
	CreateJDBCConnection createJDBCConnection;
	
	public String GetRescuerDeviceList(String reg) {

		ResultSet rs = null;
		Connection connection = null;
		PreparedStatement myStmt = null;

		try {

			connection = createJDBCConnection.getConnection();

			myStmt = connection.prepareStatement("select deviceId , email from registration where userUid= ? ");

			myStmt.setString(1, reg);
			rs = myStmt.executeQuery();

			while (rs.next()) {

				deviceId = rs.getString("deviceId");

			}

			log.debug("Device Id: "+ this.deviceId);

			connection.close();

		} catch (SQLException e) {
			log.fatal(e);
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					log.fatal(e);
				}
			}
		}
		return deviceId;

	}

}
