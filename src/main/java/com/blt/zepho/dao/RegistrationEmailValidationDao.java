package com.blt.zepho.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.blt.zepho.jdbc.CreateJDBCConnection;
import com.blt.zepho.model.Registration;

@Repository
public class RegistrationEmailValidationDao {
	static Logger log = Logger.getLogger(RegistrationEmailValidationDao.class.getName());
	
	@Autowired
	CreateJDBCConnection createJDBCConnection;

	public int registerUser(Registration reg) {
		// ResultSet rs = null;
		
		log.debug("reg " + reg.getEmail());
		Connection connection = null;
		int count = 0;
		PreparedStatement st = null;
		try {
			connection = createJDBCConnection.getConnection();

			try {

				connection = createJDBCConnection.getConnection();

				st = connection.prepareStatement("select count(*) from registration where email=? or userName=?");
                st.setString(1, reg.getEmail());
                st.setString(2, reg.getUserName());	
				                					
				ResultSet res = st.executeQuery();
				
				while (res.next()) {
					count = res.getInt(1);
				}
				
			} catch (SQLException e) {
				log.fatal(e.getMessage());
			}
		} catch (Exception e) {
			log.fatal(e.getMessage());
		}
		return count;
	}
	
	public String checkPublicReg(Registration reg) {
		// ResultSet rs = null;
		
		log.debug("reg " + reg.getEmail());
		Connection connection = null;
		String count = "";
		PreparedStatement st = null;
		try {
			connection = createJDBCConnection.getConnection();

			try {

				connection = createJDBCConnection.getConnection();
				String emailQuery = "";
				if(reg.getEmail() != "" && reg.getEmail() != null){
					emailQuery = " or email=? ";
				}

				st = connection.prepareStatement("select email, mobileNum from registration where mobileNum=? "+emailQuery);                	
				st.setString(1, reg.getMobileNum());
				if(emailQuery != ""){
					st.setString(2, reg.getEmail());	
				}				
				
				ResultSet res = st.executeQuery();
				
				while (res.next()) {
					String email = res.getString("email");
					String mob = res.getString("mobileNum");
					if(email.equalsIgnoreCase(reg.getEmail())){
						count = "Email";
					}else if(mob.equalsIgnoreCase(reg.getMobileNum())){
						count = "Mobile Number";
					}
				}
				
			} catch (SQLException e) {
				log.fatal(e.getMessage());
			}
		} catch (Exception e) {
			log.fatal(e.getMessage());
		}
		return count;
	}
	public String checkMobileNumber(String phNum) {
		// ResultSet rs = null;
		Connection connection = null;
		String count = "";
		PreparedStatement st = null;
		try {
			connection = createJDBCConnection.getConnection();
				st = connection.prepareStatement("select mobileNum from registration where mobileNum=?");
				st.setString(1, phNum);
				ResultSet res = st.executeQuery();
				while (res.next()) {
					String mob = res.getString("mobileNum");
					if(mob.equalsIgnoreCase(phNum)){
						count = "Mobile Number";
					}
				}
		} catch (Exception e) {
			log.fatal(e.getMessage());
		}
		return count;
	}
	
	public String checkEmailExists(String email) {
		// ResultSet rs = null;
		Connection connection = null;
		String count = "";
		PreparedStatement st = null;
		try {
			connection = createJDBCConnection.getConnection();
				st = connection.prepareStatement("select email from registration where email=?");
				st.setString(1, email);
				ResultSet res = st.executeQuery();
				while (res.next()) {
					String emailexists = res.getString("email");
					if(emailexists.equalsIgnoreCase(email)){
						count = "email";
					}
				}
		} catch (Exception e) {
			log.fatal(e.getMessage());
		}
		return count;
	}
}
