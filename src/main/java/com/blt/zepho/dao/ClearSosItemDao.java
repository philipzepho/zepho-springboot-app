package com.blt.zepho.dao;

import java.sql.Connection;

import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.blt.zepho.jdbc.CreateJDBCConnection;
import com.blt.zepho.model.ClearSosListItem;

@Repository
public class ClearSosItemDao {
	
	static Logger log = Logger.getLogger(ClearSosItemDao.class.getName());
	
	@Autowired
	CreateJDBCConnection createJDBCConnection;
	
	public boolean clearSosItem(ClearSosListItem clearSosListItem){
		
		
		Connection connection = null;
		boolean result=true;
		
	      
	       try {           
	           connection = CreateJDBCConnection.getConnection();
	       
	           // the mysql insert statement
	           String query = "update soswatch SET isWatched=true where  userUid= ? and uuid= ? " ;

	           // create the mysql insert preparedstatement
	           java.sql.PreparedStatement preparedStmt =  connection.prepareStatement(query);
	           preparedStmt.setString(1, clearSosListItem.getUserUid());
	           preparedStmt.setString(2, clearSosListItem.getUuid());
	           
	           
	           preparedStmt.executeUpdate();
     
	           connection.close();
	           
	           
	       } catch (SQLException e) {
	           log.fatal(e);
	           return false;
	       } finally {
	           if (connection != null) {
	               try {
	                   connection.close();
	               } catch (SQLException e) {
	            	   log.fatal(e);
	               }
	           }
	       }
		
		return result;
		
	}

}
