package com.blt.zepho.dao;

import java.sql.Connection;

import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.blt.zepho.jdbc.CreateJDBCConnection;
import com.blt.zepho.model.ClearSosListItem;
import com.blt.zepho.model.ItemId;

@Repository
public class ClearDoNotGoAlertDao {
	
	static Logger log = Logger.getLogger(ClearDoNotGoAlertDao.class.getName());
	@Autowired
	CreateJDBCConnection createJDBCConnection;
	public boolean clearDoNotGoItem(ItemId itemId){

		Connection connection = null;
		boolean result=true;
		
      
       try {           
           connection = createJDBCConnection.getConnection();
       
           // the mysql insert statement
           String query = "update alertsfromothers SET isWatched=true where  uuid= ? " ;

           // create the mysql insert preparedstatement
           java.sql.PreparedStatement preparedStmt =  connection.prepareStatement(query);
           preparedStmt.setString(1, itemId.getUuid()); 
           
           preparedStmt.executeUpdate();
 
           connection.close();
           
           
       } catch (SQLException e) {
           log.fatal(e);
           return false;
       } finally {
           if (connection != null) {
               try {
                   connection.close();
               } catch (SQLException e) {
            	   log.fatal("SQL Exception"+e);
                   e.printStackTrace();
               }
           }
       }
	
	return result;
	
}

}
