package com.blt.zepho.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.blt.zepho.jdbc.CreateJDBCConnection;
import com.blt.zepho.model.Registration;

@Repository
public class RescuerListByUserIdDao {
	static Logger log = Logger.getLogger(RescuerListByUserIdDao.class.getName());
	
	@Autowired
	CreateJDBCConnection createJDBCConnection;
	
	public ArrayList<String> GetRescuerByUserId(String reg) {
		Registration rescuer = new Registration();
		ResultSet rs = null;
		Connection connection = null;
		PreparedStatement myStmt = null;
		ArrayList<Registration> rescuerUserIDList = new ArrayList<Registration>();
		ArrayList<String> stringList= new ArrayList<String>();
		try {

			connection = createJDBCConnection.getConnection();
			// getting each user's rescuers
			// here in the particular query userUid is the user who presses sos button
			myStmt = connection.prepareStatement(
					"select rescuer1 , rescuer2 , rescuer3 , rescuer4 from registration where userUid= ? ");

			myStmt.setString(1, reg);
			rs = myStmt.executeQuery();

			while (rs.next()) {
				
				
				if(rs.getString("rescuer1")==null && rs.getString("rescuer2")==null && rs.getString("rescuer3")==null && rs.getString("rescuer4")==null){
					log.debug("NO RESCUERS FOR THIS USER");
					return stringList;
				}
				
				if( rs.getString("rescuer1")!=null)
				{
					
					if(!rs.getString("rescuer1").equals(""))
						stringList.add(rs.getString("rescuer1"));
				}
				if(rs.getString("rescuer2")!=null )
				{
					if(!rs.getString("rescuer2").equals(""))
						stringList.add(rs.getString("rescuer2"));
				}
				if(rs.getString("rescuer3")!=null )
				{
					if(!rs.getString("rescuer3").equals(""))
						stringList.add(rs.getString("rescuer3"));
				}
				if(rs.getString("rescuer4")!=null )
				{
					
					if(!rs.getString("rescuer4").equals(""))
						stringList.add(rs.getString("rescuer4"));
				}
					

			}

			

			connection.close();

		} catch (SQLException e) {
			log.fatal(e.getMessage());
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					log.fatal(e.getMessage());
				}
			}
		}
		return stringList;

	}
}
