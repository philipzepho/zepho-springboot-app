package com.blt.zepho.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.blt.zepho.jdbc.CreateJDBCConnection;
import com.blt.zepho.services.CommonSerices;
import com.blt.zepho.services.Password;
import com.blt.zepho.model.Registration;
import com.blt.zepho.model.UserLog;
import com.blt.zepho.util.WebKeys;

@Repository
public class RegistrationDao 
{

	static Logger log = Logger.getLogger(RegistrationDao.class.getName());
	
	@Autowired
	CreateJDBCConnection createJDBCConnection;
	
	public int registerUser(Registration reg) {
		// ResultSet rs = null;
	       Connection connection = null;
	       String userUid=UUID.randomUUID().toString();	       
	       reg.setUserUid(userUid);	       
	       reg.setPassword(Password.hashPassword(reg.getPassword()));	
	       Date sqlDob =  null;
	       if(reg.getDob() != null){
	    	   sqlDob = new Date(reg.getDob().getTime());
	       }
	       
	       try {           
	           connection = createJDBCConnection.getConnection();
	       
	           // the mysql insert statement
	           String query = "insert into registration (firstName, lastName, fullName, mobileNum, email, password, role, isApproved,   "
	           		+ "isRejected, addedBy, organization, deviceId, rescuer1, rescuer2, rescuer3, rescuer4,  rescuer5, "
	           		+ "extraField1, extraField2, extraField3,userName,userUid,addedById,state,city,division,rescuertype,"
	           		+ "emergencyNum,middleName,gender,address,area,alternateEmail,telephone,whatsappNumber,aadhaarNumber,"
	           		+ "profession,contactNotOwn,secondaryContact,bloodGroup,churchDetails,nameOfChurch,memberSince,pastorName,"
	           		+ "pastorOrgContact,pincode,district,approvedByAdmin,approvedBySuAdmin,approveByAdminUid, dob) "
	           		+ " values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)" ;

	           // create the mysql insert preparedstatement
	           java.sql.PreparedStatement preparedStmt =  connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
	           preparedStmt.setString(1, reg.getFirstName());
	           preparedStmt.setString(2, reg.getLastName());
	           preparedStmt.setString(3, reg.getFullName());
	           preparedStmt.setString(4, reg.getMobileNum());
	           
	           preparedStmt.setString(5, reg.getEmail());
	           preparedStmt.setString(6, reg.getPassword());
	           preparedStmt.setString(7, reg.getRole());
	           preparedStmt.setBoolean(8, reg.getIsApproved());
	           
	           preparedStmt.setBoolean(9, reg.getIsRejected());
	           preparedStmt.setString(10, reg.getAddedBy());
	           preparedStmt.setString(11, reg.getOrganization());
	           preparedStmt.setString(12, reg.getDeviceId());
	           
	           preparedStmt.setString(13, reg.getRescuer1());
	           preparedStmt.setString(14, reg.getRescuer2());
	           preparedStmt.setString(15, reg.getRescuer3());
	           preparedStmt.setString(16, reg.getRescuer4());
	           
	           preparedStmt.setString(17, reg.getRescuer5());
	           preparedStmt.setString(18, reg.getExtraField1());
	           preparedStmt.setString(19, reg.getExtraField2());
	           preparedStmt.setString(20, reg.getExtraField3());
	           preparedStmt.setString(21, reg.getUserName());
	           preparedStmt.setString(22, reg.getUserUid());
	           preparedStmt.setString(23, reg.getAddedById());
	           
	           preparedStmt.setString(24, reg.getState());
	           preparedStmt.setString(25, reg.getCity());
	           preparedStmt.setString(26, reg.getDivision());
	           preparedStmt.setString(27, reg.getRescuertype());
	           preparedStmt.setString(28, reg.getEmergencyNum());
	           preparedStmt.setString(29, reg.getMiddleName());
	           preparedStmt.setString(30, reg.getGender());
	           preparedStmt.setString(31, reg.getAddress());
	           preparedStmt.setString(32, reg.getArea());
	           preparedStmt.setString(33, reg.getAlternateEmail());
	           preparedStmt.setString(34, reg.getTelephone());
	           preparedStmt.setString(35, reg.getWhatsappNumber());
	           preparedStmt.setString(36, reg.getAadhaarNumber());
	           preparedStmt.setString(37, reg.getProfession());
	           preparedStmt.setString(38, reg.getContactNotOwn());
	           preparedStmt.setString(39, reg.getSecondaryContact());
	           preparedStmt.setString(40, reg.getBloodGroup());
	           preparedStmt.setString(41, reg.getChurchDetails());
	           preparedStmt.setString(42, reg.getNameOfChurch());
	           preparedStmt.setString(43, reg.getMemberSince());
	           preparedStmt.setString(44, reg.getPastorName());
	           preparedStmt.setString(45, reg.getPastorOrgContact());
	           preparedStmt.setInt(46, reg.getPincode());
	           preparedStmt.setString(47, reg.getDistrict());
	           preparedStmt.setBoolean(48, reg.isApprovedByAdmin());
	           preparedStmt.setBoolean(49, reg.isApprovedBySuAdmin());
	           preparedStmt.setString(50, reg.getApproveByAdminUid());
	           preparedStmt.setDate(51, sqlDob);
	         
	           // execute the preparedstatement
	          preparedStmt.execute();
	          ResultSet rs = null;
	          UserLog userLog = null;
	          CommonSerices commonSerices = new CommonSerices();
	          userLog = new UserLog();
	          rs = preparedStmt.getGeneratedKeys();    
		      	  if(rs != null && rs.next()){
		      		userLog.setUserId(rs.getInt(1));
		      		userLog.setChangedBy(reg.getUserUid());
					userLog.setUpdatedVal(Password.hashPassword(reg.getPassword()));
					userLog.setModuleName(WebKeys.RESET_PASSWORD);
					userLog.setStatus(WebKeys.STATUS_ACTIVE);
					commonSerices.maintainLog(userLog);
		      	  }
	       } catch (SQLException e) {
	           log.fatal(e.getMessage());
	       } finally {
	           if (connection != null) {
	               try {
	                   connection.close();
	               } catch (SQLException e) {
	            	   
	            	   log.fatal(e.getMessage());
	               }
	           }
	       }
		return 1;
	}
	
	public int registerPublicUser(Registration reg) {
	       Connection connection = null;	     
	       String userUid=UUID.randomUUID().toString();	       
	       reg.setUserUid(userUid);
	       reg.setPassword(Password.hashPassword(reg.getPassword()));	      
	       try {           
	    	  Date sqlDob = new Date(reg.getDob().getTime());
	           connection = createJDBCConnection.getConnection();
	       
	           // the mysql insert statement
	           String query = "insert into registration (firstName, lastName, fullName, "
	           					+ "mobileNum, email, password, role, deviceId, userUid, gender, "
	           					+ "dob, emailVerified, organization, completeReg, address, pincode,"
	           					+ "state, city, district, nameOfChurch, isApproved, isRejected, rescuer1, rescuer2, "
	           					+ " rescuer3, rescuer4, approveByAdminUid, area, addedBy) "
	           							+ "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)" ;

	           // create the mysql insert preparedstatement
	           java.sql.PreparedStatement preparedStmt =  connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
	           preparedStmt.setString(1, reg.getFirstName());
	           preparedStmt.setString(2, reg.getLastName());
	           preparedStmt.setString(3, reg.getFullName());
	           preparedStmt.setString(4, reg.getMobileNum());
	           
	           preparedStmt.setString(5, reg.getEmail());
	           preparedStmt.setString(6, reg.getPassword());
	           preparedStmt.setString(7, reg.getRole().trim());
	           preparedStmt.setString(8, reg.getDeviceId());
	           preparedStmt.setString(9, reg.getUserUid());	           
	           preparedStmt.setString(10, reg.getGender());
	           preparedStmt.setDate(11, sqlDob);
	           preparedStmt.setBoolean(12, false);
	           preparedStmt.setString(13, reg.getOrganization());
	           preparedStmt.setBoolean(14, true);
	           preparedStmt.setString(15, reg.getAddress());	           
	           preparedStmt.setInt(16, reg.getPincode());
	           preparedStmt.setString(17, reg.getState());
	           preparedStmt.setString(18, reg.getCity());
	           preparedStmt.setString(19, reg.getDistrict());
	           preparedStmt.setString(20, reg.getNameOfChurch());
	           preparedStmt.setBoolean(21, false);
	           preparedStmt.setBoolean(22, false);
	           preparedStmt.setString(23, reg.getRescuer1());
	           preparedStmt.setString(24, reg.getRescuer2());
	           preparedStmt.setString(25, reg.getRescuer3());
	           preparedStmt.setString(26, reg.getRescuer4());
	           preparedStmt.setString(27, reg.getApproveByAdminUid());
	           preparedStmt.setString(28, reg.getArea());
	           preparedStmt.setString(29, reg.getAddedBy());
	         
	           // execute the preparedstatement
	          preparedStmt.executeUpdate();    
 
            ResultSet rs = null;
            UserLog userLog = null;
            CommonSerices commonSerices = new CommonSerices();
            userLog = new UserLog();
            rs = preparedStmt.getGeneratedKeys();    
	      	  if(rs != null && rs.next()){
	      		userLog.setUserId(rs.getInt(1));
	      		userLog.setChangedBy(reg.getUserUid());
				userLog.setUpdatedVal(Password.hashPassword(reg.getPassword()));
				userLog.setModuleName(WebKeys.RESET_PASSWORD);
				userLog.setStatus(WebKeys.STATUS_ACTIVE);
				commonSerices.maintainLog(userLog);
	      	  }
	       } catch (SQLException e) {
	           log.fatal(e.getMessage());
	           return 0;
	       } finally {
	           if (connection != null) {
	               try {
	                   connection.close();
	               } catch (SQLException e) {
	            	   
	            	   log.fatal(e.getMessage());
	               }
	           }
	       }
		return 1;
	}

	public Registration getUserDataByUID(String uID) {
		Registration register = new Registration();
		ResultSet rs = null;
		Connection connection = null;
		PreparedStatement myStmt = null;		
		try {
			connection = createJDBCConnection.getConnection();
			myStmt = connection.prepareStatement("select * from registration where userUid  =?");
			myStmt.setString(1, uID);
			rs = myStmt.executeQuery();
			while (rs.next()) {
				register.setFirstName(rs.getString("firstName"));
				register.setMiddleName(rs.getString("middleName"));
				register.setLastName(rs.getString("lastName"));
				register.setGender(rs.getString("gender"));
				register.setPincode(rs.getInt("pincode"));
				register.setAddress(rs.getString("address"));
				register.setEmail(rs.getString("email"));
				register.setRole(rs.getString("role"));
				register.setFullName(rs.getString("fullName"));
				register.setMobileNum(rs.getString("mobileNum"));
				register.setUserUid(rs.getString("userUid"));
				register.setState(rs.getString("state"));
				register.setCity(rs.getString("city"));
				register.setDivision(rs.getString("division"));
				register.setDistrict(rs.getString("district"));
				register.setAadhaarNumber(rs.getString("aadhaarNumber"));				
				register.setNameOfChurch(rs.getString("nameOfChurch"));
				register.setPastorName(rs.getString("pastorName"));
				register.setPastorOrgContact(rs.getString("pastorOrgContact"));
				register.setMemberSince(rs.getString("memberSince"));
				register.setAlternateEmail(rs.getString("alternateEmail"));
				register.setWhatsappNumber(rs.getString("whatsappNumber"));
				register.setTelephone(rs.getString("telephone"));
				register.setEmergencyNum(rs.getString("emergencyNum"));
				register.setSecondaryContact(rs.getString("secondaryContact"));
				register.setProfession(rs.getString("profession"));
				register.setFatherName(rs.getString("fatherName"));				
				register.setSecondaryRelation(rs.getString("secondaryRelation"));
				register.setPrimaryRelation(rs.getString("primaryRelation"));
				register.setWillingToHelp(rs.getString("willingToHelp"));
				register.setBloodGroup(rs.getString("bloodGroup"));
				register.setOrganization(rs.getString("organization"));
				register.setPrimaryName(rs.getString("primaryName"));
				register.setSecondaryContactName(rs.getString("secondaryContactName"));
				Date date = rs.getDate("dob");
				if(date != null){
					DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	                String dobDate = dateFormat.format(date);
	                register.setDobString(dobDate);
				}
				
			}
			connection.close();

		} catch (SQLException e) {
			log.fatal(e);
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					log.fatal(e);
				}
			}
		}
		return register;
	}

	public boolean saveVerificationInfo(Registration reg) {		
		boolean rs = true;
		Connection connection = null;
		if(reg.getUserUid()==null) 
			return false;
		
		try {
			 //Date sqlDob = new Date(reg.getDob().getTime());
			 connection = createJDBCConnection.getConnection();
			 String query = "update registration SET aadhaarNumber=?,nameOfChurch=?, pastorName=?,"
			 					+ "pastorOrgContact=?, memberSince=? where  userUid= ? " ;
			
			 java.sql.PreparedStatement preparedStmt =  connection.prepareStatement(query);
			 preparedStmt.setString(1, reg.getAadhaarNumber());
			 preparedStmt.setString(2, reg.getNameOfChurch());
			 preparedStmt.setString(3, reg.getPastorName());
			 preparedStmt.setString(4, reg.getPastorOrgContact());
			 preparedStmt.setString(5, reg.getMemberSince());
			 preparedStmt.setString(6, reg.getUserUid());
			 preparedStmt.executeUpdate();
			 connection.close();

		} catch (SQLException e) {
			log.fatal(e);
			return false;
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					log.fatal(e);
				}
			}
		}
		return rs;
	}

	public boolean saveConfirmationInfo(Registration reg) {
		boolean rs = true;
		Connection connection = null;
		String complete = "";
		if(reg.getUserUid()==null) 
			return false;
		if(reg.isCompleteReg()){
			complete = ", completeReg=true ";
		}
		
		try {
			 connection = createJDBCConnection.getConnection();
			 String query = "update registration SET profession=?, role=?, bloodGroup =?,"
			 			+ "willingToHelp=?, whatsappNumber=?, alternateEmail=?,"
					 	+"telephone=?, emergencyNum=?, secondaryContact=?,"
					 	+ "primaryRelation=?, secondaryRelation=?, rescuer1=?, rescuer2=?, "
					 	+ " rescuer3=?, rescuer4=?, approveByAdminUid=?, primaryName=?, secondaryContactName=? "+complete+" where  userUid= ? " ;
			 
			 
			
			 java.sql.PreparedStatement preparedStmt =  connection.prepareStatement(query);
			 preparedStmt.setString(1, reg.getProfession());
			 preparedStmt.setString(2, reg.getRole());
			 preparedStmt.setString(3, reg.getBloodGroup());
			 preparedStmt.setString(4, reg.getWillingToHelp());
			 preparedStmt.setString(5, reg.getWhatsappNumber());
			 preparedStmt.setString(6, reg.getAlternateEmail());
			 preparedStmt.setString(7, reg.getTelephone());
			 preparedStmt.setString(8, reg.getEmergencyNum());
			 preparedStmt.setString(9, reg.getSecondaryContact());
			 preparedStmt.setString(10, reg.getPrimaryRelation());
			 preparedStmt.setString(11, reg.getSecondaryRelation());
			 preparedStmt.setString(12, reg.getRescuer1());
			 preparedStmt.setString(13, reg.getRescuer2());
			 preparedStmt.setString(14, reg.getRescuer3());
			 preparedStmt.setString(15, reg.getRescuer4());
			 preparedStmt.setString(16, reg.getApproveByAdminUid());
			 preparedStmt.setString(17, reg.getPrimaryName());
			 preparedStmt.setString(18, reg.getSecondaryContactName());
			 preparedStmt.setString(19, reg.getUserUid());
			 preparedStmt.executeUpdate();
			 connection.close();

		} catch (SQLException e) {
			log.fatal(e);
			return false;
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					log.fatal(e);
				}
			}
		}
		return rs;
	}

	public boolean updateEmailVerify(String uID) {
		boolean rs = true;
		Connection connection = null;
		if(uID==null) 
			return false;
		
		try {
			 connection = createJDBCConnection.getConnection();
			 String query = "update registration SET emailVerified=true where  userUid=?" ;			
			 java.sql.PreparedStatement preparedStmt =  connection.prepareStatement(query);
			 preparedStmt.setString(1, uID);
			 preparedStmt.executeUpdate();
			 connection.close();

		} catch (SQLException e) {
			log.fatal(e);
			return false;
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					log.fatal(e);
				}
			}
		}
		return rs;
	}
	public boolean resetPassword(Registration reg) {
		boolean rs = true;
		Connection connection = null;
		if(reg.getUserUid()==null) 
			return false;
		
		try {
			 connection = createJDBCConnection.getConnection();
			 String query = "update registration SET password=? , loginAttempts=? where  userUid=?" ;			
			 java.sql.PreparedStatement preparedStmt =  connection.prepareStatement(query);
			 preparedStmt.setString(1, reg.getPassword());
			 preparedStmt.setInt(2, 0);
			 preparedStmt.setString(3, reg.getUserUid());
			 preparedStmt.executeUpdate();
			 connection.close();
		} catch (SQLException e) {
			log.fatal(e);
			return false;
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					log.fatal(e);
				}
			}
		}
		return rs;
	}

	
}
