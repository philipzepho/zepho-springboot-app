package com.blt.zepho.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.blt.zepho.jdbc.CreateJDBCConnection;
import com.blt.zepho.model.Registration;

@Repository
public class UpdateEmailValidationDao {
	static Logger log = Logger.getLogger(UpdateEmailValidationDao.class.getName());
	
	@Autowired
	CreateJDBCConnection createJDBCConnection;

	public int validateEmail(String  email,String userUid) {
		// ResultSet rs = null;
		
		log.debug("Email " + email);
		Connection connection = null;
		int count = 0;
		PreparedStatement st = null;
		try {
			connection = createJDBCConnection.getConnection();

			try {

				connection = createJDBCConnection.getConnection();

				st = connection.prepareStatement("select count(*) from registration where email=? and userUid!=?");
                st.setString(1, email);
                st.setString(2, userUid);
               
				                					
				ResultSet res = st.executeQuery();
				
				while (res.next()) {
					count = res.getInt(1);
				}
				
			} catch (SQLException e) {
				log.fatal(e.getMessage());
			}
		} catch (Exception e) {
			log.fatal(e.getMessage());
		}
		return count;
	}
}
