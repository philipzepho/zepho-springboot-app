package com.blt.zepho.dao;

import java.sql.Connection;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.blt.zepho.jdbc.CreateJDBCConnection;
import com.blt.zepho.model.Organization;

@Repository
public class SaveOrganizationDao {
	
	@Autowired
	CreateJDBCConnection createJDBCConnection;
	
	public Boolean doSaveOrganization(Organization organization) {
		// ResultSet rs = null;
		Connection connection = null;
		boolean res = false;
		try {
			connection = createJDBCConnection.getConnection();
			System.out.println("inside jdbc if conn");
			String query = "insert into organizations (orgFullName, orgShortName) " + " values(?,?)";

			java.sql.PreparedStatement preparedStmt = connection.prepareStatement(query);
			preparedStmt.setString(1, organization.getOrgFullName());
			preparedStmt.setString(2, organization.getOrgShortName());

			res = preparedStmt.execute();

			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {

					e.printStackTrace();
				}
			}
		}
		return res;
	}

	public boolean saveOrganistaionCode(Organization organization) {
		Connection connection = null;
		boolean res = true;
		String orgShortName = organization.getOrgShortName();
		try {
			connection = createJDBCConnection.getConnection();
			System.out.println("inside jdbc if conn");
			String query = "update organizations set inviteCode=? where orgShortName=?";

			java.sql.PreparedStatement preparedStmt = connection.prepareStatement(query);
			preparedStmt.setString(1, organization.getInviteCode());
			preparedStmt.setString(2, orgShortName);
			preparedStmt.executeUpdate();
			connection.close();

		} catch (SQLException e) {
			res = false;
			e.printStackTrace();
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {

					e.printStackTrace();
				}
			}
		}
		return res;

	}
}
