package com.blt.zepho.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.blt.zepho.jdbc.CreateJDBCConnection;
//import com.blt.zepho.services.RegistrationService;
import com.blt.zepho.model.Registration;
import com.blt.zepho.model.RegistrationList;

@Repository
public class ApprovalDao {
	
	static Logger log = Logger.getLogger(ApprovalDao.class.getName());
	private String userUids = "";
	HashSet<String> userSet = new HashSet<>();
	
	@Autowired
	CreateJDBCConnection createJDBCConnection;
	
	public ArrayList<Registration> approvalActivity(Registration reg) {
		ArrayList<Registration> approvalList = new ArrayList<Registration>();
		ResultSet rs = null;
		Connection connection = null;
		PreparedStatement myStmt = null;		
		String query = "";
		boolean isUidAdded = false;
		if(reg.getRole().equalsIgnoreCase("Admin")){
			query = " and approveByAdminUid=?";
			isUidAdded = true;
			}else{
			query = " and approvedByAdmin = true";
			}
		try {
			connection = createJDBCConnection.getConnection();
			myStmt = connection.prepareStatement("select * from registration where isApproved  = ? and isRejected = ? and organization =? and emailVerified=? and completeReg=?"+query);
			myStmt.setBoolean(1, false);
			myStmt.setBoolean(2, false);
			myStmt.setString(3, reg.getOrganization());
			myStmt.setBoolean(4, true);
			myStmt.setBoolean(5, true);
			if(isUidAdded){
				myStmt.setString(6, reg.getUserUid());
			}
			rs = myStmt.executeQuery();
			
			int count = 0;
			while (rs.next()) {
				Registration register = new Registration();
				register.setEmail(rs.getString("email"));
				register.setRole(rs.getString("role"));
				register.setFullName(rs.getString("fullName"));
				register.setAddedBy(rs.getString("addedBy"));
				register.setMobileNum(rs.getString("mobileNum"));
				register.setUserUid(rs.getString("userUid"));
				register.setState(rs.getString("state"));
				register.setCity(rs.getString("city"));
				register.setDivision(rs.getString("division"));
				register.setRescuertype(rs.getString("rescuertype"));
				register.setUpdateRole("updateRole"+count);
				register.setApprovedByAdmin(rs.getBoolean("approvedByAdmin"));
				register.setApprovedBySuAdmin(rs.getBoolean("approvedBySuAdmin"));
				register.setDistrict(rs.getString("district"));
				register.setAadhaarNumber(rs.getString("aadhaarNumber"));				
				register.setNameOfChurch(rs.getString("nameOfChurch"));
				register.setPastorName(rs.getString("pastorName"));
				register.setPastorOrgContact(rs.getString("pastorOrgContact"));
				register.setMemberSince(rs.getString("memberSince"));
				register.setAddress(rs.getString("address"));
				register.setFatherName(rs.getString("fatherName"));	
				register.setPincode(rs.getInt("pincode"));	
				register.setOrganization(rs.getString("organization"));	
				Date date = rs.getDate("dob");
				if(date != null){
					DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	                String dobDate = dateFormat.format(date);
	                register.setDobString(dobDate);
				}
				// Assuming you have a user object
				approvalList.add(register);
				count++;
			}
			connection.close();

		} catch (SQLException e) {
			log.fatal(e);
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					log.fatal(e);
				}
			}
		}
		return approvalList;

	}

	@SuppressWarnings("null")
	public boolean doUserapproval(Registration reg) {
		String approvedBy="";
		boolean rs = true;
		Connection connection = null;
		if (reg.isApprovedBySuAdmin() && reg.isApprovedByAdmin()){
			approvedBy=" approvedbyAdmin=true, approvedBySuAdmin=true, isApproved=true ";
		}else if (reg.isApprovedByAdmin()){
			approvedBy="approvedbyAdmin=true";
		}else if (reg.isApprovedBySuAdmin()){
			approvedBy="approvedBySuAdmin=true";
		}
		
		
	if(reg.getUserUid()==null) return false;
		try {
			 connection = createJDBCConnection.getConnection();
			String query = "update registration SET "+approvedBy+" where  userUid= ? " ;
			// create the mysql insert preparedstatement	
			java.sql.PreparedStatement preparedStmt =  connection.prepareStatement(query);
			preparedStmt.setString(1, reg.getUserUid());
			if(reg.getRole() != null && !reg.getRole().isEmpty()){
				query = "update registration SET "+approvedBy+", role=? where  userUid= ? ";
				preparedStmt =  connection.prepareStatement(query);
				preparedStmt.setString(1, reg.getRole());
				preparedStmt.setString(2, reg.getUserUid());
				}           
	           preparedStmt.executeUpdate();
	           connection.close();

		} catch (SQLException e) {
			log.fatal(e);
			return false;
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					log.fatal(e);
				}
			}
		}
		return rs;

	}


	@SuppressWarnings("null")
	public boolean doUserRejected(Registration reg) {

		boolean rs = true;
		Connection connection = null;
		java.sql.Statement myStmt = null;
		if(reg.getUserUid()==null) return false;

		try {
			 connection = createJDBCConnection.getConnection();
				
				
				String query = "update registration SET isRejected=true where  userUid= ? " ;

		           // create the mysql insert preparedstatement
		           java.sql.PreparedStatement preparedStmt =  connection.prepareStatement(query);
		           preparedStmt.setString(1, reg.getUserUid());
		           
		           
		           
		           preparedStmt.executeUpdate();
	  
		           connection.close();
		} catch (SQLException e) {
			log.fatal(e);
			return false;
			
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					log.fatal(e);
				}
			}
		}
		return rs;

	}

	public boolean approveAllUsers(RegistrationList registrations) {
		String approvedBy="";
		boolean rs = true;
		Connection connection = null;
		/**/
		
		for(Registration reg: registrations.getRegList()){	
			if(registrations.isApprovedByAdmin()){
				reg.setApprovedByAdmin(true);
			}else if(registrations.isApprovedBySuAdmin()){
				reg.setApprovedBySuAdmin(true);
			}
						
			
			if (reg.isApprovedBySuAdmin() && reg.isApprovedByAdmin()){
				approvedBy=" approvedbyAdmin=true, approvedBySuAdmin=true, isApproved=true ";
			}else if (reg.isApprovedByAdmin()){
				approvedBy="approvedbyAdmin=true";
			}else if (reg.isApprovedBySuAdmin()){
				approvedBy="approvedBySuAdmin=true";
			}
			
			if(reg.getUserUid() != null) {				
				try {
					connection = createJDBCConnection.getConnection();
					String query = "update registration SET "+approvedBy+" where  userUid= ? " ;
					java.sql.PreparedStatement preparedStmt =  connection.prepareStatement(query);
					if(reg.getRole() != null && !reg.getRole().isEmpty()){
						query = "update registration SET "+approvedBy+", role= ? where  userUid= ? ";
						preparedStmt =  connection.prepareStatement(query);
						preparedStmt.setString(1, reg.getRole());
						preparedStmt.setString(2, reg.getUserUid());
					}
					preparedStmt.executeUpdate();
					connection.close();
				} catch (SQLException e) {
					log.fatal(e);
					return false;
				} finally {
					if (connection != null) {
						try {
							connection.close();
						} catch (SQLException e) {
							log.fatal(e);
						}
					}
				}	
			}
		}
		return rs;
	}

	public List<Registration> approveAdmins(Registration userReg) {
		List<Registration> approveAdminList = new ArrayList<Registration>();	
		Registration adminReg = new Registration();
		ResultSet rs = null;
		Connection connection = null;
		PreparedStatement myStmt = null;	
		if(userReg.getApproveByAdminUid() != "" && userReg.getApproveByAdminUid() != null){
			try {				
				connection = createJDBCConnection.getConnection();
				myStmt = connection.prepareStatement("select * from registration where userUid =?");
				myStmt.setString(1, userReg.getApproveByAdminUid());
				rs = myStmt.executeQuery();
				while (rs.next()) {				
					adminReg.setEmail(rs.getString("email"));
					adminReg.setFullName(rs.getString("fullName"));
					adminReg.setDeviceId(rs.getString("deviceId"));
					adminReg.setMobileNum(rs.getString("mobileNum"));
				}
				connection.close();
	
			} catch (SQLException e) {
				log.fatal(e);
			} finally {
				if (connection != null) {
					try {
						connection.close();
					} catch (SQLException e) {
						log.fatal(e);
					}
				}
			}
		}
		if(adminReg.getEmail() != "" && adminReg.getEmail() != null){
			approveAdminList.add(adminReg);
		}
		return approveAdminList;
	}
	
	public Registration getSuperAdminByOrg(String org) {		
		Registration saReg = new Registration();
		List<Registration> sAdminList = new ArrayList<Registration>();
		ResultSet rs = null;
		Connection connection = null;
		PreparedStatement myStmt = null;	
		if(org != "" && org != null){
			try {				
				connection = createJDBCConnection.getConnection();
				myStmt = connection.prepareStatement("select * from registration where role=? and organization=? and isApproved=?");
				myStmt.setString(1, "SuperAdmin");
				myStmt.setString(2, org);
				myStmt.setBoolean(3, true);
				rs = myStmt.executeQuery();
				while (rs.next()) {	
					Registration superAdminReg = new Registration();
					superAdminReg.setEmail(rs.getString("email"));
					superAdminReg.setDeviceId(rs.getString("deviceId"));
					superAdminReg.setFullName(rs.getString("fullName"));
					superAdminReg.setUserUid(rs.getString("userUid"));
					superAdminReg.setMobileNum(rs.getString("mobileNum"));
					sAdminList.add(superAdminReg);
				}
				connection.close();
	
			} catch (SQLException e) {
				log.fatal(e);
			} finally {
				if (connection != null) {
					try {
						connection.close();
					} catch (SQLException e) {
						log.fatal(e);
					}
				}
			}
		}
		if(sAdminList.size() > 0){
			return sAdminList.get(0);
		}else{
			return new Registration();
		}
	}

	public void replaceFarRescuers(Registration reg) {
		if(reg.getPincode() > 0){
			userSet.clear();
			userSet.add(reg.getUserUid());
			userUids = "'"+reg.getUserUid()+"'";
			boolean status = false;
			status = replaceRescuers(reg, "rescuer4");
			if(status){
				status = replaceRescuers(reg, "rescuer3");
			}
			if(status){
				status = replaceRescuers(reg, "rescuer2");
			}
			if(status){
				status = replaceRescuers(reg, "rescuer1");
			}
		}		
	}
	
	private boolean replaceRescuers(Registration reg, String res){
		ResultSet rs = null;
		Connection connection = null;
		PreparedStatement preparedStmt = null;
		String query = "";
		boolean status = true;
		try {				
			connection = createJDBCConnection.getConnection();
			query = "select * from registration r1 where r1.pincode=? and r1.organization=? and r1.userUid not in(?) and  r1."+res+" in "
					+ "(SELECT r2.userUid FROM registration r2 where r2.pincode!=? and r2.userUid = r1."+res+" and r2.userUid not in(?))";
			preparedStmt = connection.prepareStatement(query);			
			preparedStmt.setInt(1, reg.getPincode());
			preparedStmt.setString(2, reg.getOrganization());
			preparedStmt.setString(3, userUids);
			preparedStmt.setInt(4, reg.getPincode());
			preparedStmt.setString(5, userUids);
			rs = preparedStmt.executeQuery();
			while (rs.next()) {	
				String userUid = rs.getString("userUid");
				if(userUid != "" && userUid != null && !userSet.contains(userUid)){
					userSet.add(userUid);
					userUids = userUids + ",'"+rs.getString("userUid")+"'";					
					String update = "update registration SET "+res+"=? where userUid ='"+userUid+"'";
					preparedStmt = connection.prepareStatement(update);
					preparedStmt.setString(1, reg.getUserUid());
					preparedStmt.execute();
				}
			}
			connection.close();

		} catch (SQLException e) {
			status = false;
			log.fatal(e);
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					log.fatal(e);
				}
			}
		}
		return status;
	}
	
	public long getApprovalCount(Registration reg) {
		ArrayList<Registration> approvalList = new ArrayList<Registration>();
		ResultSet rs = null;
		Connection connection = null;
		PreparedStatement myStmt = null;		
		String query = "";
		boolean isUidAdded = false;
		long count = 0;
		if(reg.getRole().equalsIgnoreCase("Admin")){
			query = " and approveByAdminUid=?";
			isUidAdded = true;
			}else{
			query = " and approvedByAdmin = true";
			}
		try {
			connection = createJDBCConnection.getConnection();
			myStmt = connection.prepareStatement("select count(*) from registration where isApproved  = ? and isRejected = ? and organization =? and emailVerified=? and completeReg=?"+query);
			myStmt.setBoolean(1, false);
			myStmt.setBoolean(2, false);
			myStmt.setString(3, reg.getOrganization());
			myStmt.setBoolean(4, true);
			myStmt.setBoolean(5, true);
			if(isUidAdded){
				myStmt.setString(6, reg.getUserUid());
			}
			rs = myStmt.executeQuery();			
			
			while (rs.next()) {
				count = rs.getLong(1);
			}
			connection.close();

		} catch (SQLException e) {
			log.fatal(e);
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					log.fatal(e);
				}
			}
		}
		return count;

	}

	
}