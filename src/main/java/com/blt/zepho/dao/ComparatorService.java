package com.blt.zepho.dao;

import java.util.Comparator;

import com.blt.zepho.model.Sos;

public class  ComparatorService  implements Comparator<Sos> {
	@Override
	public int compare(Sos sos1, Sos sos2) {
		long created1 = sos1.getCreatedAt();
		long created2 = sos2.getCreatedAt();
 
		if (created1 < created2) {
			return 1;
		} else if (created1 > created2) {
			return -1;
		} else {
			return 0;
		}
	}
}