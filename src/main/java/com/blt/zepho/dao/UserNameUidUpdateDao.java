package com.blt.zepho.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.blt.zepho.jdbc.CreateJDBCConnection;
import com.blt.zepho.model.UidandRole;
import com.blt.zepho.model.UserDetails;

@Repository
public class UserNameUidUpdateDao {

	static Logger log = Logger.getLogger(UserNameUidUpdateDao.class.getName());
	
	@Autowired
	CreateJDBCConnection createJDBCConnection;
	
	public ArrayList<UserDetails> getUserUidFulName(UidandRole userData) {
		ArrayList<UserDetails> fullNameUserIDList = new ArrayList<UserDetails>();

		ResultSet rs = null;
		Connection connection = null;
		PreparedStatement myStmt = null;
		try {

			connection = createJDBCConnection.getConnection();
			// getting each user's rescuers
			// here in the particular query userUid is the rescuer uuid
			
			if(userData.getRole().equals("SuperAdmin")){
				myStmt = connection.prepareStatement("select fullName , userUid , email, mobileNum, rescuer1, rescuer2, rescuer3, rescuer4, role, emergencyNum, isApproved  from registration where organization= ? ");
				myStmt.setString(1, userData.getOrganization());
			}else{
				myStmt = connection.prepareStatement("select fullName , userUid , email, mobileNum, rescuer1, rescuer2, rescuer3, rescuer4, role, emergencyNum, isApproved  from registration where addedById= ? ");
				myStmt.setString(1, userData.getUserUid());
			}
			

			
			rs = myStmt.executeQuery();

			while (rs.next()) {
				UserDetails userDetails = new UserDetails();
				userDetails.setLabel(rs.getString("fullName"));
				userDetails.setFullName(rs.getString("fullName"));
				userDetails.setUserUid(rs.getString("userUid"));
				userDetails.setEmail(rs.getString("email"));
				userDetails.setMobileNum(rs.getString("mobileNum"));
				userDetails.setRescuer1(rs.getString("rescuer1"));
				userDetails.setRescuer2(rs.getString("rescuer2"));
				userDetails.setRescuer3(rs.getString("rescuer3"));
				userDetails.setRescuer4(rs.getString("rescuer4"));
				userDetails.setPassword("");
				userDetails.setRole(rs.getString("role"));
				userDetails.setEmergencyNum(rs.getString("emergencyNum"));
				userDetails.setApproved(rs.getBoolean("isApproved"));
				
				fullNameUserIDList.add(userDetails);
			}

			

			connection.close();

		} catch (SQLException e) {
			log.fatal(e.getMessage());
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					log.fatal(e.getMessage());
				}
			}
		}
		return fullNameUserIDList;

	}
}
