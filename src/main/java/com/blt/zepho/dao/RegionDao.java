package com.blt.zepho.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.blt.zepho.jdbc.CreateJDBCConnection;
import com.blt.zepho.model.City;
import com.blt.zepho.model.District;
import com.blt.zepho.model.Registration;
import com.blt.zepho.model.States;

@Repository
public class RegionDao {
	static Logger log = Logger.getLogger(RegionDao.class.getName());
	
	@Autowired
	CreateJDBCConnection createJDBCConnection;
	
	public ArrayList<States> getStateList(){
		ResultSet rs = null;
		Connection connection = null;
		PreparedStatement myStmt = null;
		ArrayList<States> stringList= new ArrayList<States>();
		try{
			connection = createJDBCConnection.getConnection();
			myStmt = connection.prepareStatement("select * from states order by state_name ASC");
			rs = myStmt.executeQuery();
			while (rs.next()){
				States statesName = new States();
				statesName.setStateId(rs.getInt("state_id"));
				statesName.setStateName(rs.getString("state_name"));
				stringList.add(statesName);
			}
		}catch (SQLException e) {
			log.fatal(e.getMessage());
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					log.fatal(e.getMessage());
				}
			}
		}
		return stringList;
	}
	public ArrayList<District> getDistrictList(int stateIdNumber){
		ResultSet rs = null;
		Connection connection = null;
		PreparedStatement myStmt = null;
		ArrayList<District> stringList= new ArrayList<District>();
		try{
			connection = createJDBCConnection.getConnection();
			myStmt = connection.prepareStatement("select * from district where state_id=? order by district_name ASC");
			myStmt.setInt(1, stateIdNumber);
			rs = myStmt.executeQuery();
			while (rs.next()){
				District districtName = new District();
				districtName.setDistrictId(rs.getInt("district_id"));
				districtName.setDistrictName(rs.getString("district_name"));
				districtName.setStateId(rs.getInt("state_id"));
				stringList.add(districtName);
			}
		}catch (SQLException e) {
			log.fatal(e.getMessage());
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					log.fatal(e.getMessage());
				}
			}
		}
		return stringList;
	}
	public ArrayList<City> getCityList(int districtIdNumber){
		ResultSet rs = null;
		Connection connection = null;
		PreparedStatement myStmt = null;
		ArrayList<City> stringList= new ArrayList<City>();
		try{
			connection = createJDBCConnection.getConnection();
			myStmt = connection.prepareStatement("select * from city where district_id=? order by city_name ASC");
			myStmt.setInt(1, districtIdNumber);
			rs = myStmt.executeQuery();
			while (rs.next()){
				City cityName = new City();
				cityName.setCityId(rs.getInt("city_id"));
				cityName.setCityName(rs.getString("city_name"));
				cityName.setDistrictId(rs.getInt("district_id"));
				stringList.add(cityName);
			}
		}catch (SQLException e) {
			log.fatal(e.getMessage());
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					log.fatal(e.getMessage());
				}
			}
		}
		return stringList;
	}
	public ArrayList<Registration> updateUser(Registration userProfile){
		ResultSet rs = null;
		Connection connection = null;
		PreparedStatement myStmt = null;
		ArrayList<Registration> stringList= new ArrayList<Registration>();
		try{
			connection = createJDBCConnection.getConnection();
			String query = "update registration set state=?, "
					+ "city=?, gender=?, address=?, pincode=?, district=? where userUid=?" ;
			myStmt = connection.prepareStatement(query);
			myStmt.setString(1, userProfile.getState());
			myStmt.setString(2, userProfile.getCity());
			myStmt.setString(3, userProfile.getGender());
			myStmt.setString(4, userProfile.getAddress());
			myStmt.setInt(5, userProfile.getPincode());
			myStmt.setString(6, userProfile.getDistrict());
			myStmt.setString(7, userProfile.getUserUid());
			myStmt.executeUpdate();
			
		}catch (SQLException e) {
			log.fatal(e.getMessage());
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					log.fatal(e.getMessage());
				}
			}
		}
		return stringList;
	}
}
