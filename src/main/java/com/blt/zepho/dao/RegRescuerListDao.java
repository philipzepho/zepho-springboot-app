package com.blt.zepho.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.blt.zepho.jdbc.CreateJDBCConnection;

import com.blt.zepho.model.Rescuer;

@Repository
public class RegRescuerListDao 
{

	static Logger log = Logger.getLogger(RegRescuerListDao.class.getName());
	
	@Autowired
	CreateJDBCConnection createJDBCConnection;
	
	public ArrayList<Rescuer> GetRescuerByUserIdz(String regIds) {
		ArrayList<Rescuer> rescuerList = new ArrayList<>();
		ResultSet rs = null;
		Connection connection = null;
		PreparedStatement myStmt = null;
		try {

			connection = createJDBCConnection.getConnection();
			//getting each user's rescuers
			//here in the particular query userUid is the rescuer uuid
			myStmt = connection.prepareStatement("select deviceId , email , fullName, organization, userUid, mobileNum from registration where isApproved=? and userUid in ("+regIds+") ");

			myStmt.setBoolean(1, true);
			//myStmt.setString(2, regIds);
			rs = myStmt.executeQuery();
			
			while (rs.next()) {
				Rescuer rescuer = new Rescuer();
				rescuer.setDeviceId(rs.getString("deviceId"));
				rescuer.setEmail( rs.getString("email"));
				rescuer.setFullName(rs.getString("fullName"));
				rescuer.setOrg(rs.getString("organization"));
				rescuer.setUserUid(rs.getString("userUid"));
				rescuer.setMobile(rs.getString("mobileNum"));
				rescuerList.add(rescuer);
			}

			

			connection.close();

		} catch (SQLException e) {
			log.fatal(e.getMessage());
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					log.fatal(e.getMessage());
				}
			}
		}
		return rescuerList;

	}
	
}
