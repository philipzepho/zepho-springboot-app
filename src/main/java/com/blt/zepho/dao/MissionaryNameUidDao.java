package com.blt.zepho.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.blt.zepho.jdbc.CreateJDBCConnection;
import com.blt.zepho.model.Missionary;

@Repository
public class MissionaryNameUidDao {
	
	@Autowired
	CreateJDBCConnection createJDBCConnection;
	
	public ArrayList<Missionary> getUserUidFulName(String userUid) {
		ArrayList<Missionary> fullNameUserIDList = new ArrayList<Missionary>();

		ResultSet rs = null;
		Connection connection = null;
		PreparedStatement myStmt = null;
		try {

			connection = createJDBCConnection.getConnection();
			// getting each user's rescuers
			// here in the particular query userUid is the rescuer uuid
			myStmt = connection.prepareStatement("select fullName , userUid ,deviceId  ,email from registration where role= 'Missionary' and userUid!= ? ");
			myStmt.setString(1, userUid);
		
			rs = myStmt.executeQuery();

			while (rs.next()) {
				Missionary missionary = new Missionary();
				missionary.setFullName(rs.getString("fullName"));
				missionary.setUserUid(rs.getString("userUid"));
				missionary.setDeviceId(rs.getString("deviceId"));
				missionary.setEmail(rs.getString("email"));
				fullNameUserIDList.add(missionary);
			}

			

			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return fullNameUserIDList;

	}
	
	public ArrayList<Missionary> getUserUidFulNameMappedToRescuer(String rUid) {
		ArrayList<Missionary> fullNameUserIDList = new ArrayList<Missionary>();

		ResultSet rs = null;
		Connection connection = null;
		PreparedStatement myStmt = null;
		try {

			connection = createJDBCConnection.getConnection();
			// getting each user's rescuers
			// here in the particular query userUid is the rescuer uuid
			myStmt = connection.prepareStatement("select fullName , userUid ,deviceId ,email, mobileNum from registration where rescuer1= ? or rescuer2= ? or rescuer3= ? or rescuer4= ?  ");

			myStmt.setString(1, rUid);
			myStmt.setString(2, rUid);
			myStmt.setString(3, rUid);
			myStmt.setString(4, rUid);
			rs = myStmt.executeQuery();

			while (rs.next()) {
				Missionary missionary = new Missionary();
				missionary.setFullName(rs.getString("fullName"));
				missionary.setUserUid(rs.getString("userUid"));
				missionary.setDeviceId(rs.getString("deviceId"));
				missionary.setEmail(rs.getString("email"));
				missionary.setMobile(rs.getString("mobileNum"));
				fullNameUserIDList.add(missionary);
			}

			

			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return fullNameUserIDList;

	}

}
