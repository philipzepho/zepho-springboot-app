package com.blt.zepho.dao;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.blt.zepho.jdbc.CreateJDBCConnection;
import com.blt.zepho.model.AlertsFromOthers;
import com.blt.zepho.model.Missionary;

@Repository
public class DoNotGoDao {
	
	@Autowired
	CreateJDBCConnection createJDBCConnection;
	
	public String addAlertFromOther(List<AlertsFromOthers> alertsFromOthersList){
		
	     Connection connection = null;		
      
      try {           
          connection = createJDBCConnection.getConnection();
      
          // the mysql insert statement
          String query = "insert into alertsfromothers (uuid, fromUserName, fromUserUid, toUserUid, isWatched, createdAt, message, fromUserRole, attachmentPath) "+ " values(?,?,?,?,?,?,?,?,?)" ;

          // create the mysql insert preparedstatement
          java.sql.PreparedStatement preparedStmt =  connection.prepareStatement(query);
         
           // execute the preparedstatement
          int i = 0;
          for (AlertsFromOthers alertsFromOthers : alertsFromOthersList) {
        	  preparedStmt.setString(1, alertsFromOthers.getUuid());
              preparedStmt.setString(2, alertsFromOthers.getFromUserName());
              preparedStmt.setString(3, alertsFromOthers.getFromUserUid());
              preparedStmt.setString(4, alertsFromOthers.getToUserUid());
              preparedStmt.setBoolean(5, alertsFromOthers.isWatched());
              preparedStmt.setLong(6, alertsFromOthers.getCreatedAt());
              preparedStmt.setString(7, alertsFromOthers.getMessage());
              preparedStmt.setString(8, alertsFromOthers.getFromUserRole());
              preparedStmt.setString(9, alertsFromOthers.getAttachmentPath());
              preparedStmt.addBatch();
              i++;

              if (i % 500 == 0 || i == alertsFromOthersList.size()) {
            	  preparedStmt.executeBatch(); // Execute every 5000 items.
              }
          }          
          //preparedStmt.executeUpdate();   
          connection.close();
          
          
      } catch (SQLException e) {
    	  e.printStackTrace();
          return "INSERTION_FIAL";
      } finally {
          if (connection != null) {
              try {
                  connection.close();
              } catch (SQLException e) {
                  e.printStackTrace();
              }
          }
      }
	return "SUCCESS";
}
	
	
	public ArrayList<AlertsFromOthers> getAlertList(String rUid) {
		ArrayList<AlertsFromOthers> alertsFromOthersList = new ArrayList<AlertsFromOthers>();

		ResultSet rs = null;
		Connection connection = null;
		PreparedStatement myStmt = null;
		try {

			connection = createJDBCConnection.getConnection();
			// getting each user's rescuers
			// here in the particular query userUid is the rescuer uuid
			myStmt = connection.prepareStatement("select fromUserName , fromUserUid ,createdAt ,uuid ,message ,fromUserRole, attachmentPath from alertsfromothers where toUserUid= ? and isWatched= false ");

			myStmt.setString(1, rUid);
			
			rs = myStmt.executeQuery();

			while (rs.next()) {
				AlertsFromOthers alert = new AlertsFromOthers();
				alert.setFromUserName(rs.getString("fromUserName"));
				alert.setFromUserUid(rs.getString("fromUserUid"));
				alert.setUuid(rs.getString("uuid"));
				alert.setCreatedAt(rs.getLong("createdAt"));
				alert.setMessage(rs.getString("message"));
				alert.setFromUserRole(rs.getString("fromUserRole"));
				alert.setAttachmentPath(rs.getString("attachmentPath"));
				alert.setImageData("");
				alertsFromOthersList.add(alert);
			}

			System.out.println("MISSIONARY LIST");

			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return alertsFromOthersList;

	}


	public ArrayList<String> loadStatesByOrg(String org) {
		ArrayList<String> stateList = new ArrayList<String>();

		ResultSet rs = null;
		Connection connection = null;
		PreparedStatement myStmt = null;
		try {
			connection = createJDBCConnection.getConnection();
			myStmt = connection.prepareStatement("select DISTINCT state from registration where organization= ? and isApproved= true and state!=''");

			myStmt.setString(1, org);
			
			rs = myStmt.executeQuery();

			while (rs.next()) {
				String state = rs.getString("state");				
				stateList.add(state);
			}

			System.out.println("State LIST");

			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return stateList;
	}


	public ArrayList<Missionary> getSelectedRoleUsers(ArrayList<Missionary> selectedMembers, String state, String org) {
		ArrayList<Missionary> allUsersList = new ArrayList<Missionary>();

		ResultSet rs = null;
		Connection connection = null;
		PreparedStatement myStmt = null;
		try {

			String roleQuery = "";
			String stateQuery = "";
			for(Missionary m: selectedMembers){
				if(roleQuery == ""){
					roleQuery = " and ( role='"+m.getFullName()+"' ";
				}else{
					roleQuery += " or role='"+m.getFullName()+"' ";
				}
			}
			
			if(state != "" && state != null && !state.equalsIgnoreCase("All States")){
				stateQuery = " state = '"+state+"' ";
			}
			
			if(roleQuery != "" && stateQuery != ""){
				roleQuery += " ) and "+stateQuery;
			}else if(roleQuery != "" && stateQuery == ""){
				roleQuery += " ) ";
			}else if(roleQuery == "" && stateQuery != ""){
				roleQuery = stateQuery;
			}
			
			
			connection = createJDBCConnection.getConnection();
			myStmt = connection.prepareStatement("select fullName , userUid ,deviceId ,email, mobileNum from registration where isApproved= true and "
					+ "organization ='"+org+"' "+roleQuery);

			
			rs = myStmt.executeQuery();
			
			while (rs.next()) {
				Missionary user = new Missionary();
				user.setFullName(rs.getString("fullName"));
				user.setUserUid(rs.getString("userUid"));
				user.setDeviceId(rs.getString("deviceId"));
				user.setEmail(rs.getString("email"));
				user.setMobile(rs.getString("mobileNum"));
				allUsersList.add(user);
			}

			System.out.println("Users LIST");

			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return allUsersList;
	}


	public String getImagePath(AlertsFromOthers alertsFromOthers) {
		String imagePath = null;

		ResultSet rs = null;
		Connection connection = null;
		PreparedStatement myStmt = null;
		try {

			connection = createJDBCConnection.getConnection();
			myStmt = connection.prepareStatement("select attachmentPath from alertsfromothers where toUserUid= ? and uuid= ? ");

			myStmt.setString(1, alertsFromOthers.getToUserUid());
			myStmt.setString(2, alertsFromOthers.getUuid());
			
			rs = myStmt.executeQuery();

			while (rs.next()) {
				imagePath = rs.getString("attachmentPath");
			}
			
			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return imagePath;
	}
	
	public long getAlertCount(String rUid) {
		ArrayList<AlertsFromOthers> alertsFromOthersList = new ArrayList<AlertsFromOthers>();

		ResultSet rs = null;
		Connection connection = null;
		PreparedStatement myStmt = null;
		long count = 0;
		
		// Getting today and tomorrow date time stamp
		String pattern = "yyyy-MM-dd";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

		LocalDate today = LocalDate.now();
	    LocalDate tomorrow = today.plusDays(1);
		Date todayDate = new Date();
		Date tomorrowDate = new Date();
		try {
			todayDate = simpleDateFormat.parse(today.toString());
			tomorrowDate = simpleDateFormat.parse(tomorrow.toString());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
		
		try {

			connection = createJDBCConnection.getConnection();
			// getting each user's rescuers
			// here in the particular query userUid is the rescuer uuid
			myStmt = connection.prepareStatement("select count(*) from alertsfromothers where toUserUid= ? and isWatched= false "
						+ " and CAST(createdAt AS UNSIGNED) >= ? and CAST(createdAt AS UNSIGNED) <= ?");	
			myStmt.setString(1, rUid);
			myStmt.setLong(2, todayDate.getTime());
			myStmt.setLong(3, tomorrowDate.getTime());
			
			rs = myStmt.executeQuery();

			while (rs.next()) {
				count = rs.getLong(1);
			}

			System.out.println("MISSIONARY LIST");

			connection.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return count;

	}

}
