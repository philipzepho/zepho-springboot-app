package com.blt.zepho.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.blt.zepho.jdbc.CreateJDBCConnection;
import com.blt.zepho.model.Registration;

@Repository
public class MasterAdminDao {

	static Logger log = Logger.getLogger(MasterAdminDao.class.getName());
	
	@Autowired
	CreateJDBCConnection createJDBCConnection;
	
	public ArrayList<Registration> getSuperAdminList(String organization){
		ResultSet rs = null;
		Connection connection = null;
		PreparedStatement myStmt = null;
		ArrayList<Registration> superAdminList = new ArrayList<Registration>();
		try {

			connection = createJDBCConnection.getConnection();
			
			myStmt = connection.prepareStatement("select fullName from registration where organization=? "
					+ " and role= ? ");

			myStmt.setString(1, "organization");
			myStmt.setString(2, "SuperAdmin");
			rs = myStmt.executeQuery();

			while (rs.next()) {
				Registration registration= new Registration();
	
				registration.setFullName(rs.getString("fullName"));			
				superAdminList.add(registration);
			}

			
			

			connection.close();

			return superAdminList;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					log.fatal(e.getMessage());
					e.printStackTrace();
				}
			}
			
		}
		

	}
	public ArrayList<Registration> getActivityList(String superAdmin) {
		ResultSet rs = null;
		Connection connection = null;
		PreparedStatement myStmt = null;
		ArrayList<Registration> activityList = new ArrayList<Registration>();
		try {

			connection = createJDBCConnection.getConnection();
			
			myStmt = connection.prepareStatement("select * from registration where addedBy=?");
			
			/*String query = "select * from people where (first_name = :name or last_name = :name) and address = :address");
					NamedParameterJdbcTemplate p = new NamedParameterStatement(con, query);
					p.setString("name", name);
					p.setString("address", address);*/

			myStmt.setString(1, superAdmin);
			rs = myStmt.executeQuery();

			while (rs.next()) {	
				Registration registration = new Registration();
				registration.setEmail(rs.getString("email"));
				registration.setRole(rs.getString("role"));
				registration.setFullName(rs.getString("fullName"));
				registration.setAddedBy(rs.getString("addedBy"));
				registration.setMobileNum(rs.getString("mobileNum"));
				registration.setUserUid(rs.getString("userUid"));
				registration.setState(rs.getString("state"));
				registration.setCity(rs.getString("city"));
				registration.setDivision(rs.getString("division"));
				registration.setRescuertype(rs.getString("rescuertype"));	
				registration.setIsApproved(rs.getBoolean("isApproved"));
				activityList.add(registration);
			}

			
			

			connection.close();

			return activityList;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					log.fatal(e.getMessage());
					e.printStackTrace();
				}
			}
			
		}
	}
	
}
