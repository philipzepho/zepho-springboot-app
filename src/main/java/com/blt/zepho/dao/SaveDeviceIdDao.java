package com.blt.zepho.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.blt.zepho.jdbc.CreateJDBCConnection;
import com.blt.zepho.model.Registration;

@Repository
public class SaveDeviceIdDao {
	static Logger log = Logger.getLogger(SaveDeviceIdDao.class.getName());
	
	@Autowired
	CreateJDBCConnection createJDBCConnection;
	
	public boolean saveDeviceIDRegTable(Registration reg) {

		
		boolean rs = true;
		Connection connection = null;
		int status = 0;

		if (reg.getUserUid() == null)
			return false;
		try {
			connection = createJDBCConnection.getConnection();

			String query = "update registration SET deviceId=? , loginAttempts=? where  userUid= ? ";

			// create the mysql insert preparedstatement
			java.sql.PreparedStatement preparedStmt = connection.prepareStatement(query);
			preparedStmt.setString(1, reg.getDeviceId());
			preparedStmt.setInt(2, 0);
			preparedStmt.setString(3, reg.getUserUid());

			status = preparedStmt.executeUpdate();

			connection.close();

		} catch (SQLException e) {
			log.fatal(e.getMessage());
			return false;
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					log.fatal(e.getMessage());
				}
			}
		}
		if(status > 0){
			return rs;
		}else{
			return false;
		}
		

	}
	
	public boolean saveDeviceIDRescuerTable(Registration reg) {

		System.out.println();
		boolean rs = true;
		Connection connection = null;

		if (reg.getUserUid() == null)
			return false;
		try {
			connection = createJDBCConnection.getConnection();

			String query = "update rescuer SET deviceId=? where  userUid= ? ";

			// create the mysql insert preparedstatement
			java.sql.PreparedStatement preparedStmt = connection.prepareStatement(query);
			preparedStmt.setString(1, reg.getDeviceId());
			preparedStmt.setString(2, reg.getUserUid());

			preparedStmt.executeUpdate();

			connection.close();

		} catch (SQLException e) {
			log.fatal(e.getMessage());
			return false;
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					log.fatal(e.getMessage());
				}
			}
		}
		return rs;

	}

	
	public Registration getUserDeviceIdByUID(String uID) {
		Registration register = new Registration();
		ResultSet rs = null;
		Connection connection = null;
		PreparedStatement myStmt = null;		
		try {
			connection = createJDBCConnection.getConnection();
			myStmt = connection.prepareStatement("select * from registration where userUid  =?");
			myStmt.setString(1, uID);
			rs = myStmt.executeQuery();
			while (rs.next()) {				
				register.setDeviceId(rs.getString("deviceId"));
				register.setEmail(rs.getString("email"));
				register.setFullName(rs.getString("fullName"));
				register.setRole(rs.getString("role"));
				register.setIsApproved(rs.getBoolean("isApproved"));
				register.setIsRejected(rs.getBoolean("isRejected"));
				register.setOrganization(rs.getString("organization"));
				register.setMobileNum(rs.getString("mobileNum"));
			}
			connection.close();

		} catch (SQLException e) {
			log.fatal(e);
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					log.fatal(e);
				}
			}
		}
		return register;
	}

	public boolean removeDeviceId(String userUid) {
		boolean rs = true;
		Connection connection = null;
		int status = 0;

		if (userUid == null || userUid == "")
			return false;
		try {
			connection = createJDBCConnection.getConnection();
			String query = "update registration SET deviceId=? where userUid= ? ";
			// create the mysql insert preparedstatement
			java.sql.PreparedStatement preparedStmt = connection.prepareStatement(query);
			preparedStmt.setString(1, null);
			preparedStmt.setString(2, userUid);
			status = preparedStmt.executeUpdate();
			connection.close();

		} catch (SQLException e) {
			log.fatal(e.getMessage());
			return false;
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					log.fatal(e.getMessage());
				}
			}
		}
		if(status > 0){
			return rs;
		}else{
			return false;
		}
	}

}
