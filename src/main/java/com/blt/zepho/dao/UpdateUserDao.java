package com.blt.zepho.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.blt.zepho.jdbc.CreateJDBCConnection;
import com.blt.zepho.model.UserDetails;
import com.blt.zepho.services.Password;

@Repository
public class UpdateUserDao {

	
	static Logger log = Logger.getLogger(UpdateUserDao.class.getName());
	
	@Autowired
	CreateJDBCConnection createJDBCConnection;
	
	public int updateUser(UserDetails userDetails) {
		// ResultSet rs = null;
	   boolean isPasswordChanged=false;
       Connection connection = null;	       
       
       if(userDetails.getPassword().equals("") || userDetails.getPassword()==null){
    	   
       }else{
    	   isPasswordChanged=true;
    	   
    	   userDetails.setPassword(Password.hashPassword(userDetails.getPassword()));
    	   log.debug("Password Changed..");
       }
	       
	       
	      String res1="", res2="", res3="", res4 = "";
	      
	      if(userDetails.getRescuer1() != null && userDetails.getRescuer1() != ""){
	    	  res1 = " , rescuer1='"+userDetails.getRescuer1()+"' ";
	      }
	      if(userDetails.getRescuer2() != null && !userDetails.getRescuer2().equalsIgnoreCase("")){
	    	  res2 = " , rescuer2='"+userDetails.getRescuer2()+"' ";
	      }
	      if(userDetails.getRescuer3() != null && !userDetails.getRescuer3().equalsIgnoreCase("") ){
	    	  res3 = " , rescuer3='"+userDetails.getRescuer3()+"' ";
	      }
	      if(userDetails.getRescuer4() != null && !userDetails.getRescuer4().equalsIgnoreCase("")){
	    	  res4 = " , rescuer4='"+userDetails.getRescuer4()+"' ";
	      }
	       
	       try {           
	           connection = createJDBCConnection.getConnection();
	           
	          
	           String query;
	           if(isPasswordChanged){
	        	   
	        	   // the mysql update statement
		          query = "update registration SET email=? , mobileNum=?  " + res1 + res2 + res3 + res4 +" , emergencyNum=? , isApproved=?, isRejected=?, password=? where userUid =? "; 
	        	   
	           }else{
	        	  query = "update registration SET email=? , mobileNum=? " + res1 + res2 + res3 + res4 +",  emergencyNum=?, isApproved=?, isRejected=? where userUid =? ";
	        	   
	           }
	           
	          
	          
	           // create the mysql insert preparedstatement
	           java.sql.PreparedStatement preparedStmt =  connection.prepareStatement(query);
	           preparedStmt.setString(1, userDetails.getEmail());
	           preparedStmt.setString(2, userDetails.getMobileNum());
	           preparedStmt.setString(3, userDetails.getEmergencyNum());
	           preparedStmt.setBoolean(4, userDetails.isApproved());
	           preparedStmt.setBoolean(5, !userDetails.isApproved());//reject user
	           
	           if(isPasswordChanged){
	        	   preparedStmt.setString(6, userDetails.getPassword());
	        	   preparedStmt.setString(7, userDetails.getUserUid());
	           }else{
	        	   preparedStmt.setString(6, userDetails.getUserUid());
	           }
	           
	           
	           
	           
	           
	         
	           // execute the preparedstatement
	          preparedStmt.execute();
	           
	           connection.close();
	           
	           
	       } catch (SQLException e) {
	           log.fatal(e.getMessage());
	       } finally {
	           if (connection != null) {
	               try {
	                   connection.close();
	               } catch (SQLException e) {
	            	   
	            	   log.fatal(e.getMessage());
	               }
	           }
	       }
		return 1;
	}
	public void removeAssignmentsUser(UserDetails userDetails) {
		Connection connection = null;
		PreparedStatement preparedStmt = null;
		ResultSet rs = null;
		String query = "";
		try {		
			if(userDetails.getRole().equals("Rescuer")){
				query = "update registration SET rescuer1=? where rescuer1 =? and  organization=?";		
				
				connection = createJDBCConnection.getConnection();
				preparedStmt = connection.prepareStatement(query);
				preparedStmt.setString(1, null);
				preparedStmt.setString(2, userDetails.getUserUid());
				preparedStmt.setString(3, userDetails.getOrganization());
				preparedStmt.execute();
				
				query = "update registration SET rescuer2=? where rescuer2 =? and  organization=?";
				preparedStmt = connection.prepareStatement(query);
				preparedStmt.setString(1, null);
				preparedStmt.setString(2, userDetails.getUserUid());
				preparedStmt.setString(3, userDetails.getOrganization());
				preparedStmt.execute();
				
				query = "update registration SET rescuer3=? where rescuer3 =? and  organization=?";
				preparedStmt = connection.prepareStatement(query);
				preparedStmt.setString(1, null);
				preparedStmt.setString(2, userDetails.getUserUid());
				preparedStmt.setString(3, userDetails.getOrganization());
				preparedStmt.execute();
				
				query = "update registration SET rescuer4=? where rescuer4 =? and  organization=?";
				preparedStmt = connection.prepareStatement(query);
				preparedStmt.setString(1, null);
				preparedStmt.setString(2, userDetails.getUserUid());
				preparedStmt.setString(3, userDetails.getOrganization());
				preparedStmt.execute();			
				
			}else if(userDetails.getRole().equals("Admin")){
				query = "update registration SET approveByAdminUid=? where approveByAdminUid =? and  organization=?";		
				
				connection = createJDBCConnection.getConnection();
				preparedStmt = connection.prepareStatement(query);
				preparedStmt.setString(1, null);
				preparedStmt.setString(2, userDetails.getUserUid());
				preparedStmt.setString(3, userDetails.getOrganization());
				preparedStmt.execute();
			}
			connection.close();

		} catch (SQLException e) {
			log.fatal(e);
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					log.fatal(e);
				}
			}
		}
		
	}


}
