package com.blt.zepho.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.blt.zepho.jdbc.CreateJDBCConnection;

import com.blt.zepho.model.Organization;

@Repository
public class GetOrgnizationListDao {

	static Logger log = Logger.getLogger(GetOrgnizationListDao.class.getName());
	
	@Autowired
	CreateJDBCConnection createJDBCConnection;
	
	public ArrayList<Organization> doGetOrgnizationListDao(	) {
		ResultSet rs = null;
		Connection connection = null;
		PreparedStatement myStmt = null;
		ArrayList<Organization> organizationList = new ArrayList<Organization>();
		try {

			connection = createJDBCConnection.getConnection();
			
			myStmt = connection.prepareStatement("select *  from organizations");

			
			rs = myStmt.executeQuery();

			while (rs.next()) {
				Organization organization= new Organization();
				
	
				organization.setOrgFullName(rs.getString("orgFullName"));
				organization.setOrgShortName(rs.getString("orgShortName"));	
				organization.setInviteCode(rs.getString("inviteCode"));	
				organizationList.add(organization);
			}

			
			

			connection.close();

			return organizationList;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					log.fatal(e.getMessage());
					e.printStackTrace();
				}
			}
			
		}
		

	}
	
}
