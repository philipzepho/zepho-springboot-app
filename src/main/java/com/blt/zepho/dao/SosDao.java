package com.blt.zepho.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.blt.zepho.jdbc.CreateJDBCConnection;
import com.blt.zepho.model.Registration;
import com.blt.zepho.model.Rescuer;
import com.blt.zepho.model.Sos;

@Repository
public class SosDao {
	static Logger log = Logger.getLogger(SosDao.class.getName());
	
	@Autowired
	CreateJDBCConnection createJDBCConnection;
	
	public int addSosData(Sos sos) {
		
		
	       Connection connection = null;
	
	      
	       try {           
	           connection = createJDBCConnection.getConnection();
	       
	           
	           // the mysql insert statement
	           String query = "insert into sos (createdAt, latitude, longitude, userUid, uuid, fromUser,type,locationName,onBehalf,obUserName,obUserUid,locality,postalCode,subLocality,subThoroughfare,thoroughfare) "+ " values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)" ;

	           // create the mysql insert preparedstatement
	           java.sql.PreparedStatement preparedStmt =  connection.prepareStatement(query);
	           preparedStmt.setLong(1, sos.getCreatedAt());
	           preparedStmt.setDouble(2, sos.getLatitude());
	           preparedStmt.setDouble(3, sos.getLongitude());
	           preparedStmt.setString(4, sos.getUserUid());
	           preparedStmt.setString(5, sos.getUuid());
	           preparedStmt.setString(6, sos.getFromUser());
	           preparedStmt.setString(7, sos.getType());
	           preparedStmt.setString(8, sos.getLocationName());
	           preparedStmt.setBoolean(9, sos.isOnBehalf());
	           preparedStmt.setString(10, sos.getObUserName());
	           preparedStmt.setString(11, sos.getObUserUid());
	           preparedStmt.setString(12,sos.getLocality());
	           preparedStmt.setString(13,sos.getPostalCode());
	           preparedStmt.setString(14, sos.getSubLocality());
	           preparedStmt.setString(15, sos.getSubThoroughfare());
	           preparedStmt.setString(16, sos.getThoroughfare());

	           // execute the preparedstatement
	           //System.out.println();
	           
	           //preparedStmt.execute();
	           preparedStmt.execute();
	           //System.out.println(result.getRow());
	           
	          
	           
	           connection.close();
	           
	           
	       } catch (SQLException e) {
	           
	    	   log.fatal(e.getMessage());
	       } finally {
	           if (connection != null) {
	               try {
	                   connection.close();
	               } catch (SQLException e) {
	            	   log.fatal(e.getMessage());
	               }
	           }
	       }
		return -1;
	}
	public ArrayList<Rescuer> getRescuersByPin(int userPincode, String org, String rescuerIds) {
		Registration saReg = new Registration();
		ArrayList<Rescuer> resList = new ArrayList<Rescuer>();
		ResultSet rs = null;
		Connection connection = null;
		PreparedStatement myStmt = null;	
		if(org != "" && org != null && userPincode > 0){
			try {				
				connection = createJDBCConnection.getConnection();
				myStmt = connection.prepareStatement("select * from registration where role='Rescuer' and organization=? and isApproved=? and pincode=? and userUid not in("+rescuerIds+")");
				myStmt.setString(1, org);
				myStmt.setBoolean(2, true);
				myStmt.setInt(3, userPincode);
				rs = myStmt.executeQuery();
				while (rs.next()) {	
					Rescuer rescuer = new Rescuer();
					rescuer.setEmail(rs.getString("email"));
					rescuer.setDeviceId(rs.getString("deviceId"));
					rescuer.setFullName(rs.getString("fullName"));
					rescuer.setOrg(rs.getString("organization"));
					rescuer.setMobile(rs.getString("mobileNum"));
					resList.add(rescuer);
				}
				connection.close();
	
			} catch (SQLException e) {
				log.fatal(e);
			} finally {
				if (connection != null) {
					try {
						connection.close();
					} catch (SQLException e) {
						log.fatal(e);
					}
				}
			}
		}
		if(resList.size() > 0){
			return resList;
		}else{
			return new ArrayList<Rescuer>();
		}
	}

}
