package com.blt.zepho.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.blt.zepho.jdbc.CreateJDBCConnection;
import com.blt.zepho.model.Rescuer;

@Repository
public class UserUidFullNameDao {

	static Logger log = Logger.getLogger(UserUidFullNameDao.class.getName());
	
	@Autowired
	CreateJDBCConnection createJDBCConnection;
	
	public ArrayList<Rescuer> getUserUidFulName(String org) {
		ArrayList<Rescuer> fullNameUserIDList = new ArrayList<Rescuer>();

		ResultSet rs = null;
		Connection connection = null;
		PreparedStatement myStmt = null;
		try {

			connection = createJDBCConnection.getConnection();
			// getting each user's rescuers
			// here in the particular query userUid is the rescuer uuid
			myStmt = connection.prepareStatement("select fullName , userUid from rescuer where organization=?");
			myStmt.setString(1, org);

		
			rs = myStmt.executeQuery();

			while (rs.next()) {
				Rescuer rescuer = new Rescuer();
				rescuer.setLabel(rs.getString("fullName"));
				rescuer.setUserUid(rs.getString("userUid"));
				fullNameUserIDList.add(rescuer);
			}

			System.out.println("deviceId");

			connection.close();

		} catch (SQLException e) {
			log.fatal(e.getMessage());
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					log.fatal(e.getMessage());
				}
			}
		}
		return fullNameUserIDList;

	}
}
