package com.blt.zepho.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.blt.zepho.jdbc.CreateJDBCConnection;
import com.blt.zepho.model.Sos;

@Repository
public class SosAlertListDao {
	static Logger log = Logger.getLogger(SosAlertListDao.class.getName());
	
	@Autowired
	CreateJDBCConnection createJDBCConnection;
	
	public ArrayList<String> GetSosAlertList(String reg) {
		ResultSet rs = null;
		Connection connection = null;
		PreparedStatement myStmt = null;
		ArrayList<String> stringList = new ArrayList<String>();
		try {

			connection = createJDBCConnection.getConnection();
			// getting each user's rescuers
			// here in the particular query userUid is the user who presses sos
			// button
			
			myStmt = connection.prepareStatement("select uuid from soswatch where userUid = ? and isWatched=false");

			myStmt.setString(1, reg);
			rs = myStmt.executeQuery();

			while (rs.next()) {

				if (rs.getString("uuid") != "") {
					stringList.add(rs.getString("uuid"));
					
				}

			}

			
			

			connection.close();

		} catch (SQLException e) {
			log.fatal(e.getMessage());
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					log.fatal(e.getMessage());
				}
			}
		}
		return stringList;

	}

	public ArrayList<Sos> GetUserSosAlertList(String reg) {
		ResultSet rs = null;
		Connection connection = null;
		PreparedStatement myStmt = null;
	//	ArrayList<Sos> sosList = new ArrayList<Sos>();
		ArrayList<Sos> sosList = new ArrayList<>();
		try {

			connection = createJDBCConnection.getConnection();
			// getting each user's rescuers
			// here in the particular query userUid is the user who presses sos
			// button
			myStmt = connection.prepareStatement("select * from sos where uuid in (select uuid from soswatch where userUid= ? and isWatched=false)");			

			myStmt.setString(1, reg);
			rs = myStmt.executeQuery();

			while (rs.next()) {
				Sos sos = new Sos();
				sos.setCreatedAt(rs.getLong("createdAt"));
				sos.setFromUser(rs.getString("fromUser"));
				sos.setLatitude(rs.getDouble("latitude"));
				sos.setLongitude(rs.getDouble("longitude"));
				sos.setUserUid(rs.getString("userUid"));
				sos.setUuid(rs.getString("uuid"));
				sos.setType(rs.getString("type"));
				sos.setLocationName(rs.getString("locationName"));
				sos.setOnBehalf(rs.getBoolean("onBehalf"));
				sos.setObUserName(rs.getString("obUserName"));
				sos.setObUserUid(rs.getString("obUserUid"));
				sos.setLocality(rs.getString("locality"));
				
				sos.setSubLocality(rs.getString("subLocality"));
				sos.setSubThoroughfare(rs.getString("subThoroughfare"));
				sos.setThoroughfare(rs.getString("thoroughfare"));
				sosList.add(sos);
				
			}

			

			connection.close();

		} catch (SQLException e) {
			log.fatal(e.getMessage());
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					log.fatal(e.getMessage());
				}
			}
		}
		return sosList;

	}
	
	public long getUserSosAlertCount(String reg) {
		ResultSet rs = null;
		Connection connection = null;
		PreparedStatement myStmt = null;
		long count = 0;
		
		// Getting today and tomorrow date time stamp
		String pattern = "yyyy-MM-dd";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

		LocalDate today = LocalDate.now();
	    LocalDate tomorrow = today.plusDays(1);
		Date todayDate = new Date();
		Date tomorrowDate = new Date();
		try {
			todayDate = simpleDateFormat.parse(today.toString());
			tomorrowDate = simpleDateFormat.parse(tomorrow.toString());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
		
		
		try {
			connection = createJDBCConnection.getConnection();
			myStmt = connection.prepareStatement("select count(*) from sos where uuid in (select uuid from soswatch where userUid= ? and isWatched=false)"
					+ " and CAST(createdAt AS UNSIGNED) >= ? and CAST(createdAt AS UNSIGNED) <= ?");	
			myStmt.setString(1, reg);
			myStmt.setLong(2, todayDate.getTime());
			myStmt.setLong(3, tomorrowDate.getTime());
			rs = myStmt.executeQuery();

			while (rs.next()) {
				count = rs.getLong(1);				
			}
			connection.close();

		} catch (SQLException e) {
			log.fatal(e.getMessage());
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					log.fatal(e.getMessage());
				}
			}
		}
		return count;

	}

}