package com.blt.zepho.services;

import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blt.zepho.dao.NewTripDao;
import com.blt.zepho.model.UserData;
import com.blt.zepho.model.NewTrip;
import com.blt.zepho.model.Sos;;

@Service
public class TripService {
	static Logger log = Logger.getLogger(TripService.class.getName());
	
	NewTrip newTrip;
	@Autowired
	NewTripDao newTripDao;
	
	String tripID = UUID.randomUUID().toString();	  
	   
	public Map<String,Object> newTripRequest(NewTrip newTrip){ 	   
		newTrip.setTripId(tripID);	   
		Map resultMap=new HashMap<String,Object>();	   
		this.newTrip=newTrip;
		log.debug("NEW TRIP :: " + newTrip.getSourcePlaceName());		
	    newTripDao.addNewTrip(newTrip);
	    String result=newTripDao.addCurrentTrip(newTrip);
	    resultMap.put("Result", result);
	    return resultMap;
	}
	   
	
	public Map<String,Object> checkCurrentTrip(UserData userUid){ 
		log.debug("INSIDE CURRENT TRIP: "+ userUid.getUserUid());
		Map<String,Object> resultMap=new HashMap<String,Object>();
		boolean result=newTripDao.checkCurrentTrip(userUid.getUserUid());
		resultMap.put("isFinished",""+result);
		return resultMap;			
	}
	   
	  
	   public Map<String,Object> endCurrentTrip( UserData endTrip){
		   Map<String,Object> resultMap=new HashMap<String,Object>();
		   String result=newTripDao.endCurrentTrip(endTrip.getUserUid());		  
		   resultMap.put("Success",result);		   
		   return resultMap;
	}

}
