package com.blt.zepho.services;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blt.zepho.dao.ClearSosItemDao;
import com.blt.zepho.model.ClearSosListItem;


@Service 
public class ClearSosItemService {
	static Logger log = Logger.getLogger(ClearSosItemService.class.getName());

	@Autowired
	ClearSosItemDao clearSosItemDao;  
	   
	   public Map<String,Object> clearSosItem(ClearSosListItem clearSosListItem){ 
		   Map<String, Object> result = new HashMap<String, Object>();
		  
		   log.debug("USER:: " + clearSosListItem.getUuid()+" :: "+ clearSosListItem.getUserUid());
		   
		   	boolean res=clearSosItemDao.clearSosItem(clearSosListItem);
		   	
		   	result.put("Success",""+res);
			return result;

			
	   }

}
