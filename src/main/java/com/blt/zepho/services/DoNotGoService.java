package com.blt.zepho.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.swing.plaf.synth.SynthSeparatorUI;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.blt.zepho.dao.ApprovalDao;
import com.blt.zepho.dao.DoNotGoDao;
import com.blt.zepho.dao.RegRescuerListDao;
import com.blt.zepho.dao.RescuerListByUserIdDao;
import com.blt.zepho.dao.SosAlertListDao;
import com.blt.zepho.model.AlertsFromOthers;
import com.blt.zepho.model.DoNotGo;
import com.blt.zepho.model.Missionary;
import com.blt.zepho.model.Registration;
import com.blt.zepho.model.Rescuer;
import com.blt.zepho.model.Sos;
import com.blt.zepho.model.SosWatch;
import com.blt.zepho.model.UserData;
import com.blt.zepho.util.FileUploadUtil;
import com.blt.zepho.util.WebKeys;

@Service
public class DoNotGoService {
	
	static Logger log = Logger.getLogger(DoNotGoService.class.getName());	
	
	@Autowired	
	DoNotGoDao doNotGoDao;
	
	@Autowired
	AndroidPushNotificationsService notificationsService;
	
	@Autowired
	SendOtpService sendOtpService;
	
	@Autowired
	SosAlertListDao sosalertlistdao;
	@Autowired
	ApprovalDao aprovalDao;
	
	@Autowired
	AsyncService asyncService;
	
	DoNotGo doNotGo = new DoNotGo();

	public Map<String,Object> sosRequest(DoNotGo obj) throws Exception {
		String uuid = UUID.randomUUID().toString();
		this.doNotGo=obj;
		String title;
		String body;
		this.doNotGo.setUuid(uuid);		
		boolean status = false;
		
		Map resultMap=new HashMap<String,Object>();

		
		ArrayList<Missionary> selectedMembers=doNotGo.getSelectedMembers();
		
		if(doNotGo.getFromUserRole().equalsIgnoreCase("Super Admin")){
			ArrayList<Missionary>  misList = doNotGoDao.getSelectedRoleUsers(selectedMembers, doNotGo.getState(), doNotGo.getOrg());
			if(misList.size() > 0){
				if(doNotGo.getImageData() != null && !doNotGo.getImageData().equalsIgnoreCase("")){
					String path = saveAttachement(doNotGo);
					if(path != null && path != ""){
						doNotGo.setFileName(path);
					}
				}else{
					doNotGo.setFileName(null);
				}
				//status = sendAlerts(misList);
				asyncService.sendAlerts(misList, doNotGo);
			}
			
		}else{
			doNotGo.setFileName(null);
			//status = runSendAlertsThread(selectedMembers);
			asyncService.sendAlerts(selectedMembers, doNotGo);
		}		
		/*for(int i=0;i<selectedMembers.size();i++){
			Missionary mis=selectedMembers.get(i);
			pushFCMNotification(mis.getDeviceId(),"From "+ doNotGo.getFromUserName(),doNotGo.getMessage());
			doNotGoDao.addAlertFromOther(new AlertsFromOthers(UUID.randomUUID().toString(),
					doNotGo.getFromUserName(), 
					doNotGo.getFromUserUid(), 
					mis.getUserUid(),
					false, 
					doNotGo.getCreatedAt(),
					doNotGo.getMessage(),
					doNotGo.getFromUserRole()));
			sendEmail(selectedMembers.get(i).getEmail(),doNotGo.getMessage());		
			
		}	*/

		if(status){
			resultMap.put("Success", "true");
		}else{
			resultMap.put("Success", "false");
		}
		
		return resultMap;
	}
	
	public Map<String, Object> alertsFromOthers(UserData userInfo) {
		
		System.out.println("inside GET RESCUERS");

		Map<String, Object> map = new HashMap<String, Object>();

		ArrayList<AlertsFromOthers> fullNameUserIDList = doNotGoDao.getAlertList(userInfo.getUserUid());

		if (fullNameUserIDList.isEmpty()) {
			map.put("Error", "No Notifications");

			return map;
		} else {
			Collections.sort(fullNameUserIDList, new ComparatorService());
			map.put("AlertList", fullNameUserIDList);
			map.put("size", fullNameUserIDList.size());
			return map;
		}

	}
	
	public Map<String, Object> loadStatesByOrg(String org) {
		Map<String, Object> map = new HashMap<String, Object>();

		ArrayList<String> stateList = doNotGoDao.loadStatesByOrg(org);

		if (stateList.isEmpty()) {
			map.put("Error", "No States");
			return map;
		} else {
			map.put("stateList", stateList);
			map.put("size", stateList.size());
			return map;
		}

	}
	
	

	
	
	public void sendEmail(String emailId,String msg) {
		
		
		Properties props = new Properties();		
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", WebKeys.EMAIL_PORT);
		props.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", WebKeys.EMAIL_PORT);
		Session session = Session.getDefaultInstance(props,
			new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(WebKeys.EMAIL_ID, WebKeys.EMAIL_PASSWORD);  
				}
			});

		try {

			System.out.println("EMAIL:: "+emailId);
			if(emailId==null){
				return;
			}
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(WebKeys.EMAIL_ID));
			message.setRecipients(Message.RecipientType.TO,
			InternetAddress.parse(emailId));
			message.setSubject("Reg: Safety Alert from Zepho Send By "+doNotGo.getFromUserName());
		    message.setText(msg);
			Transport.send(message);

			System.out.println("EMAIL SENT");

		} catch (MessagingException e) {
			log.fatal(e.getMessage());
			throw new RuntimeException(e);
		}
	}
	
	public boolean runSendAlertsThread(ArrayList<Missionary>  misList){
		boolean status = false;
		try{
			Runnable runnable = new Runnable() {
	            @Override
	            public void run() {
	                //System.out.println("Inside : " + Thread.currentThread().getName());
	                try{
	                	String mobileList = "";
		                for(int i=0;i<misList.size();i++){
		        			Missionary mis=misList.get(i);
		        			notificationsService.sendNotification(mis.getDeviceId(),"From "+ doNotGo.getFromUserName(),doNotGo.getMessage(), WebKeys.NOTIFICATION);
		        			AlertsFromOthers afo = new AlertsFromOthers(UUID.randomUUID().toString(),
		        					doNotGo.getFromUserName(), 
		        					doNotGo.getFromUserUid(), 
		        					mis.getUserUid(),
		        					false, 
		        					doNotGo.getCreatedAt(),
		        					doNotGo.getMessage(),
		        					doNotGo.getFromUserRole());
		        			if(doNotGo.getFileName() != null && doNotGo.getFileName() != ""){
		        				afo.setAttachmentPath(doNotGo.getFileName());
		        			}		        			
		        			//doNotGoDao.addAlertFromOther(afo);
		        			sendEmail(misList.get(i).getEmail(),doNotGo.getMessage());
		        			if(misList.get(i).getMobile() != null && misList.get(i).getMobile() != ""){
		        				if(mobileList == "" || mobileList == null){
			    					mobileList = misList.get(i).getMobile();
			    				}else{
			    					mobileList += ","+misList.get(i).getMobile();
			    				}
		        			}		        			
		        		}
		                if(mobileList != null && mobileList != ""){
		        			sendOtpService.sendSMSAlert(mobileList, "You received notification alert from "+doNotGo.getFromUserName()+". Please check Zepho app for more details");
		        		}
		                System.out.println("End of Thread...");
		                Thread.currentThread().interrupt();
	                }catch(Exception e){
	                	e.printStackTrace();
	                	Thread.currentThread().interrupt();
	                }
	            }
	        };
	        System.out.println("Creating Thread...");
	        Thread thread = new Thread(runnable);
	
	        System.out.println("Starting Thread...");
	        thread.start();
	        status = true;
		}catch(Exception e){
        	e.printStackTrace();
        }
		return status;
	}
	
	public String saveAttachement(DoNotGo dng){
		String imageData = dng.getImageData();
		String encData[] = imageData.split("base64,");
		byte[] data = Base64.getMimeDecoder().decode(encData[1]);
		String filePath = "";
		String commonPath = FileUploadUtil.getCommonFilesPath();
		try{			
			filePath = FileUploadUtil.getUploadImagePath(dng) + File.separator + dng.getFileName();
			OutputStream stream = new FileOutputStream(commonPath + filePath);
			stream.write(data);
		}
		catch (Exception e) {
			filePath = "";
			System.err.println("Couldn't write to file...");
			e.printStackTrace();
		}
		return filePath;
	}

	public Map<String, Object> getImageData(AlertsFromOthers alertsFromOthers) {
		Map<String, Object> map = new HashMap<String, Object>();
		
		String imagePath = doNotGoDao.getImagePath(alertsFromOthers);
		if(imagePath != null && imagePath != ""){
			File imageFile = new File(FileUploadUtil.getCommonFilesPath() + imagePath);
			String encodedFile = "";
			try{
				if(imageFile.exists()){
					FileInputStream fileInputStreamReader = new FileInputStream(imageFile);
		            byte[] bytes = new byte[(int)imageFile.length()];
		            fileInputStreamReader.read(bytes);
		            encodedFile = Base64.getEncoder().encodeToString(bytes);
				}			
			} catch (Exception e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	        } 
			

			if (encodedFile != "") {
				map.put("imageData", encodedFile);
				return map;
			} else {
				map.put("Error", "File not found");
				return map;			
			}
		}else{
			map.put("Error", "Not authorized");
			return map;	
		}
		
		
	}

	public Map<String, Object> getNotificationCount(Registration userReg) {
		Map<String, Object> map = new HashMap<String, Object>();
		try{
		if(userReg.getRole().equalsIgnoreCase(WebKeys.SUPER_ADMIN)){
			long sosCount = sosalertlistdao.getUserSosAlertCount(userReg.getUserUid());
			long approvalCount = aprovalDao.getApprovalCount(userReg);
			map.put("sosCount", sosCount);
			map.put("approvalCount", approvalCount);			
		}else if(userReg.getRole().equalsIgnoreCase(WebKeys.ADMIN)){			
			long approvalCount = aprovalDao.getApprovalCount(userReg);
			long notificationCount = doNotGoDao.getAlertCount(userReg.getUserUid());			
			map.put("approvalCount", approvalCount);
			map.put("notificationCount", notificationCount);			
		}else if(userReg.getRole().equalsIgnoreCase(WebKeys.RESCUER)){
			long sosCount = sosalertlistdao.getUserSosAlertCount(userReg.getUserUid());
			long notificationCount = doNotGoDao.getAlertCount(userReg.getUserUid());
			map.put("sosCount", sosCount);
			map.put("notificationCount", notificationCount);			
		}else if(userReg.getRole().equalsIgnoreCase(WebKeys.MISSIONARY) || userReg.getRole().equalsIgnoreCase(WebKeys.CHURCH_MEMBER)){
			long notificationCount = doNotGoDao.getAlertCount(userReg.getUserUid());
			map.put("notificationCount", notificationCount);	
		}
		}catch(Exception e){
			e.printStackTrace();
			map.put("Error", "No records found");
		}
		return map;
	}
	
	/*@Async
	public void sendAlerts(ArrayList<Missionary>  misList){
		try{
        	String mobileList = "";
        	List<AlertsFromOthers> alertList = new ArrayList<>();
            for(int i=0;i<misList.size();i++){
    			Missionary mis=misList.get(i);
    			notificationsService.sendNotification(mis.getDeviceId(),"From "+ doNotGo.getFromUserName(),doNotGo.getMessage(), WebKeys.NOTIFICATION);
    			AlertsFromOthers afo = new AlertsFromOthers(UUID.randomUUID().toString(),
    					doNotGo.getFromUserName(), 
    					doNotGo.getFromUserUid(), 
    					mis.getUserUid(),
    					false, 
    					doNotGo.getCreatedAt(),
    					doNotGo.getMessage(),
    					doNotGo.getFromUserRole());
    			if(doNotGo.getFileName() != null && doNotGo.getFileName() != ""){
    				afo.setAttachmentPath(doNotGo.getFileName());
    			}	
    			alertList.add(afo);
    			//doNotGoDao.addAlertFromOther(afo);
    			sendEmail(misList.get(i).getEmail(),doNotGo.getMessage());
    			if(misList.get(i).getMobile() != null && misList.get(i).getMobile() != ""){
    				if(mobileList == "" || mobileList == null){
    					mobileList = misList.get(i).getMobile();
    				}else{
    					mobileList += ","+misList.get(i).getMobile();
    				}
    			}		        			
    		}
            doNotGoDao.addAlertFromOther(alertList);
            if(mobileList != null && mobileList != ""){
    			sendOtpService.sendSMSAlert(mobileList, "You received notification alert from "+doNotGo.getFromUserName()+". Please check Zepho app for more details");
    		}
            System.out.println("End of Thread...");
        }catch(Exception e){
        	e.printStackTrace();
        }
	}*/
	
}

class  ComparatorService  implements Comparator<AlertsFromOthers> {
	@Override
	public int compare(AlertsFromOthers alertsFromOthers1, AlertsFromOthers alertsFromOthers2) {
		long created1 = alertsFromOthers1.getCreatedAt();
		long created2 = alertsFromOthers2.getCreatedAt();
 
		if (created1 < created2) {
			return 1;
		} else if (created1 > created2) {
			return -1;
		} else {
			return 0;
		}
	}
}
