package com.blt.zepho.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blt.zepho.dao.UserUidFullNameDao;
import com.blt.zepho.model.Registration;
import com.blt.zepho.model.Rescuer;

@Service
public class GetRescuerListService {
	static Logger log = Logger.getLogger(GetRescuerListService.class.getName());
	@Autowired
	UserUidFullNameDao userUidFullNameDao;
	public Map<String, Object> getRescuerList(String org) {
		log.debug("inside GET RESCUERS");

		Map<String, Object> map = null;

		map = new HashMap<String, Object>();

		ArrayList<Rescuer> fullNameUserIDList = userUidFullNameDao.getUserUidFulName(org);

		if (fullNameUserIDList.isEmpty()) {
			map.put("Error", "No Rescuer");

			return map;
		} else {
			map.put("RescuerList", fullNameUserIDList);
			return map;
		}

	}

}
