package com.blt.zepho.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blt.zepho.dao.GetOrgnizationListDao;
import com.blt.zepho.model.Organization;
import com.blt.zepho.model.Registration;

@Service
public class GetOrganizationListService {
	static Logger log = Logger.getLogger(GetOrganizationListService.class.getName());
    
	@Autowired
	GetOrgnizationListDao getOrgnizationListDao;
	public Map<String, Object> GetOrganizationList() {
		log.debug("inside getOrganizationList");

		Map<String, Object> map = null;

		map = new HashMap<String, Object>();

		ArrayList<Organization> organizationList=	getOrgnizationListDao.doGetOrgnizationListDao();

		if (organizationList==null) {
			map.put("success", false);
			map.put("message", "organizationList empty");

			return map;
		} else {
			map.put("success", true);
			map.put("organizationsList", organizationList);
			return map;
		}

		

	}
}
