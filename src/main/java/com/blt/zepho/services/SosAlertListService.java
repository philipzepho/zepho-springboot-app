package com.blt.zepho.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blt.zepho.dao.ComparatorService;
import com.blt.zepho.dao.SosAlertListDao;
import com.blt.zepho.model.Registration;
import com.blt.zepho.model.Sos;

@Service
public class SosAlertListService {
	static Logger log = Logger.getLogger(SosAlertListService.class.getName());
	@Autowired
	SosAlertListDao sosalertlistdao;

	
	public Map<String, Object> sosAlertListService(Registration reg) throws Exception {
		// rename to sos alertlistdao
		Map<String, Object> mapz = new HashMap<String, Object>();

		ArrayList<Sos> sosList = new ArrayList<Sos>();
		sosList = sosalertlistdao.GetUserSosAlertList(reg.getUserUid());
		Collections.sort(sosList, new ComparatorService());
		
		if(!sosList.isEmpty()){
			mapz.put("sosAlertList", sosList);		
		}else{
			mapz.put("info", "Empty in sos table");
		}	
		
		return mapz;

	}
}
