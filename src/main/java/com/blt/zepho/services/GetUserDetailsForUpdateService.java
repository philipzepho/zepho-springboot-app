package com.blt.zepho.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blt.zepho.dao.UserNameUidUpdateDao;
import com.blt.zepho.dao.UserUidFullNameDao;
import com.blt.zepho.model.Rescuer;
import com.blt.zepho.model.UidandRole;
import com.blt.zepho.model.UserData;
import com.blt.zepho.model.UserDetails;


@Service
public class GetUserDetailsForUpdateService {
	static Logger log = Logger.getLogger(GetUserDetailsForUpdateService.class.getName());
	@Autowired
	UserNameUidUpdateDao userNameUidUpdateDao;

	public Map<String, Object> doGetRescuerListService(UidandRole userData) {
		log.debug("inside GET RESCUERS");

		Map<String, Object> map = null;

		map = new HashMap<String, Object>();

		ArrayList<UserDetails> fullNameUserIDList = userNameUidUpdateDao.getUserUidFulName(userData);

		if (fullNameUserIDList.isEmpty()) {
			map.put("Error", "No Users");

			return map;
		} else {
			map.put("UsersList", fullNameUserIDList);
			return map;
		}

	}

}


