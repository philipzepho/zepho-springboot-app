package com.blt.zepho.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.blt.zepho.util.HeaderRequestInterceptor;
import com.blt.zepho.util.WebKeys;
import com.google.firebase.messaging.FirebaseMessagingException;


@Service
public class AndroidPushNotificationsService {
	
	@Autowired
	FirebaseMessagingSnippets fms;

	private static final String FIREBASE_SERVER_KEY = WebKeys.AUTH_KEY_FCM;
	private static final String FIREBASE_API_URL =  WebKeys.API_URL_FCM;
	
	@Async
	public CompletableFuture<String> send(HttpEntity<String> entity) {

		RestTemplate restTemplate = new RestTemplate();

		ArrayList<ClientHttpRequestInterceptor> interceptors = new ArrayList<>();
		interceptors.add(new HeaderRequestInterceptor("Authorization", "key=" + FIREBASE_SERVER_KEY));
		interceptors.add(new HeaderRequestInterceptor("Content-Type", "application/json"));
		restTemplate.setInterceptors(interceptors);

		String firebaseResponse = restTemplate.postForObject(FIREBASE_API_URL, entity, String.class);

		return CompletableFuture.completedFuture(firebaseResponse);
	}
	
	@Async
	public ResponseEntity<String> sendNotification(String deviceToken,String title,String message, String type) throws Exception {

		if(deviceToken==null){
			return null;
		}		
		try {
			fms.sendToToken(deviceToken, title, message, type);
		} catch (FirebaseMessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/*//deviceToken="Parent";
		JSONObject body = new JSONObject();
		body.put("to",deviceToken);
		body.put("priority", "high");

		JSONObject notification = new JSONObject();
		notification.put("title", title);
		notification.put("body", message);
		//String GAME_PLAY_REQUEST_FORMAT = "%@ and %@ have invited you to play Monopoly";
		//notification.put("body_loc_key", GAME_PLAY_REQUEST_FORMAT);
		//String names[] = {"Jenna", "Frank"};
		//notification.put("body_loc_args", names);
		//notification.put("tag", "MISSED_CALL");
		// body
		notification.put("sound", "default");
		notification.put("click_action", "FCM_PLUGIN_ACTIVITY");
		notification.put("icon", "fcm_push_icon");
		
		JSONObject data = new JSONObject();
		data.put("Key-1", "JSA Data 1");
		data.put("Key-2", "JSA Data 2");
		data.put("title", title); // Notification title
		data.put("body", message); // Notification
		data.put("type", type); // Notification
		
		JSONObject android = new JSONObject();
		JSONObject notif = new JSONObject();
		android.put("ttl", "86400s");
		notif.put("click_action", "FCM_PLUGIN_ACTIVITY");
		android.put("notification", notif);

		body.put("notification", notification);
		body.put("data", data);
		body.put("ttl", "86400s");
		
		JSONObject messageObj = new JSONObject();
		body.put("message", body);

		HttpEntity<String> request = new HttpEntity<>(body.toString());

		CompletableFuture<String> pushNotification = send(request);
		CompletableFuture.allOf(pushNotification).join();

		try {
			String firebaseResponse = pushNotification.get();
			return new ResponseEntity<>(firebaseResponse, HttpStatus.OK);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}*/

		// return new ResponseEntity<>("Push Notification ERROR!", HttpStatus.BAD_REQUEST);
		return new ResponseEntity<>("est", HttpStatus.OK);
	}

}