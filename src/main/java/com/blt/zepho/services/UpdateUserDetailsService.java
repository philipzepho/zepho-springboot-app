package com.blt.zepho.services;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blt.zepho.dao.RegistrationDao;
import com.blt.zepho.dao.RegistrationEmailValidationDao;
import com.blt.zepho.dao.RescuerDao;
import com.blt.zepho.dao.UpdateEmailValidationDao;
import com.blt.zepho.dao.UpdateUserDao;
import com.blt.zepho.model.Registration;
import com.blt.zepho.model.UserDetails;

@Service
public class UpdateUserDetailsService {


	static Logger log = Logger.getLogger(UpdateUserDetailsService.class.getName());

	@Autowired
	UpdateUserDao updateUserDao;
//	Registration reg = new Registration();
	@Autowired
	RescuerDao rescuerDao;

	public Map<String, Object> updateUser(UserDetails userDetails) {
		Registration Registration = new Registration();
		Map<String, Object> mapz = new HashMap<String, Object>();

	UpdateEmailValidationDao updateEmailValidationDao = new UpdateEmailValidationDao();
		int res = updateEmailValidationDao.validateEmail(userDetails.getEmail(),userDetails.getUserUid());
		log.debug("After checking Email/UserName");
		log.debug("Count: " + res);
		if (res == 0) {

			updateUserDao.updateUser(userDetails);
			if (userDetails.getRole().equals("Rescuer")) {
				rescuerDao.updateRescuer(userDetails);
				mapz.put("Success", "Success");
			} else{
				System.out.println("Not a Rescuer");
				mapz.put("Success", "Success");
			}
			if(userDetails.getRole().equals("Rescuer") || userDetails.getRole().equals("Admin")){
				updateUserDao.removeAssignmentsUser(userDetails);
			}
		} else {
			System.out.println("Email/UserName Already Exists");
			mapz.put("info", "Email/UserName Already Exists");
			return mapz;
			
		}
		log.debug("Account Successfully Updated"); 
		
		mapz.put("info", "Successfully updated");
		return mapz;
	}

}

