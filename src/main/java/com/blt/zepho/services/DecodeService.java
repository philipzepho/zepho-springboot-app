package com.blt.zepho.services;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blt.zepho.dao.ApprovalDao;
import com.blt.zepho.dao.SaveDeviceIdDao;
import com.blt.zepho.model.AlertsFromOthers;
import com.blt.zepho.model.ClearSosListItem;
import com.blt.zepho.model.DoNotGo;
import com.blt.zepho.model.EmailForward;
import com.blt.zepho.model.ItemId;
import com.blt.zepho.model.NewTrip;
import com.blt.zepho.model.Organization;
import com.blt.zepho.model.OtpInfo;
import com.blt.zepho.model.Registration;
import com.blt.zepho.model.RegistrationList;
import com.blt.zepho.model.Sos;
import com.blt.zepho.model.UidandRole;
import com.blt.zepho.model.UserData;
import com.blt.zepho.model.UserDetails;
import com.blt.zepho.util.AuthNumber;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

@Service
public class DecodeService {
	
	public Registration decodeRegistration(String encodeString) {		
		String decodeString = decodeStringData(encodeString);		//
		Gson g = new Gson(); 
		Registration reg = g.fromJson(decodeString, Registration.class);
		return reg;
	}
	
	public String decodeString(String encodeString) {		
		String decodeString = decodeStringData(encodeString);	//		
		return decodeString;
	}
	
	public OtpInfo decodeOtpInfo(String encodeString) {		
		String decodeString = decodeStringData(encodeString);		//
		Gson g = new Gson(); 
		OtpInfo otpInfo = g.fromJson(decodeString, OtpInfo.class);
		return otpInfo;
	}
	
	public int decodeInteger(String encodeString) {		
		String decodeString = decodeStringData(encodeString);	//	
		int intValue = Integer.parseInt(decodeString);
		return intValue;
	}	
	
	public Organization decodeOrganization(String encodeString) {		
		String decodeString = decodeStringData(encodeString);		//
		Gson g = new Gson(); 
		Organization organization = g.fromJson(decodeString, Organization.class);
		return organization;
	}	
	
	public Sos decodeSos(String encodeString) {		
		String decodeString = decodeStringData(encodeString);		//
		Gson g = new Gson(); 
		Sos sos = g.fromJson(decodeString, Sos.class);
		return sos;
	}
	
	public NewTrip decodeNewTrip(String encodeString) {		
		String decodeString = decodeStringData(encodeString);		//
		Gson g = new Gson(); 
		NewTrip newTrip = g.fromJson(decodeString, NewTrip.class);
		return newTrip;
	}

	public UserData decodeUserData(String encodeString) {		
		String decodeString = decodeStringData(encodeString);		//
		Gson g = new Gson(); 
		UserData userData = g.fromJson(decodeString, UserData.class);
		return userData;
	}	
	
	public UserDetails decodeUserDetails(String encodeString) {		
		String decodeString = decodeStringData(encodeString);		//
		Gson g = new Gson(); 
		UserDetails userDetails = g.fromJson(decodeString, UserDetails.class);
		return userDetails;
	}
	
	public RegistrationList decodeRegistrationList(String encodeString) {		
		String decodeString = decodeStringData(encodeString);		//
		Gson g = new Gson(); 
		RegistrationList registration = g.fromJson(decodeString, RegistrationList.class);
		return registration;
	}
	
	public ItemId decodeItemId(String encodeString) {		
		String decodeString = decodeStringData(encodeString);		//
		Gson g = new Gson(); 
		ItemId itemId = g.fromJson(decodeString, ItemId.class);
		return itemId;
	}
	
	public ClearSosListItem decodeClearSosListItem(String encodeString) {		
		String decodeString = decodeStringData(encodeString);		//
		Gson g = new Gson(); 
		 ClearSosListItem clearSosListItems = g.fromJson(decodeString, ClearSosListItem.class);
		return clearSosListItems;
	}
	
	public DoNotGo decodeDoNotGo(String encodeString) {		
		String decodeString = decodeStringData(encodeString);		//
		Gson g = new Gson(); 
		DoNotGo doNotGo= g.fromJson(decodeString, DoNotGo.class);
		return doNotGo;
	}
	
		
	public EmailForward decodeEmailForward(String encodeString) {		
		String decodeString = decodeStringData(encodeString);		//
		Gson g = new Gson(); 
		EmailForward emailForwards= g.fromJson(decodeString, EmailForward.class);
		return emailForwards;
	}
	
	public UidandRole decodeUidandRole(String encodeString) {		
		String decodeString = decodeStringData(encodeString);		//
		Gson g = new Gson(); 
		UidandRole uidandrole= g.fromJson(decodeString, UidandRole.class);
		return uidandrole;
	}
	
	public String encodeData(Map<String, Object> map) {
		String encodeData = "";
		ObjectMapper mapperObj = new ObjectMapper();
		try {
			String jsonString = mapperObj.writeValueAsString(map);
			encodeData = Base64.getEncoder().encodeToString(jsonString.getBytes("utf-8"));//Encode 1st time			
			encodeData = new StringBuilder(encodeData).reverse().toString(); // Reverse the string
			encodeData = Base64.getEncoder().encodeToString(encodeData.getBytes("utf-8")); //Encode 2nd time
			encodeData = generatingRandomString() + encodeData;
		} catch (JsonProcessingException | UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return encodeData;
	}
	
	public String generatingRandomString() {
		String generatedString = RandomStringUtils.randomAlphanumeric(7);
		return generatedString;
	}
	
	public String decodeStringData(String encode){
		encode = encode.substring(7);
		String firstDecodeString = new String(Base64.getDecoder().decode(encode)); //Decode first time
		firstDecodeString = new StringBuilder(firstDecodeString).reverse().toString(); // Reverse the string	
		String decode = new String(Base64.getDecoder().decode(firstDecodeString));  //Decode 2nd time
		return decode;
	}
	
	public int getRandomAuthNumber(int[] auth){
		int rnd = new Random().nextInt(auth.length);
		return auth[rnd];		
	}

	public AlertsFromOthers decodeAlertsFromOthers(String encodeString) {
		String decodeString = decodeStringData(encodeString);		//
		Gson g = new Gson(); 
		AlertsFromOthers afo= g.fromJson(decodeString, AlertsFromOthers.class);
		return afo;
	}
}