package com.blt.zepho.services;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blt.zepho.dao.SaveOrganizationDao;
import com.blt.zepho.model.Organization;

@Service
public class SaveOrganizationService {
	static Logger log = Logger.getLogger(SaveOrganizationService.class.getName());
	@Autowired
	SaveOrganizationDao saveOrganizationDao;

	public Map<String, Object> DoSaveOrganizationIdService(Organization organization) {
		log.debug("inside DoSaveDeviceIdService");

		Map<String, Object> map = null;

		map = new HashMap<String, Object>();
		boolean res = saveOrganizationDao.doSaveOrganization(organization);

		if (res == false) {
			map.put("Error", "Failure");

			return map;
		} else {
			map.put("Success", "Success");
			return map;
		}

	}
	
	public Map<String, Object> saveOrganistaionCode(Organization organization) {
		log.debug("inside saveOrganistaionCode");

		Map<String, Object> map = null;

		map = new HashMap<String, Object>();
		boolean res = saveOrganizationDao.saveOrganistaionCode(organization);

		if (res == false) {
			map.put("info", "Failed to add Organisation Code");

			return map;
		} else {
			map.put("Success", "Success");
			return map;
		}

	}
}
