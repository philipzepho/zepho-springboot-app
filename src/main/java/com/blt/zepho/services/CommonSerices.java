package com.blt.zepho.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;

import com.blt.zepho.jdbc.CreateJDBCConnection;
import com.blt.zepho.model.Registration;
import com.blt.zepho.model.UserLog;
import com.blt.zepho.util.WebKeys;

public class CommonSerices {
	static Logger log = Logger.getLogger(LoginService.class.getName());
	
	public boolean maintainLog(UserLog userLog) {
		 Connection connection = null;
		 try { 			
			  UserLog ulog = getPreviousHistory(userLog);
			  if(ulog != null) {
				  try {
					     connection = CreateJDBCConnection.getConnection();
						 String query = "update user_log SET status=? where log_id= ?" ;						
						 java.sql.PreparedStatement preparedStmt =  connection.prepareStatement(query);
						 preparedStmt.setString(1,WebKeys.STATUS_INACTIVE);
						 preparedStmt.setInt(2, ulog.getLogId());
						 preparedStmt.executeUpdate();
						 connection.close();
						 userLog.setPreviousVal(ulog.getUpdatedVal());
						 userLog.setPreviousDate(ulog.getChangedDate());
					} catch (SQLException e) {
						log.fatal(e);
						return false;
					} finally {
						if (connection != null) {
							try {
								connection.close();
							} catch (SQLException e) {
								log.fatal(e);
							}
						}
					}
			    }
			  
		      connection = CreateJDBCConnection.getConnection();
			  Date changedDate = new Date();
			  DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			  String cDate = dateFormat.format(changedDate);
			  String pDate = "";
			  if(userLog.getPreviousDate() != null) {
				  pDate = dateFormat.format(userLog.getPreviousDate());	
			  }else {
				  pDate = null;	
			  }
			
			  String query = "insert into user_log (user_id, previous_val, updated_val, module_name, "						 
						+ " status, comment, changed_by, previous_date, changed_date) "
						+ "values(?,?,?,?,?,?,?,?,?)" ;
			 
			 java.sql.PreparedStatement preparedStmt =  connection.prepareStatement(query);
			 preparedStmt.setInt(1, userLog.getUserId());
	         preparedStmt.setString(2, userLog.getPreviousVal());
	         preparedStmt.setString(3, userLog.getUpdatedVal());
	         preparedStmt.setString(4, userLog.getModuleName());
	         preparedStmt.setString(5, userLog.getStatus());
	         preparedStmt.setString(6, userLog.getComment());
	         preparedStmt.setString(7, userLog.getChangedBy());
	         preparedStmt.setString(8, pDate);
	         preparedStmt.setString(9, cDate);
	         
	         preparedStmt.execute();
	         connection.close();        

		 }catch(Exception e) {
			 e.printStackTrace();
			 return false;
		 }		
		return true;
	}
	
	public UserLog getPreviousHistory(UserLog userLog) {		
		UserLog ulog = null;
		ResultSet rs = null;
		Connection connection = null;
		PreparedStatement myStmt = null;		
		try {
			connection = CreateJDBCConnection.getConnection();
			myStmt = connection.prepareStatement("select * from user_log where user_id  ="+userLog.getUserId()+" and module_name='"+userLog.getModuleName()+"' and status='"+WebKeys.STATUS_ACTIVE+"'");
			rs = myStmt.executeQuery();
			while (rs.next()) {
				ulog = new UserLog();
				ulog.setLogId(rs.getInt("log_id"));
				ulog.setUserId(rs.getInt("user_id"));
				ulog.setPreviousVal(rs.getString("previous_val"));
				ulog.setUpdatedVal(rs.getString("updated_val"));
				ulog.setPreviousDate(rs.getTime("previous_date"));
				String changedDate = rs.getString("changed_date");
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				Date cDate = dateFormat.parse(changedDate);
				ulog.setChangedDate(cDate);
				ulog.setComment(rs.getString("comment"));
				ulog.setStatus(rs.getString("status"));		
				ulog.setChangedBy(rs.getString("changed_by"));
			}
		}catch(Exception e) {
			 e.printStackTrace();
			 return ulog;
		}
		return ulog;
	}
}
