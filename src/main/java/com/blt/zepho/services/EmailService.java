package com.blt.zepho.services;



import java.util.Properties;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.google.code.geocoder.Geocoder;
import com.google.code.geocoder.GeocoderRequestBuilder;
import com.google.code.geocoder.model.GeocodeResponse;
import com.google.code.geocoder.model.GeocoderAddressComponent;
import com.google.code.geocoder.model.GeocoderRequest;
import com.google.code.geocoder.model.GeocoderResult;
import com.google.code.geocoder.model.GeocoderStatus;
import com.google.code.geocoder.model.LatLng;
import com.blt.zepho.model.Rescuer;
import com.blt.zepho.model.Sos;
import com.blt.zepho.util.WebKeys;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.BufferedReader;
import java.io.InputStreamReader;

@Service
public class EmailService {
	static Logger log = Logger.getLogger(EmailService.class.getName());

	public void doEmail(ArrayList<Rescuer> rescuerList,Sos sos) {
		
		log.debug("BEFORE SENDING EMAIL");
		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", WebKeys.EMAIL_PORT);
		props.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", WebKeys.EMAIL_PORT);

		Session session = Session.getDefaultInstance(props,
			new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(WebKeys.EMAIL_ID, WebKeys.EMAIL_PASSWORD);
				}
			});

		try {			
			 String htmlText="";
			 String imageSrc = "";
			 String address = "";
			 String locationDetails = "";			 
			
			if(sos.isOnBehalf()==false){
				
				String zipcode = null;
			    try
			    {
			        /*LatLng latLng = new LatLng();
			        latLng.setLat(BigDecimal.valueOf(sos.getLatitude()));
			        latLng.setLng(BigDecimal.valueOf(sos.getLongitude()));
			        
			        
			        Map<String, String> map = new HashMap<String, String>();*/
			    	
			    	final Geocoder geocoder1 = new Geocoder();
			    	GeocoderRequest request = new GeocoderRequestBuilder()
			    			.setLocation(new LatLng(new BigDecimal(sos.getLatitude()), new BigDecimal(sos.getLongitude())))
			    			.setLanguage("en")
			    			.getGeocoderRequest();

			    	GeocodeResponse response = geocoder1.geocode(request);
			    	List<GeocoderResult> results = new ArrayList<GeocoderResult>();
			    	if (response.getStatus().equals(GeocoderStatus.OK)) {
			    		results = response.getResults();
			    		
			    	} else {
			    		log.info(response.getStatus().name());
			    	}			        
			        
			        /*final Geocoder geocoder = new Geocoder();
			        GeocoderRequest geocoderRequest = new GeocoderRequestBuilder().setLocation(latLng).getGeocoderRequest();
			        GeocodeResponse geocoderResponse = geocoder.geocode(geocoderRequest);
			        List<GeocoderResult> results = geocoderResponse.getResults();*/
			        
			        List<GeocoderAddressComponent> geList= new ArrayList<>();
			        
			        if(results.size() > 0){
			        	geList= results.get(0).getAddressComponents();
			        }
			        
			        for(int i=0; i<geList.size();i++){
			        	if(!geList.get(i).getTypes().get(0).trim().contentEquals("postal_code")){
			        		if(address == ""){
				        		address = geList.get(i).getLongName();
				        	}else{
				        		if(geList.get(i).getLongName() != "" && !address.contains(geList.get(i).getLongName())){
				        			address = address + ", "+ geList.get(i).getLongName();
				        		}				        		
				        	}
				        }
			        }
			        
			        if(results.size() > 1){
			        	geList= results.get(1).getAddressComponents();
			        }
			        
			        if(geList.size()>0 && geList.get(geList.size()-1).getTypes().get(0).trim().contentEquals("postal_code")){
			            zipcode = geList.get(geList.size()-1).getLongName();
			        }
			        else if(geList.size()>0 && geList.get(0).getTypes().get(0).trim().equalsIgnoreCase("postal_code"))
			        {
			            zipcode = geList.get(0).getLongName();
			        }
			        if(zipcode != null){
			        	address = address + ", "+ zipcode;
			        }
			    }catch (Exception e) {
			        e.printStackTrace();
			    }

				
				
					
			    imageSrc = signRequest("/maps/api/staticmap",
						"zoom=15&size=600x300&markers=color:red%7Clabel:C%7C"+sos.getLatitude()+","+sos.getLongitude()+"&key="+WebKeys.API_KEY, 
						WebKeys.SECRET_SIGNING);
				
				
				if(address == null || address == ""){
					if(sos.getSubLocality()!=null && !sos.getSubLocality().equals("")){
						locationDetails = sos.getSubLocality();
					}
					if(sos.getSubThoroughfare()!=null && !sos.getSubThoroughfare().equals("")){
						if(locationDetails != ""){
							locationDetails = locationDetails + ", " +sos.getSubThoroughfare();
						}else{
							locationDetails = sos.getSubThoroughfare();
						}					
					}
					if(sos.getThoroughfare()!=null && !sos.getThoroughfare().equals("")){
						if(locationDetails != ""){
							locationDetails = locationDetails + ",  " +sos.getThoroughfare();
						}else{
							locationDetails = sos.getThoroughfare();
						}
					}
					if(sos.getLocality()!=null && !sos.getLocality().equals("")){
						if(locationDetails != ""){
							locationDetails = locationDetails + ", " +sos.getLocality();
						}else{
							locationDetails = sos.getLocality();
						}
					}
					if(sos.getAdministrativeArea()!=null && !sos.getAdministrativeArea().equals("")){
						if(locationDetails != ""){
							locationDetails = locationDetails + ", " +sos.getAdministrativeArea();
						}else{
							locationDetails = sos.getAdministrativeArea();
						}
					}
					if(sos.getPostalCode() !=null && !sos.getPostalCode().equals("")){
						if(locationDetails != ""){
							locationDetails = locationDetails + ", " +sos.getPostalCode();
						}else{
							locationDetails = sos.getPostalCode();
						}
					}
				}
			}
				
				
				
		        // Image part
				//"https://maps.googleapis.com/maps/api/staticmap?zoom=15&size=600x300&markers=color:red%7Clabel:C%7C"+sos.getLatitude()+","+sos.getLongitude()+"&key="+WebKeys.API_KEY+"&signature="+sign;
		        /*try {	        	 
		        	URLConnection openConnection = new URL(imageURL).openConnection();
		     		openConnection.addRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0");
		     		InputStream imageStream  = openConnection.getInputStream();	   
		            DataSource fds = new ByteArrayDataSource(IOUtils.toByteArray(imageStream), "image/png");
		            messageBodyImagePart.setDataHandler(new DataHandler(fds));
		            messageBodyImagePart.setHeader("Content-ID","<image>");
		        }catch (Exception e) {
		            // TODO Auto-generated catch block
		            e.printStackTrace();
		        }*/		        
		        
				
			
			
			
			for(Rescuer rs: rescuerList){
				String rescuerName = rs.getFullName();
				String email = rs.getEmail();
				log.debug("EMAIL:: "+email);
				if(email==null){
					return;
				}
				MimeMultipart multipart = new MimeMultipart("related");
			    BodyPart messageBodyPart = new MimeBodyPart();
				Message message = new MimeMessage(session);
				message.setFrom(new InternetAddress(WebKeys.EMAIL_ID));
				message.setRecipients(Message.RecipientType.TO,
						InternetAddress.parse(email));
				 
				if(sos.isOnBehalf()==false){
				
					if(sos.getType().equals("Missionary") || sos.getType().equals("Rescuer")|| sos.getType().equals("SuperAdmin")|| sos.getType().equals("Admin") || sos.getType().equalsIgnoreCase("ChurchMember")){
						String subject = "Self in Danger";
						if(sos.getCallOrSos() != null && sos.getCallOrSos() != "" && sos.getCallOrSos().equalsIgnoreCase("call")){
							subject = "Calling to Persecution relief that he/she in Danger";
						}			
						message.setSubject(""+sos.getFromUser()+ " - Reported: "+subject);					
						htmlText = "<H4> Dear "+rescuerName+", </H4>"+"<br/> <p>"+sos.getFromUser()+ " has indicated that <b>Self in Danger</b>, Please find the indicative location details below; Request you to kindly take necessary action immediately. </p><br/>";
						
					}else if(sos.getType().equals("Home")){
						message.setSubject(""+sos.getFromUser()+ " - Reported: Home being attacked");
						htmlText = "<H4> Dear "+rescuerName+", </H4>"+"<br/> <p>"+sos.getFromUser()+ " has indicated that <b>Home being Attacked</b>, Please find the indicative location details below; Request you to kindly take necessary action immediately. </p><br/> ";
					}else if(sos.getType().equals("Church")){
						message.setSubject(""+sos.getFromUser()+ " - Reported: Church Worship being disturbed");
						htmlText = "<H4> Dear "+rescuerName+", </H4>"+"<br/> <p>"+sos.getFromUser()+ " has indicated that  <b>Church Worship being disturbed</b>, Please find the indicative location details below; Request you to kindly take necessary action immediately. </p><br/>";
					}
					
					
					if(address != null && address != ""){
						htmlText+= "<H4> Location Details : </H4><p><b> "+address+"</b></p>" + "<br/><img src='"+imageSrc+"' /> <br/>";
					}else if(locationDetails !=null && !locationDetails.equals("")){
			        	htmlText+= "<H4> Location Details : </H4><p><b> "+locationDetails+"</b></p>" + "<br/><img src='"+imageSrc+"' /> <br/>";		        	
			        }else{
			        	htmlText+= "<H2> Location Details : </H2>"+ "<img src='"+imageSrc+"' /><br/>";
			        }
					
					htmlText+="<H4>Zepho Alerts Team</H4><p style='color:red;'><b>PS: This is auto generated mail, there will be no response if you respond back to this ID.</b></p>";
		        
				}else{
					
					if(sos.getObUserUid()==null || sos.getObUserUid()==""){
						message.setSubject(""+sos.getFromUser()+ " - Reported:  "+sos.getObUserName()+" in trouble");
						
						htmlText = "<H4> Dear "+rescuerName+", </H4>"+"<br/> <p>"+sos.getFromUser()+ " has indicated that <b>"+sos.getObUserName()+ " </b> is in trouble, Please find the indicative location details below; Request you to kindly take necessary action immediately. </p><br/>";					
						htmlText+= "<H2> Location Details : </H2><p><b> "+sos.getLocationName()+"</b></p><br/>";
			        	htmlText+="<H4>Zepho Alerts Team</H4><p style='color:red;'><b>PS: This is auto generated mail, there will be no response if you respond back to this ID.</b></p>";

			
					}else{
						
						message.setSubject(""+sos.getObUserName()+ " - Reported:  "+sos.getFromUser()+" in trouble");
						
						
						htmlText = "<H4> Dear "+rescuerName+", </H4>"+"<br/> <p>"+sos.getObUserName()+ " has indicated that <b>"+sos.getFromUser()+ " </b> is in trouble, Please find the indicative location details below; Request you to kindly take necessary action immediately. </p><br/>";					
						htmlText+= "<H2> Location Details : </H2><p><b> "+sos.getLocationName()+"</b></p><br/>";
			        	htmlText+="<H4>Zepho Alerts Team</H4><p style='color:red;><b>PS: This is auto generated mail, there will be no response if you respond back to this ID.</b></p>";

					}
				}
				messageBodyPart.setContent(htmlText, "text/html");

		        // add it
		        multipart.addBodyPart(messageBodyPart);    
		        
		        // put everything together
		        message.setContent(multipart);			
				
				System.out.println(htmlText);
				
				Transport.send(message);
			}

			System.out.println("EMAILS SENT");

		} catch (MessagingException e) {
			log.fatal(e.getMessage());
			throw new RuntimeException(e);
		} catch (InvalidKeyException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (NoSuchAlgorithmException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (URISyntaxException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	

	public void emailAlert(String email, String mailTxt, String subject) {
		
		log.debug("BEFORE SENDING EMAIL");
		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", WebKeys.EMAIL_PORT);
		props.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", WebKeys.EMAIL_PORT);

		Session session = Session.getDefaultInstance(props,
		new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(WebKeys.EMAIL_ID, WebKeys.EMAIL_PASSWORD);
			}
		});

		try {

			log.debug("EMAIL:: "+email);
			if(email==null){
				return;
			}
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(WebKeys.EMAIL_ID));
			message.setRecipients(Message.RecipientType.TO,	InternetAddress.parse(email));		
			message.setSubject(subject);
		  
			MimeMultipart multipart = new MimeMultipart("related");
	        BodyPart messageBodyPart = new MimeBodyPart();
	        //System.out.println(mailTxt);
	        messageBodyPart.setContent(mailTxt, "text/html");
	        multipart.addBodyPart(messageBodyPart);		        
	        message.setContent(multipart);				
			Transport.send(message);
			System.out.println("EMAIL SENT");

		} catch (MessagingException e) {
			log.fatal(e.getMessage());
			throw new RuntimeException(e);
		}		
	}
	
	public String signRequest(String path, String query, String keyString) throws NoSuchAlgorithmException,
    InvalidKeyException, UnsupportedEncodingException, URISyntaxException {
    
		keyString = keyString.replace('-', '+');
	    keyString = keyString.replace('_', '/');
	    // Base64 is JDK 1.8 only - older versions may need to use Apache Commons or similar.
	    byte[] key = Base64.getDecoder().decode(keyString);
		
	    // Retrieve the proper URL components to sign
	    String resource = path + '?' + query;
	    
	    // Get an HMAC-SHA1 signing key from the raw key bytes
	    SecretKeySpec sha1Key = new SecretKeySpec(key, "HmacSHA1");
	
	    // Get an HMAC-SHA1 Mac instance and initialize it with the HMAC-SHA1 key
	    Mac mac = Mac.getInstance("HmacSHA1");
	    mac.init(sha1Key);
	
	    // compute the binary signature for the request
	    byte[] sigBytes = mac.doFinal(resource.getBytes());
	
	    // base 64 encode the binary signature
	    // Base64 is JDK 1.8 only - older versions may need to use Apache Commons or similar.
	    String signature = Base64.getEncoder().encodeToString(sigBytes);
	    
	    // convert the signature to 'web safe' base 64
	    signature = signature.replace('+', '-');
	    signature = signature.replace('/', '_');
	    
	    return "https://maps.googleapis.com"+resource;// + "&signature=" + signature;
  }
}
