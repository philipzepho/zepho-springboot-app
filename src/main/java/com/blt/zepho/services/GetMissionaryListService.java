package com.blt.zepho.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blt.zepho.dao.MissionaryNameUidDao;
import com.blt.zepho.model.Missionary;
import com.blt.zepho.model.Rescuer;
import com.blt.zepho.model.UserData;

@Service
public class GetMissionaryListService {
	static Logger log = Logger.getLogger(GetMissionaryListService.class.getName());
    
	@Autowired
	MissionaryNameUidDao missionaryNameUidDemo;
	
	public Map<String, Object> doGetMissionaryListService(UserData userData) {
		log.debug("inside GET GetMissionaryList");

		Map<String, Object> map = null;

		map = new HashMap<String, Object>();

		ArrayList<Missionary> fullNameUserIDList = missionaryNameUidDemo.getUserUidFulName(userData.getUserUid());

		if (fullNameUserIDList.isEmpty()) {
			map.put("Error", "No Missionary");

			return map;
		} else {
			map.put("MissionaryList", fullNameUserIDList);
			return map;
		}

	}
	
	public Map<String, Object> getRescuerMappedMisListService(UserData userInfo) {
		log.debug("inside GET RESCUERS");

		Map<String, Object> map = null;

		map = new HashMap<String, Object>();

		ArrayList<Missionary> fullNameUserIDList = missionaryNameUidDemo.getUserUidFulNameMappedToRescuer(userInfo.getUserUid());

		if (fullNameUserIDList.isEmpty()) {
			map.put("Error", "No Missionary");

			return map;
		} else {
			map.put("MissionaryList", fullNameUserIDList);
			return map;
		}

	}

}
