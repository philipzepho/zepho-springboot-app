package com.blt.zepho.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.blt.zepho.dao.DoNotGoDao;
import com.blt.zepho.model.AlertsFromOthers;
import com.blt.zepho.model.DoNotGo;
import com.blt.zepho.model.Missionary;
import com.blt.zepho.model.OtpInfo;
import com.blt.zepho.util.WebKeys;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class SendOtpService {
	
	@Autowired	
	DoNotGoDao doNotGoDao;
	
	@Autowired
	AndroidPushNotificationsService notificationsService;
  
  private static final String MSG91_URL = "https://control.msg91.com/api/";
  private static final String SEND_PATH = "sendotp.php";
  private static final String MSG91_SMS_URL = "https://api.msg91.com/api/";
  private static final String SEND_SMS = "v2/sendsms";
  private static final String RETRY_PATH = "retryotp.php";
  private static final String VERIFY_PATH = "verifyRequestOTP.php";
  private static final String SENDER_ID = "ZEPALT";
  private static final String OTP_EXPIRY = "90";
  
  private String authKey;
  private String messageTemplate;
    
  /**
   * 
   * Constructor with custom message template.
   *
   * @param authKey
   * @param messageTemplate 
   */
  public SendOtpService(String messageTemplate) {
    this.authKey = WebKeys.MSG91_AUTH_KEY;
    if(messageTemplate == null || messageTemplate.isEmpty()) {
      this.messageTemplate = "Your otp is {{otp}}. Please do not share it with anybody.";
    } else {
      this.messageTemplate = messageTemplate;
    }
  }
  
  /**
   * 
   * Constructor with default message template.
   * 
   * @param authKey 
   */
  public SendOtpService() {
    this.authKey = WebKeys.MSG91_AUTH_KEY;
    this.messageTemplate = "Your otp is {{otp}}. Please do not share it with anybody";
  }
  
  public String getAuthKey() {
    return this.authKey;
  }
  
  public String getMessageTemplate() {
    return this.messageTemplate;
  }
  
  public void sendOTP(String phNumber) {
	  
      //Send SMS API
      String mainUrl=MSG91_URL+SEND_PATH+"?";//"http://api.msg91.com/api/sendhttp.php?";
      
      //String otp = generateOtp(4);

      //Prepare parameter string
      StringBuilder sbPostData= new StringBuilder(mainUrl);
      sbPostData.append("authkey="+authKey);
      sbPostData.append("&mobile=91"+phNumber);
      sbPostData.append("&sender="+SENDER_ID);
      sbPostData.append("&otp_expiry="+OTP_EXPIRY);      
      //sbPostData.append("&message="+this.messageTemplate);
      //sbPostData.append("&otp="+otp);

      //final string
      mainUrl = sbPostData.toString();
      postData(mainUrl);
  }
  
  public String generateOtp(int len) {
	  if(len <= 0) {
		  len = 6;
	  }
	  String chars = "0123456789";
	  String randomString = "";
	  int length = chars.length();
	  for (int i = 0; i < len; i++) {
		  randomString += chars.split("")[(int) (Math.random() * (length - 1))];
	  }
	  return randomString;
  }
  
  public Map<String, Object> verifyOTP(OtpInfo otpInfo) {      

      //Send SMS API
      String mainUrl=MSG91_URL+VERIFY_PATH+"?";//"http://api.msg91.com/api/sendhttp.php?";
      
      //Prepare parameter string
      StringBuilder sbPostData= new StringBuilder(mainUrl);
      sbPostData.append("authkey="+authKey);
      sbPostData.append("&mobile="+otpInfo.getMobileNumber());
      sbPostData.append("&otp="+otpInfo.getOtp());

      //final string
      mainUrl = sbPostData.toString();
      String responseData = postData(mainUrl);
      Map<String, Object> map = new HashMap<String, Object>();
      ObjectMapper mapper = new ObjectMapper();
      // convert JSON string to Map
      try {
    	  if(responseData != null) {
    		  map = mapper.readValue(responseData, new TypeReference<Map<String, String>>(){});
    	  }
      } catch (JsonParseException e) {
		// TODO Auto-generated catch block
    	  e.printStackTrace();
      } catch (JsonMappingException e) {
		// TODO Auto-generated catch block
    	  e.printStackTrace();
      } catch (IOException e) {
		// TODO Auto-generated catch block
    	  e.printStackTrace();
      }
      return map;
  }
  
  public Map<String, Object>  retryOTP(OtpInfo otpInfo) {      

	  String retryType = "voice";
	    if(!otpInfo.isVoice()) {
	      retryType = "text";
	    }
	  
      //Send SMS API
      String mainUrl=MSG91_URL+ RETRY_PATH +"?";//"http://api.msg91.com/api/sendhttp.php?";
      
      //Prepare parameter string
      StringBuilder sbPostData= new StringBuilder(mainUrl);
      sbPostData.append("authkey="+authKey);
      sbPostData.append("&mobile="+otpInfo.getMobileNumber());
      sbPostData.append("&retrytype="+retryType);

      //final string
      mainUrl = sbPostData.toString();
      String responseData = postData(mainUrl);
      Map<String, Object> map = new HashMap<String, Object>();
      ObjectMapper mapper = new ObjectMapper();
      // convert JSON string to Map
      try {
    	  if(responseData != null) {
    		  map = mapper.readValue(responseData, new TypeReference<Map<String, String>>(){});
    	  }
      } catch (JsonParseException e) {
		// TODO Auto-generated catch block
    	  e.printStackTrace();
      } catch (JsonMappingException e) {
		// TODO Auto-generated catch block
    	  e.printStackTrace();
      } catch (IOException e) {
		// TODO Auto-generated catch block
    	  e.printStackTrace();
      }
      return map;
      
  }
  
  private String postData(String mainUrl) {
	//Prepare Url
      URLConnection myURLConnection=null;
      URL myURL=null;
      BufferedReader reader=null;
      String responseData = "";
	  try
      {
          //prepare connection
          myURL = new URL(mainUrl);
          myURLConnection = myURL.openConnection();
          myURLConnection.connect();
          reader= new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));
          String response = "";
          //reading response          
          while ((response = reader.readLine()) != null) {
          //print response
        	  responseData = response;
        	  System.out.println(response);
          }

          //finally close connection
          reader.close();
      }
      catch (IOException e)
      {
              e.printStackTrace();
      }
	  return responseData;
  }
  
  public void sendSMSAlert(String mobiles, String message) {
	  System.out.println("numbers: "+mobiles);
      //Send SMS API
      String mainUrl=MSG91_SMS_URL+SEND_SMS+"?";

      //Prepare parameter string
      StringBuilder sbPostData= new StringBuilder(mainUrl);
      sbPostData.append("authkey="+authKey);
      sbPostData.append("&mobiles="+mobiles);
      sbPostData.append("&sender="+SENDER_ID);
      sbPostData.append("&message="+message);
      sbPostData.append("&route=4");
      sbPostData.append("&country=91");

      //final string
      mainUrl = sbPostData.toString();
      postData(mainUrl);
  }
  
  @Async
  public void sendAlerts(ArrayList<Missionary> misList, DoNotGo doNotGo){
		try{
      	String mobileList = "";
      	List<AlertsFromOthers> alertList = new ArrayList<>();
          for(int i=0;i<misList.size();i++){
  			Missionary mis=misList.get(i);
  			notificationsService.sendNotification(mis.getDeviceId(),"From "+ doNotGo.getFromUserName(),doNotGo.getMessage(), WebKeys.NOTIFICATION);
  			AlertsFromOthers afo = new AlertsFromOthers(UUID.randomUUID().toString(),
  					doNotGo.getFromUserName(), 
  					doNotGo.getFromUserUid(), 
  					mis.getUserUid(),
  					false, 
  					doNotGo.getCreatedAt(),
  					doNotGo.getMessage(),
  					doNotGo.getFromUserRole());
  			if(doNotGo.getFileName() != null && doNotGo.getFileName() != ""){
  				afo.setAttachmentPath(doNotGo.getFileName());
  			}	
  			alertList.add(afo);
  			//doNotGoDao.addAlertFromOther(afo);
  			sendEmail(misList.get(i).getEmail(),doNotGo.getMessage(), doNotGo);
  			if(misList.get(i).getMobile() != null && misList.get(i).getMobile() != ""){
  				if(mobileList == "" || mobileList == null){
  					mobileList = misList.get(i).getMobile();
  				}else{
  					mobileList += ","+misList.get(i).getMobile();
  				}
  			}		        			
  		}
          doNotGoDao.addAlertFromOther(alertList);
          if(mobileList != null && mobileList != ""){
  			this.sendSMSAlert(mobileList, "You received notification alert from "+doNotGo.getFromUserName()+". Please check Zepho app for more details");
  		}
          System.out.println("End of Thread...");
      }catch(Exception e){
      	e.printStackTrace();
      }
	}
  
  public void sendEmail(String emailId,String msg, DoNotGo doNotGo) {
		
		
		Properties props = new Properties();		
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", WebKeys.EMAIL_PORT);
		props.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", WebKeys.EMAIL_PORT);
		Session session = Session.getDefaultInstance(props,
			new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(WebKeys.EMAIL_ID, WebKeys.EMAIL_PASSWORD);  
				}
			});

		try {

			System.out.println("EMAIL:: "+emailId);
			if(emailId==null){
				return;
			}
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(WebKeys.EMAIL_ID));
			message.setRecipients(Message.RecipientType.TO,
			InternetAddress.parse(emailId));
			message.setSubject("Reg: Safety Alert from Zepho Send By "+doNotGo.getFromUserName());
		    message.setText(msg);
			Transport.send(message);

			System.out.println("EMAIL SENT");

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}
}