package com.blt.zepho.services;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blt.zepho.dao.ApprovalDao;
import com.blt.zepho.dao.SaveDeviceIdDao;
import com.blt.zepho.model.Registration;
import com.blt.zepho.util.AuthNumber;
import com.blt.zepho.util.WebKeys;

@Service
public class SaveDeviceIdService {

	static Logger log = Logger.getLogger(SaveDeviceIdService.class.getName());
	@Autowired
	SaveDeviceIdDao saveDeviceIdDao;
	@Autowired
	ApprovalDao approvalDao;
	@Autowired
	DecodeService decodeService;

	public Map<String, Object> DoSaveDeviceIdService(Registration reg) {
		log.debug("inside DoSaveDeviceIdService");

		Map<String, Object> map = null;

		map = new HashMap<String, Object>();
		Registration oldDeviceReg = saveDeviceIdDao.getUserDeviceIdByUID(reg.getUserUid());
		boolean regRes = saveDeviceIdDao.saveDeviceIDRegTable(reg);
		//boolean rescuerRes = saveDeviceIdDao.saveDeviceIDRescuerTable(reg);

		if (regRes == true) {
			if(oldDeviceReg.getDeviceId() != null &&!oldDeviceReg.getDeviceId().equalsIgnoreCase(reg.getDeviceId()) 
					&& oldDeviceReg.getIsApproved()==true && !oldDeviceReg.getDeviceId().equalsIgnoreCase("undefined")
					&& !reg.getDeviceId().equalsIgnoreCase("undefined")){
				System.out.println(reg.getDeviceId()+" : new device id");
				System.out.println(oldDeviceReg.getDeviceId()+" : new device id");
				EmailService emailService = new EmailService();
				Registration saReg = approvalDao.getSuperAdminByOrg(oldDeviceReg.getOrganization());
				String mailTxt =  "<p> Dear <b>"+oldDeviceReg.getFullName()+"</b>, </p>"+"<br/> <p>";
				mailTxt += "<p> We noticed that you have logged from new device</b>. We hope, that's you. If not, Please contact your Admin(Email: "+saReg.getEmail()+") immediately.</p><br/><br/>";
				mailTxt += "<p>---<p></br>";
				mailTxt += "<p><b>Zepho-Team</b><p>";
				mailTxt += "</br>"+WebKeys.SUPPORT_EMAIL;
				String subject= "Zepho Security Alert: Logged in from new device";
				emailService.emailAlert(oldDeviceReg.getEmail(), mailTxt, subject);
			}
			String role = oldDeviceReg.getRole();
			boolean isApproved = oldDeviceReg.getIsApproved();
			map.put("isApproved", isApproved);
			map.put("isRejected", oldDeviceReg.getIsRejected());
			map.put("role",role);			
			if(isApproved){
				if(role.equals("Admin")){				
					map.put("rand", decodeService.getRandomAuthNumber(AuthNumber.ADMIN));
				}else if(role.equals("SuperAdmin")){
					map.put("rand", decodeService.getRandomAuthNumber(AuthNumber.SUPER_ADMIN));
				}else if(role.equals("Rescuer")){
					map.put("rand", decodeService.getRandomAuthNumber(AuthNumber.RESCUER));
				}else if(role.equals("Missionary")){
					map.put("rand", decodeService.getRandomAuthNumber(AuthNumber.MISSIONARY));
				}else if(role.equals("MasterAdmin")){
					map.put("rand", decodeService.getRandomAuthNumber(AuthNumber.MASTER_ADMIN));
				}else if(role.equals("Public")||role.equals("ChurchMember")){
					map.put("rand", decodeService.getRandomAuthNumber(AuthNumber.CHURCH_MEMBER));
				}else if(role.equals("Registered")){
					map.put("rand", "User");
				}
			}			
			map.put("Success", true);
			return map;
		} else {
			map.put("Success", false);
			return map;
		}

		// return new ArrayList<Registration>();

	}

	public Map<String, Object> removeDeviceId(Registration reg) {
		Map<String, Object> map = new HashMap<String, Object>();
		try{
			boolean regRes = saveDeviceIdDao.removeDeviceId(reg.getUserUid());
			map.put("Success", regRes);
		}catch(Exception e){
			map.put("Success", false);
		}		
		return map;
	}
}
