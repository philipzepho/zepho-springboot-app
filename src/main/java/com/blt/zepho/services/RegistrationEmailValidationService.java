package com.blt.zepho.services;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blt.zepho.dao.RegistrationEmailValidationDao;
import com.blt.zepho.model.Registration;

@Service
public class RegistrationEmailValidationService {
	

	static Logger log = Logger.getLogger(RegistrationService.class.getName());

	@Autowired
	RegistrationEmailValidationDao registrationEmailValidationDao;
	Registration reg;

	public int registerUser(Registration reg) {

		log.debug("email " + reg.getEmail());
		// reg.setEmail(email);
		// reg.setPassword(password);

		log.debug("Login from login service");
		return registrationEmailValidationDao.registerUser(reg);
	}
}
