package com.blt.zepho.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.Executor;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import com.blt.zepho.dao.DoNotGoDao;
import com.blt.zepho.model.AlertsFromOthers;
import com.blt.zepho.model.DoNotGo;
import com.blt.zepho.model.Missionary;
import com.blt.zepho.model.OtpInfo;
import com.blt.zepho.util.WebKeys;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/*@Configuration
@EnableAsync*/
@Service
public class AsyncService {
	
	@Autowired	
	DoNotGoDao doNotGoDao;
	
	@Autowired
	AndroidPushNotificationsService notificationsService;
   
	@Autowired
	SendOtpService sendOtpService;
  
	public AsyncService() {   
		  
	}
  
	/*@Bean(name = "threadPoolTaskExecutor")
	public Executor threadPoolTaskExecutor() {
		return new ThreadPoolTaskExecutor();
	}*/
    
	// @Async("threadPoolTaskExecutor")
	public void sendAlerts(ArrayList<Missionary> misList, DoNotGo doNotGo){
		try{
			String mobileList = "";
      	  	List<AlertsFromOthers> alertList = new ArrayList<>();
      	  	for(int i=0;i<misList.size();i++){
      	  		Missionary mis=misList.get(i);      	  		
      	  		AlertsFromOthers afo = new AlertsFromOthers(UUID.randomUUID().toString(),
  					doNotGo.getFromUserName(), 
  					doNotGo.getFromUserUid(), 
  					mis.getUserUid(),
  					false, 
  					doNotGo.getCreatedAt(),
  					doNotGo.getMessage(),
  					doNotGo.getFromUserRole());
	  			if(doNotGo.getFileName() != null && doNotGo.getFileName() != ""){
	  				afo.setAttachmentPath(doNotGo.getFileName());
	  			}	
	  			alertList.add(afo);	  			
	  			if(misList.get(i).getMobile() != null && misList.get(i).getMobile() != ""){
	  				if(mobileList == "" || mobileList == null){
	  					mobileList = misList.get(i).getMobile();
	  				}else{
	  					mobileList += ","+misList.get(i).getMobile();
	  				}
	  			}		        			
      	  	}
      	  	if(alertList.size() > 0){
      	  		doNotGoDao.addAlertFromOther(alertList);
      	  	}
      	  	
      	  	if(mobileList != null && mobileList != ""){
    	  		sendOtpService.sendSMSAlert(mobileList, "You received notification alert from "+doNotGo.getFromUserName()+". Please check Zepho app for more details");
    	  	}
      	  	
      	  	for(int i=0;i<misList.size();i++){
    	  		Missionary mis=misList.get(i);
    	  		notificationsService.sendNotification(mis.getDeviceId(),"From "+ doNotGo.getFromUserName(),doNotGo.getMessage(), WebKeys.NOTIFICATION);
	  			sendEmail(misList.get(i).getEmail(),doNotGo.getMessage(), doNotGo);	  			      			
    	  	}
      	  	
      	  	System.out.println("End of Thread...");
		}catch(Exception e){
			e.printStackTrace();
	    }
	}
  
	public void sendEmail(String emailId,String msg, DoNotGo doNotGo) {		
		Properties props = new Properties();		
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", WebKeys.EMAIL_PORT);
		props.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", WebKeys.EMAIL_PORT);
		Session session = Session.getDefaultInstance(props,
			new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(WebKeys.EMAIL_ID, WebKeys.EMAIL_PASSWORD);  
				}
			});

		try {

			System.out.println("EMAIL:: "+emailId);
			if(emailId==null){
				return;
			}
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(WebKeys.EMAIL_ID));
			message.setRecipients(Message.RecipientType.TO,
			InternetAddress.parse(emailId));
			message.setSubject("Reg: Safety Alert from Zepho Send By "+doNotGo.getFromUserName());
		    message.setText(msg);
			Transport.send(message);

			System.out.println("EMAIL SENT");

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}
}