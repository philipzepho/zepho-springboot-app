package com.blt.zepho.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blt.zepho.dao.MasterAdminDao;
import com.blt.zepho.model.Organization;
import com.blt.zepho.model.Registration;

@Service
public class MasterAdminService {
	static Logger log = Logger.getLogger(MasterAdminService.class.getName());
    
	@Autowired
	MasterAdminDao masterAdmintDao;
	
	public Map<String, Object> getSuperAdminList(String organization) {
		log.debug("inside getSuperAdminList");

		Map<String, Object> map = null;

		map = new HashMap<String, Object>();

		ArrayList<Registration> superAdminList=	masterAdmintDao.getSuperAdminList(organization);

		if (superAdminList==null) {
			map.put("success", false);
			map.put("message", "superAdminList empty");

			return map;
		} else {
			map.put("success", true);
			map.put("superAdminList", superAdminList);
			return map;
		}
	}
	
	
	public Map<String, Object> getActivityList(String superAdmin) {
		log.debug("inside getActivityList");

		Map<String, Object> map = null;

		map = new HashMap<String, Object>();

		ArrayList<Registration> activityList=	masterAdmintDao.getActivityList(superAdmin);

		if (activityList==null) {
			map.put("success", false);
			map.put("message", "activityList empty");

			return map;
		} else {
			map.put("success", true);
			map.put("activityList", activityList);
			return map;
		}
	}
}
