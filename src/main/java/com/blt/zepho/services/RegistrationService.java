package com.blt.zepho.services;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blt.zepho.dao.ApprovalDao;
import com.blt.zepho.dao.RegistrationDao;
import com.blt.zepho.dao.RegistrationEmailValidationDao;
import com.blt.zepho.dao.RescuerDao;
import com.blt.zepho.model.Registration;
import com.blt.zepho.util.WebKeys;


@Service
public class RegistrationService {

	static Logger log = Logger.getLogger(RegistrationService.class.getName());
    
	@Autowired
	RegistrationDao registrationDao;
	Registration reg = new Registration();
	@Autowired
	RescuerDao rescuerDao;
	AssignAdminValidator assignAdminvalidator = new AssignAdminValidator();
	@Autowired
	ApprovalDao approvalDao;
	ApprovalService approvalService = new ApprovalService();
	SosService sosService = new SosService();
	@Autowired
	RegistrationEmailValidationDao regValidationDao;
	@Autowired
	SendOtpService sendOtpService;
	@Autowired
	AndroidPushNotificationsService notificationsService;
	

	public Map<String, Object> registerUserz(Registration reg) {
		Registration Registration = new Registration();
		Map<String, Object> mapz = new HashMap<String, Object>();
		List<Registration> rescuerList = new ArrayList<>();		
		
		String res = regValidationDao.checkPublicReg(reg);
		log.debug("After checking PhoneNumer");
		Registration userUidReg = new Registration();
		
		if (res == "") {	
			// assign validator
			if(reg.getRole() != null && reg.getRole().equalsIgnoreCase("SuperAdmin") && reg.getRole().equalsIgnoreCase("Admin")){
				userUidReg = assignAdminvalidator.setValidator(reg);
				if(userUidReg.getUserUid() != "" && userUidReg.getUserUid() != null){
					reg.setApproveByAdminUid(userUidReg.getUserUid());
				}else{
					mapz.put("info", "Admin not available to validate, please add Admin first.");
					return mapz;
				}
			}
			
			// assign rescuer
			rescuerList = assignAdminvalidator.getRescuers(reg);
			
			if(rescuerList.size() > 0){
				int count = 0;
				for(Registration rescuer: rescuerList){
					if(count == 0)
						reg.setRescuer1(rescuer.getUserUid());
					
					if(count == 1)
						reg.setRescuer2(rescuer.getUserUid());
					
					if(count == 2)
						reg.setRescuer3(rescuer.getUserUid());
					
					if(count == 3)
						reg.setRescuer4(rescuer.getUserUid());
					
					if(count == 4)
						break;
					
					count++;
				}
			}
			String dobString=reg.getDobString();  
			try {
				if(dobString != null && dobString != ""){
					Date dob = new SimpleDateFormat("dd/MM/yyyy").parse(dobString);
					reg.setDob(dob);
				}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}  
			
			registrationDao.registerUser(reg);
			if (reg.getRole().equals("Rescuer")) {
				approvalDao.replaceFarRescuers(reg);
				rescuerDao.addRescuer(reg);
				mapz.put("Success", "Success");
			} else{
				System.out.println("Not a Rescuer");
				mapz.put("Success", "Success");
			}
			if(reg.isApprovedByAdmin()){
				Registration saReg = approvalDao.getSuperAdminByOrg(reg.getOrganization());					
				approvalService.sendApproveEmail(saReg, reg);
				String title= "New member registered";
				String body= reg.getFullName() + " is registered as "+reg.getRole()+", please verify and approve.";
				if(saReg.getDeviceId() != null && saReg.getDeviceId() != ""){
					try {
						notificationsService.sendNotification(saReg.getDeviceId(), title, body, WebKeys.APPROVAL);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}	
				if(saReg.getMobileNum() != null && saReg.getMobileNum() != ""){
					sendOtpService.sendSMSAlert(saReg.getMobileNum(), body);
				}
			}			
		} else {
			System.out.println(res+" Already Exists");
			mapz.put("info", res+" Already Exists");
			return mapz;
			
		}
		log.debug("New Account Successfully Added"); 
		
		mapz.put("info", "Successfully Added");
		return mapz;
	}

	public Map<String, Object> publicRegistration(Registration reg) {
		Registration Registration = new Registration();
		Map<String, Object> mapz = new HashMap<String, Object>();

		try{
			String res = regValidationDao.checkPublicReg(reg);
			List<Registration> rescuerList = new ArrayList<>();
			Registration userUidReg = new Registration();
			log.debug("After checking Email/UserName");
			if (res == "") {
				String dobString=reg.getDobString();  
			    Date dob=new SimpleDateFormat("dd/MM/yyyy").parse(dobString);  
				reg.setDob(dob);		
				
				userUidReg = assignAdminvalidator.setValidator(reg);
				if(userUidReg.getUserUid() != "" && userUidReg.getUserUid() != null){
					reg.setApproveByAdminUid(userUidReg.getUserUid());
				}else{
					mapz.put("error", "Admin not available to validate, please add Admin first.");
					return mapz;
				}
				
				// assign rescuer
				rescuerList = assignAdminvalidator.getRescuers(reg);
				
				if(rescuerList.size() > 0){
					int count = 0;
					for(Registration resc: rescuerList){
						if(count == 0)
							reg.setRescuer1(resc.getUserUid());
						
						if(count == 1)
							reg.setRescuer2(resc.getUserUid());
						
						if(count == 2)
							reg.setRescuer3(resc.getUserUid());
						
						if(count == 3)
							reg.setRescuer4(resc.getUserUid());
						
						if(count == 4)
							break;
						
						count++;
					}
				}				
				
			    int resStatus = registrationDao.registerPublicUser(reg);
			    if(resStatus > 0){
			    	
			    	log.debug("New Account Successfully Added");					
					mapz.put("info", "Successfully Added");
			    }else{
			    	mapz.put("info", "Unable to save");
			    }
				
			} else {
				System.out.println(res+" Already Exists");
				mapz.put("info", res+" Already Exists");
				return mapz;
				
			}
			
		}catch(Exception e){
			log.debug("Error occured : "+e.getMessage());
			mapz.put("info", "Unable to save");
		}
		return mapz;
	}
	public Map<String, Object> checkMobileNumberExists(String phNum) {
		Map<String, Object> mapz = new HashMap<String, Object>();
		try{
			String res = regValidationDao.checkMobileNumber(phNum);
			log.debug("After checking Mobile Number");
			if (res == "") {
				sendOtpService.sendOTP(phNum);
			    log.debug("Mobile Number not exexists");					
				mapz.put("info", "Successfully Added");
			}else{
			   	mapz.put("info", "Mobile Number already exists");
			}
			
		}catch(Exception e){
			log.debug("Error occured : "+e.getMessage());
			mapz.put("info", "Unable to check Mobile Number");
		}
		return mapz;
	}
	
	public Map<String, Object> getUserDataByUID(String UID) {
		Registration registration = new Registration();
		Map<String, Object> mapz = new HashMap<String, Object>();	
		try{
			registration = registrationDao.getUserDataByUID(UID);			
			mapz.put("Body", registration);
		}catch(Exception e){
			log.debug("Error occured : "+e.getMessage());
		}
		return mapz;
	}
	
	public Map<String, Object> saveVerificationInfo(Registration reg) {
		Map<String, Object> mapz = new HashMap<String, Object>();	
		try{
			String dobString=reg.getDobString();  
			/*if(dobString != ""){
			    Date dob=new SimpleDateFormat("dd/MM/yyyy").parse(dobString);  
				reg.setDob(dob);	
			}*/
			boolean status = registrationDao.saveVerificationInfo(reg);	
			if(status){
				mapz.put("info", "Successfully Saved");
			}else{
				mapz.put("error", "Failed to save");
			}
			
		}catch(Exception e){
			log.debug("Error occured : "+e.getMessage());
			mapz.put("error", "Failed to save");
		}
		return mapz;
	}
	
	public Map<String, Object> saveConfirmationInfo(Registration reg) {
		Map<String, Object> mapz = new HashMap<String, Object>();
		List<Registration> rescuerList = new ArrayList<>();
		try{	
			Registration userUidReg = new Registration();
			Registration totalReg = new Registration();
			if(reg.getState() == null){
				totalReg = registrationDao.getUserDataByUID(reg.getUserUid());
			}
			userUidReg = assignAdminvalidator.setValidator(totalReg);
			if(userUidReg.getUserUid() != "" && userUidReg.getUserUid() != null){
				reg.setApproveByAdminUid(userUidReg.getUserUid());
			}else{
				mapz.put("error", "Admin not available to validate, please add Admin first.");
				return mapz;
			}
			// assign rescuer
			rescuerList = assignAdminvalidator.getRescuers(totalReg);
			
			if(rescuerList.size() > 0){
				int count = 0;
				for(Registration res: rescuerList){
					if(count == 0)
						reg.setRescuer1(res.getUserUid());
					
					if(count == 1)
						reg.setRescuer2(res.getUserUid());
					
					if(count == 2)
						reg.setRescuer3(res.getUserUid());
					
					if(count == 3)
						reg.setRescuer4(res.getUserUid());
					
					if(count == 4)
						break;
					
					count++;
				}
			}
			
			boolean status = registrationDao.saveConfirmationInfo(reg);	
			if(status){
				mapz.put("info", "Successfully Saved");
			}else{
				mapz.put("error", "Failed to save");
			}
			
		}catch(Exception e){
			log.debug("Error occured : "+e.getMessage());
			mapz.put("error", "Failed to save");
		}
		return mapz;
	}
	
	public Map<String, Object> updateEmailVerify(Registration reg) {
		Map<String, Object> mapz = new HashMap<String, Object>();	
		List<Registration> approveAdminsList = new ArrayList<>();
		try{			
			boolean status = registrationDao.updateEmailVerify(reg.getUserUid());				
			if(status){
				approveAdminsList = approvalDao.approveAdmins(reg);
				for(Registration aReg: approveAdminsList){
					approvalService.sendApproveEmail(aReg, reg);
					String title= "New member registered";
					String body= reg.getFullName() + " is registered as "+reg.getRole()+", please verify and approve.";
					if(aReg.getDeviceId() != null && aReg.getDeviceId() != ""){
						notificationsService.sendNotification(aReg.getDeviceId(), title, body, WebKeys.APPROVAL);
					}	
					if(aReg.getMobileNum() != null && aReg.getMobileNum() != ""){
						sendOtpService.sendSMSAlert(aReg.getMobileNum(), body);
					}
				}
				
				mapz.put("info", "Successfully Saved");
			}else{
				mapz.put("error", "Failed to save");
			}
			
		}catch(Exception e){
			log.debug("Error occured : "+e.getMessage());
			mapz.put("error", "Failed to save");
		}
		return mapz;
	}
	
	public Map<String, Object> checkEmailExists(String email) {
		Map<String, Object> mapz = new HashMap<String, Object>();
		
		try{
			String res = regValidationDao.checkEmailExists(email);
			log.debug("After Email");
			if (res == "") {
			    log.debug("Email not exexists");					
				mapz.put("info", "Email not exist");
			}else{
			   	mapz.put("info", "Email already exists");
			}
			
		}catch(Exception e){
			log.debug("Error occured : "+e.getMessage());
			mapz.put("info", "Unable to check Email");
		}
		return mapz;
	}
}
