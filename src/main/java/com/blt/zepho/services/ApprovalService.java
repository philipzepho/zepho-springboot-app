package com.blt.zepho.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blt.zepho.dao.ApprovalDao;
import com.blt.zepho.dao.SaveDeviceIdDao;
import com.blt.zepho.model.Registration;
import com.blt.zepho.model.RegistrationList;
import com.blt.zepho.util.WebKeys;

@Service
public class ApprovalService {
	static Logger log = Logger.getLogger(ApprovalService.class.getName());
	@Autowired
	ApprovalDao aprovalDao;
	@Autowired
	AndroidPushNotificationsService notificationsService;
	@Autowired
	SendOtpService sendOtpService;
	
	SosService sosService = new SosService();

	public Map<String, Object> doApprovalService(Registration reg) {
		log.debug("inside doApprovalService");
		Map<String, Object> map = new HashMap<String, Object>();
		ArrayList<Registration> appList = new ArrayList<Registration>();
		appList = aprovalDao.approvalActivity(reg);
		if (appList.isEmpty()) {
			map.put("Error", "nothing");
			map.put("approvalList", new ArrayList<Registration>());
		} else {
			map.put("approvalList", appList);
		}
		return map;
		//return new ArrayList<Registration>();

	}
	
	
	public Map<String, Object> doApprove(Registration reg) {
		log.debug("inside doApprovalService");
		log.debug(reg.getUserUid());
		Map<String, Object> mapz = new HashMap<String, Object>();
		boolean rs;		
			rs = aprovalDao.doUserapproval(reg);
			if (rs == true) {
				SaveDeviceIdDao sdi = new SaveDeviceIdDao();
				Registration regNew = sdi.getUserDeviceIdByUID(reg.getUserUid());
				if (reg.isApprovedBySuAdmin() && reg.isApprovedByAdmin()){
					if(reg.getRole().equals("Rescuer")){
						aprovalDao.replaceFarRescuers(reg);
					}
					sendVerifyEmail(regNew);
					if(regNew.getMobileNum() != null && regNew.getMobileNum() != ""){
						sendOtpService.sendSMSAlert(regNew.getMobileNum(), WebKeys.AFTER_VERIFY);
					}
				}else if(reg.isApprovedByAdmin()){
					Registration saReg = aprovalDao.getSuperAdminByOrg(reg.getOrganization());					
					sendApproveEmail(saReg, regNew);
					String title= "New member registered";
					String body= regNew.getFullName() + " is registered as "+regNew.getRole()+", please verify and approve.";
					if(saReg.getDeviceId() != null && saReg.getDeviceId() != ""){
						try {
							notificationsService.sendNotification(saReg.getDeviceId(), title, body, WebKeys.APPROVAL);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}	
					if(saReg.getMobileNum() != null && saReg.getMobileNum() != ""){
						sendOtpService.sendSMSAlert(saReg.getMobileNum(), body);
					}
				}
				mapz.put("Success", true);
			} else {
				mapz.put("Error", "Not_Approved");
			}
		

		return mapz;
	}

	
	public Map<String, Object> doReject(Registration reg) {
		log.debug("inside doRejectingService");
		Map<String, Object> mapz = new HashMap<String, Object>();
		boolean rs;
		
			rs = aprovalDao.doUserRejected(reg);
			if (rs == true) {
				mapz.put("Success", true);
			} else {
				mapz.put("Error", "Not_Rejected");
			}
		 

		return mapz;
	}
	
	
	public Map<String, Object>  approveAll(RegistrationList registrations) {
		log.debug("inside Approve all");
		Map<String, Object> mapz = new HashMap<String, Object>();
		boolean rs;		
		rs = aprovalDao.approveAllUsers(registrations);
		if (rs == true) {
			for(Registration reg: registrations.getRegList()){
				if (reg.isApprovedBySuAdmin() && reg.isApprovedByAdmin()){
					if(reg.getRole().equals("Rescuer")){
						aprovalDao.replaceFarRescuers(reg);
					}
					sendVerifyEmail(reg);
				}
			}
			mapz.put("Success", true);
		} else {
			mapz.put("Error", "Not_Approved");
		}		
		return mapz;
	}
	
	public void sendVerifyEmail(Registration reg){
		EmailService emailService = new EmailService();
		String mailTxt =  "<p> Dear <b>"+reg.getFullName()+"</b>, </p>"+"<br/> <p>";
		mailTxt += "<p> "+WebKeys.AFTER_VERIFY+"</p><br/><br/>";
		mailTxt += "<p>---<p></br>";
		mailTxt += "<p><b>Zepho-Team</b><p>";
		String subject= "Zepho Alert: Your account verified";
		emailService.emailAlert(reg.getEmail(), mailTxt, subject);
	}
	
	public void sendApproveEmail(Registration aReg, Registration reg){
		EmailService emailService = new EmailService();
		String mailTxt =  "<p> Dear <b>"+aReg.getFullName()+"</b>, </p>"+"<br/> <p>";
		mailTxt += "<p> <b>"+reg.getFullName()+"</b> registered as "+reg.getRole()+" , please verify and approve him to access his account</p><br/><br/>";
		mailTxt += "<p>---<p></br>";
		mailTxt += "<p><b>Zepho-Team</b><p>";
		String subject= "Zepho Alert: New member registered";
		emailService.emailAlert(aReg.getEmail(), mailTxt, subject);
	}
}