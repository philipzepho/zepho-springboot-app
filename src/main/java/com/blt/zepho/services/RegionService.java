package com.blt.zepho.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blt.zepho.dao.RegionDao;
import com.blt.zepho.model.City;
import com.blt.zepho.model.District;
import com.blt.zepho.model.Registration;
import com.blt.zepho.model.States;

@Service
public class RegionService {
	static Logger log = Logger.getLogger(RegionService.class.getName());
	@Autowired
	RegionDao regionDao;
	
	public Map<String, Object> getStateNameList() {
		log.debug("inside getStateNames");	
		Map<String, Object> map = new HashMap<String, Object>();
		ArrayList<States> statesList= regionDao.getStateList();
	
		if (statesList==null) {
			map.put("success", false);
			map.put("message", "statesList empty");
	
			return map;
		} else {
			map.put("success", true);
			map.put("states", statesList);
			return map;
		}
	}
	
	public Map<String, Object> getDistrictNameList(int stateIdNumber) {
		log.debug("inside getDistrictNames");	
		//int stateIdNumber = Integer.parseInt(stateId);
		Map<String, Object> map = new HashMap<String, Object>();
		ArrayList<District> districtList= regionDao.getDistrictList(stateIdNumber);
	
		if (districtList==null) {
			map.put("success", false);
			map.put("message", "districtList is empty");
	
			return map;
		} else {
			map.put("success", true);
			map.put("district", districtList);
			return map;
		}
	}
	
	public Map<String, Object> getCityNameList(int districtIdNumber) {
		log.debug("inside getCityNames");	
		Map<String, Object> map = new HashMap<String, Object>();
		ArrayList<City> cityList= regionDao.getCityList(districtIdNumber);
	
		if (cityList==null) {
			map.put("success", false);
			map.put("message", "districtList is empty");
	
			return map;
		} else {
			map.put("success", true);
			map.put("city", cityList);
			return map;
		}
	}
	
	public Map<String, Object> updateUserProfile(Registration userProfile) {
		log.debug("inside updateUser");	
		Map<String, Object> map = new HashMap<String, Object>();
		ArrayList<Registration> cityList= regionDao.updateUser(userProfile);
	
		if (cityList==null) {
			map.put("success", false);
			map.put("message", "Failed to Add");
	
			return map;
		} else {
			map.put("success", true);
			map.put("info", "Successfully Added");
			return map;
		}
	}
	
}
