package com.blt.zepho.services;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blt.zepho.model.EmailForward;
import com.blt.zepho.util.WebKeys;


@Service
public class EmailForwardService {
	static Logger log = Logger.getLogger(EmailForwardService.class.getName());
	
	@Autowired
	EmailService emailService;
	
public Map<String,Object> doEmail(EmailForward emailForward) {
			
			Map resultMap=new HashMap<String,Object>();
		
		log.debug("BEFORE SENDING EMAIL");
		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", WebKeys.EMAIL_PORT);
		props.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", WebKeys.EMAIL_PORT);

		Session session = Session.getDefaultInstance(props,
			new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(WebKeys.EMAIL_ID, WebKeys.EMAIL_PASSWORD);
				}
			});

		try {

			log.debug("Rescuer Name:: "+emailForward.getRescuerName());
			
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(WebKeys.EMAIL_ID));
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(emailForward.getEmailId()));
			
			
			if(emailForward.isOnBehalf()==false){
				
				

				if(emailForward.getType().equals("Missionary") || emailForward.getType().equals("Rescuer") 
						|| emailForward.getType().equals("Admin") || emailForward.getType().equals("SuperAdmin")
						|| emailForward.getType().equalsIgnoreCase("ChurchMember")){
					message.setSubject("[ Forwarded from : "+emailForward.getRescuerName()+" ] "+emailForward.getFromUser()+ " is In Danger.");
					
				}else{
					message.setSubject("[ Forwarded from :  "+emailForward.getRescuerName()+" ] "+emailForward.getFromUser() + " Says His/Her "+ emailForward.getType() +" was attacked.");
					
				}
				
				
				
				//String imageUrl="https://maps.googleapis.com/maps/api/staticmap?zoom=15&size=600x300&markers=color:red%7Clabel:C%7C"+emailForward.getLatitude()+","+emailForward.getLongitude();//+"&key="+WebKeys.API_KEY;
				String imageUrl = emailService.signRequest("/maps/api/staticmap",
						"zoom=15&size=600x300&markers=color:red%7Clabel:C%7C"+emailForward.getLatitude()+","+emailForward.getLongitude()+"&key="+WebKeys.API_KEY, 
						WebKeys.SECRET_SIGNING);
				//String imageSrc= "cid:image";

		        MimeMultipart multipart = new MimeMultipart("related");
		        BodyPart messageBodyPart = new MimeBodyPart();
		        String htmlText = "<H3>"+emailForward.getFromUser()+ " is In Danger. </H3> <p> Please find the indicative location details below; Request you to kindly take necessary action immediately. </p><br/>" ;
		        htmlText+="<H4> Location Details : </H4><p><b> "+emailForward.getLocality()+" / "+emailForward.getSubLocality()+"</b></p>" + "<br/><img src='"+imageUrl+"' />";
		        log.debug("Mail Content: "+htmlText);
		        messageBodyPart.setContent(htmlText, "text/html");
		        multipart.addBodyPart(messageBodyPart);	
		        
		       /* //Image part
		        try {	        	 
		        	URLConnection openConnection = new URL(imageUrl).openConnection();
		     		openConnection.addRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0");
		     		InputStream imageStream  = openConnection.getInputStream();	   
		            DataSource fds = new ByteArrayDataSource(IOUtils.toByteArray(imageStream), "image/png");
		            messageBodyImagePart.setDataHandler(new DataHandler(fds));
		            messageBodyImagePart.setHeader("Content-ID","<image>");
		            multipart.addBodyPart(messageBodyImagePart);
		        }catch (Exception e) {
		            // TODO Auto-generated catch block
		            e.printStackTrace();
		        }*/
		        
		        message.setContent(multipart);			
				
				
			}else{
				
				if(emailForward.getObUserUid()==null || emailForward.getObUserUid()==""){
					message.setSubject("[ Forwarded from :   "+emailForward.getRescuerName()+" ] "+emailForward.getObUserName()+ " is In Danger.");
					message.setText("Dear Rescuer," +
							"\n\n "
							+ " " +emailForward.getFromUser() +" Says "+emailForward.getObUserName()+ " is In Danger."
							+ "  \n Location : "
							+ emailForward.getLocationName()
							+"\n\n  So please respond immediately.");

		
				}else{
					message.setSubject("[ Forwarded from :   "+emailForward.getRescuerName()+" ] "+emailForward.getFromUser()+ " is In Danger.");
					message.setText("Dear Rescuer," +
							"\n\n "
							+ " " +emailForward.getObUserName() +" Says "+emailForward.getFromUser()+ " is In Danger."
							+ "  \n Location : "
							+ emailForward.getLocationName()
							+"\n\n  So please respond immediately.");
				}
			}

			
			Transport.send(message);
			
			resultMap.put("Success", "true");

			log.debug("EMAIL SENT");
			
			return resultMap;

		} catch (MessagingException | InvalidKeyException | NoSuchAlgorithmException | UnsupportedEncodingException | URISyntaxException e) {
			//throw new RuntimeException(e);
			log.fatal(e.getMessage());
			resultMap.put("Success", "false");
			return resultMap;
		}
	}
	
}
