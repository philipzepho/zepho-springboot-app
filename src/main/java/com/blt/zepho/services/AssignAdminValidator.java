package com.blt.zepho.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.blt.zepho.dao.RegistrationDao;
import com.blt.zepho.jdbc.CreateJDBCConnection;
import com.blt.zepho.model.Registration;
import com.blt.zepho.model.States;

public class AssignAdminValidator {
	static Logger log = Logger.getLogger(AssignAdminValidator.class.getName());
    @Autowired
	RegistrationDao registrationDao;
	public Registration setValidator(Registration reg) {
		log.debug("reg " + reg.getState());
		Connection connection = null;
		PreparedStatement st = null;
		ArrayList<Registration> regList = new ArrayList<Registration>();
		try {		
			
			connection = CreateJDBCConnection.getConnection();
			try {
				connection = CreateJDBCConnection.getConnection();
				st = connection.prepareStatement("select * from registration where role='Admin' and organization=? and isApproved=? and (city='"+reg.getCity()+"' or district='"+reg.getDistrict()+"' or state='"+reg.getState()+"')");
                st.setString(1, reg.getOrganization());
                st.setBoolean(2, true);
				ResultSet res = st.executeQuery();
				while (res.next()) {
					Registration regObj = new Registration();
					regObj.setCity(res.getString("city"));
					regObj.setDistrict(res.getString("district"));
					regObj.setState(res.getString("state"));
					regObj.setUserUid(res.getString("userUid"));
					regList.add(regObj);
				}
			} catch (SQLException e) {
				log.fatal(e.getMessage());
			}
		} catch (Exception e) {
			log.fatal(e.getMessage());
		}
		Registration userUidReg = new Registration();
		for (int i=0; i<regList.size(); i++){
			if (reg.getCity().equalsIgnoreCase(regList.get(i).getCity())){
				userUidReg= regList.get(i);
			}
		}
		if(userUidReg.getUserUid() == "" || userUidReg.getUserUid() == null){
			for(Registration r: regList){
				if (reg.getDistrict().equalsIgnoreCase(r.getDistrict())){
					userUidReg= r;
				}
			}			
		}
		if(userUidReg.getUserUid() == "" || userUidReg.getUserUid() == null){
			for(Registration r: regList){
				if (reg.getState().equalsIgnoreCase(r.getState())){
					userUidReg= r;
				}
			}
		}
		if(userUidReg.getUserUid() == "" || userUidReg.getUserUid() == null){
			try {
				connection = CreateJDBCConnection.getConnection(); 
				try {
					connection = CreateJDBCConnection.getConnection();
					st = connection.prepareStatement("select * from registration where role='Admin' and organization=?");
	                st.setString(1, reg.getOrganization());
					ResultSet res = st.executeQuery();
					while (res.next()) {
						Registration regObj = new Registration();
						regObj.setUserUid(res.getString("userUid"));
						regList.add(regObj);
					}
				} catch (SQLException e) {
					log.fatal(e.getMessage());
				}
			} catch (Exception e) {
				log.fatal(e.getMessage());
			}
			if (regList.size()>0){
				userUidReg= regList.get(0);
			}			
		}
/*		else if (reg.getDistrict()==regList.get(i).getDistrict()){
			userUid= regList.get(i).getUserUid();
		}else if (reg.getState()==regList.get(i).getState()){
			userUid= regList.get(i).getUserUid();
		}*/
		return userUidReg;
		
	}
	public List<Registration> getRescuers(Registration reg) {
		Connection connection = null;
		PreparedStatement st = null;
		ArrayList<Registration> regList = new ArrayList<Registration>();
		List<Registration> rescuerList = new ArrayList<Registration>();
		try {		
			
			connection = CreateJDBCConnection.getConnection();
			try {
				connection = CreateJDBCConnection.getConnection();
				st = connection.prepareStatement("select * from registration where role='Rescuer' and organization=? and isApproved=?");
                st.setString(1, reg.getOrganization());
                st.setBoolean(2, true);
				ResultSet res = st.executeQuery();
				while (res.next()) {
					Registration regObj = new Registration();
					regObj.setCity(res.getString("city"));
					regObj.setDistrict(res.getString("district"));
					regObj.setState(res.getString("state"));
					regObj.setUserUid(res.getString("userUid"));
					regList.add(regObj);
				}
			} catch (SQLException e) {
				log.fatal(e.getMessage());
			}
		} catch (Exception e) {
			log.fatal(e.getMessage());
		}
		HashMap<String, String> uniqCheck = new HashMap<>();
		for(Registration r: regList){
			if (reg.getCity().equalsIgnoreCase(r.getCity())){
				rescuerList.add(r);
				uniqCheck.put(r.getUserUid(), r.getUserUid());
			}
		}
		for(Registration r: regList){
			if (reg.getDistrict().equalsIgnoreCase(r.getDistrict())){
				if(!uniqCheck.containsKey(r.getUserUid())){
					rescuerList.add(r);
					uniqCheck.put(r.getUserUid(), r.getUserUid());
				}				
			}
		}	
		for(Registration r: regList){
			if (reg.getState().equalsIgnoreCase(r.getState())){
				if(!uniqCheck.containsKey(r.getUserUid())){
					rescuerList.add(r);
					uniqCheck.put(r.getUserUid(), r.getUserUid());
				}
			}
		}
		
		if(rescuerList.size() < 4 && regList.size() > 0){
			for(int j=0; j<regList.size();j++ ){
				if(!uniqCheck.containsKey(regList.get(j).getUserUid())){
					if(rescuerList.size()<4){
						rescuerList.add(regList.get(j));
					}else{
						break;
					}
					uniqCheck.put(regList.get(j).getUserUid(), regList.get(j).getUserUid());
				}
			}
		}
		
		return rescuerList;
	}
}
