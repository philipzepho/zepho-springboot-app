package com.blt.zepho.services;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blt.zepho.dao.ClearDoNotGoAlertDao;
import com.blt.zepho.dao.ClearSosItemDao;
import com.blt.zepho.model.ClearSosListItem;
import com.blt.zepho.model.ItemId;

@Service 
public class ClearDoNotGoService {

	static Logger log = Logger.getLogger(ClearDoNotGoService.class.getName());

	@Autowired
	ClearDoNotGoAlertDao clearDoNotGoAlertDao;
	   
	   public Map<String,Object> othersWatched(ItemId itemId){ 
		   Map<String, Object> result = new HashMap<String, Object>();
		  
		   log.debug("USER:: " + itemId.getUuid());
		   
		   	boolean res=clearDoNotGoAlertDao.clearDoNotGoItem(itemId);
		   	
		   	result.put("Success",""+res);
			return result;

			
	   }

}


