package com.blt.zepho.services;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blt.zepho.dao.ApprovalDao;
import com.blt.zepho.dao.LoginDao;
import com.blt.zepho.dao.RegistrationDao;
import com.blt.zepho.dao.UserUidFullNameDao;
import com.blt.zepho.jdbc.CreateJDBCConnection;
import com.blt.zepho.model.Registration;
import com.blt.zepho.model.Rescuer;
import com.blt.zepho.model.UserLog;
import com.blt.zepho.util.AuthNumber;
import com.blt.zepho.util.WebKeys;

import javax.ws.rs.core.Response;

@Service
public class LoginService {
	
	static Logger log = Logger.getLogger(LoginService.class.getName());

    @Autowired
	LoginDao loginDao;
    @Autowired
	RegistrationDao regDao;
	Registration reg;
	@Autowired
	private HttpServletRequest servletRequest;
	@Autowired
	UserUidFullNameDao userUidFullNameDao;
	@Autowired
	ApprovalDao approvalDao;
	@Autowired
	SendOtpService sendOtpService;
	@Autowired
	DecodeService decodeService;
	
	
	public Map<String, Object> loginUser(Registration reg) {

		log.debug("email " + reg.getEmail());
		
		servletRequest.getSession(true);
		
		log.debug("Login from login service");
		
		Registration resreg = loginDao.loginUser(reg);

		Map<String, Object> map = null;
		
		System.out.println("resreg " + resreg);
		if (resreg == null) {
			
			map =new HashMap<String, Object>();
			map.put("Error", true);
			map.put("ErrorMessage", "Invalid Credentials");
			
		} else {
			
			map =	new HashMap<String, Object>();
			
			if(resreg.getLoginAttempts() > 2){
				map.put("Error", true);
				map.put("ErrorMessage", WebKeys.MAX_ATTEMPTS);
				return map;
			}
			map.put("Error", false);
						
			// URL selection
			String role=resreg.getRole();
			role = role.trim();
			resreg.setRole(role);
			log.debug("Role: "+role);
			
			// This is for Zepho main Testing and Realease.
			if(role.equals("Admin")){
				map.put("url", WebKeys.PATH_EXT+"/admin");
				if(resreg.getIsApproved()){
					resreg.setSalt(decodeService.getRandomAuthNumber(AuthNumber.ADMIN));
				}							
				map.put("Body", resreg);
				
			}else if(role.equals("SuperAdmin")){
				map.put("url", WebKeys.PATH_EXT+"/superAdmin");
				if(resreg.getIsApproved()){
					resreg.setSalt(decodeService.getRandomAuthNumber(AuthNumber.SUPER_ADMIN));
				}			
				map.put("Body", resreg);
			}else if(role.equals("Rescuer")){
				map.put("url", WebKeys.PATH_EXT+"/rescuer");
				if(resreg.getIsApproved()){
					resreg.setSalt(decodeService.getRandomAuthNumber(AuthNumber.RESCUER));
				}
				map.put("Body", resreg);
			}else if(role.equals("Missionary")){
				map.put("url",WebKeys.PATH_EXT+"/missionary");
				if(resreg.getIsApproved()){
					resreg.setSalt(decodeService.getRandomAuthNumber(AuthNumber.MISSIONARY));
				}
				map.put("Body", resreg);
			}else if(role.equals("MasterAdmin")){
				map.put("url", WebKeys.PATH_EXT+"/masterAdmin");
				if(resreg.getIsApproved()){
					resreg.setSalt(decodeService.getRandomAuthNumber(AuthNumber.MASTER_ADMIN));
				}
				map.put("Body", resreg);
			}else if(role.equals("Public")||role.equals("ChurchMember")){
				map.put("url", WebKeys.PATH_EXT+"/public");
				if(resreg.getIsApproved()){
					resreg.setSalt(decodeService.getRandomAuthNumber(AuthNumber.CHURCH_MEMBER));
				}
				map.put("Body", resreg);
			}else if(role.equals("Registered")){
				map.put("url", WebKeys.PATH_EXT+"/registered");
				map.put("Body", resreg);
			}
		}
		return map;

	}
	
	public Map<String, Object> verifyUser(Registration reg) {
		Registration resreg = null;
		servletRequest.getSession(true);	
		resreg = loginDao.verifyUserByMobile(reg);
		Map<String, Object> map = null;		
		if (resreg == null) {			
			map =new HashMap<String, Object>();
			map.put("Error", true);
			map.put("ErrorMessage", "Invalid Credentials");			
		}else {
			sendOtpService.sendOTP(resreg.getMobileNum());
			map =	new HashMap<String, Object>();
			map.put("Error", false);
			map.put("Body", resreg);
		}
		return map;
	}

	public Map<String, Object> verifySecurityQuestions(Registration reg){
		Registration resreg = null;
		String mailTxt="";		
		String subject="";		
		EmailService emailService = new EmailService();
		resreg = loginDao.verifySecurityQuestions(reg);		
		Map<String, Object> map = null;		
		if (resreg == null) {			
			map =new HashMap<String, Object>();
			map.put("Error", true);
			map.put("ErrorMessage", "Invalid Details");			
		}else {
			map =	new HashMap<String, Object>();
			map.put("Error", false);
			map.put("Body", resreg);
			
			resreg = loginDao.verifyUser(reg);
			Registration saReg = approvalDao.getSuperAdminByOrg(resreg.getOrganization());	
			mailTxt =  "<p> Dear <b>"+resreg.getFullName()+"</b>, </p>"+"<br/> <p>";
			mailTxt += "<p> We noticed that someone <b> attempt to change your password</b>. We hope, that's you. If not, Please contact your Admin(Email: "+saReg.getEmail()+") immediately.</p><br/><br/>";
			mailTxt += "<p>---<p></br>";
			mailTxt += "<p><b>Zepho-Team</b><p>";
			mailTxt += "</br>"+WebKeys.SUPPORT_EMAIL;
			subject= "Zepho Security Alert: Attempt to Password Change";
			
			emailService.emailAlert(resreg.getEmail(), mailTxt, subject);	
		}			
		
		
		return map;
	}
	
	
	public Map<String, Object> resetPassword(Registration reg){
		Map<String, Object> map = null;	
		UserLog userLog = null;
		String mailTxt="";		
		String subject="";		
		try {
			EmailService emailService = new EmailService();
			CommonSerices commonSerices = new CommonSerices();
			String changePwd = reg.getPassword();
			reg.setPassword(Password.hashPassword(changePwd));
			boolean status = regDao.resetPassword(reg);
			
			if (!status) {			
				map =new HashMap<String, Object>();
				map.put("Error", true);			 	
			}else {
				map =	new HashMap<String, Object>();
				reg = loginDao.getUserDetailsByUid(reg);
				Registration saReg = approvalDao.getSuperAdminByOrg(reg.getOrganization());
				subject = "Zepho Security Alert: Password Changed";	
				mailTxt = "<p> Dear <b>"+reg.getFullName()+"</b>, </p>"+"<br/> <p>";
				mailTxt += "<p> We noticed that someone <b>changed your password</b>. We hope, that's you. If not, Please contact your Admin(Email: "+saReg.getEmail()+") immediately.</p><br/><br/>";
				mailTxt += "<p>---<p></br>";
				mailTxt += "<p><b>Zepho-Team</b><p>";
				mailTxt += "</br>"+WebKeys.SUPPORT_EMAIL;
				emailService.emailAlert(reg.getEmail(), mailTxt, subject);	
				map.put("Error", false);
				

				userLog = new UserLog();
				userLog.setUserId(reg.getId());
				userLog.setChangedBy(reg.getUserUid());
				userLog.setUpdatedVal(Password.hashPassword(changePwd));
				userLog.setModuleName(WebKeys.RESET_PASSWORD);
				userLog.setStatus(WebKeys.STATUS_ACTIVE);
				commonSerices.maintainLog(userLog);
			}
			map.put("status", status);
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		return map;
	}
	
}
