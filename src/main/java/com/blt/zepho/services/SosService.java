package com.blt.zepho.services;

import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blt.zepho.dao.ApprovalDao;
import com.blt.zepho.dao.ListRescuerDeviceIdDao;
import com.blt.zepho.dao.RegRescuerListDao;
import com.blt.zepho.dao.RescuerListByUserIdDao;
import com.blt.zepho.dao.SosDao;
import com.blt.zepho.dao.SosWatchDao;
import com.blt.zepho.model.Registration;
import com.blt.zepho.model.Rescuer;
import com.blt.zepho.model.Sos;
import com.blt.zepho.model.SosWatch;
import com.blt.zepho.util.WebKeys;

@Service
public class SosService {
	static Logger log = Logger.getLogger(SosService.class.getName());

	@Autowired
	ListRescuerDeviceIdDao listRescuerDeviceIdDao;
	
	@Autowired
	AndroidPushNotificationsService notificationsService;

	// Method to send Notifications from server to client end.

	@Autowired
	SosDao sosDao;
	Sos sos;

	@Autowired
	SosWatchDao sosWatchDao;
	@Autowired
	ApprovalDao aprovalDao;
	@Autowired
	RegRescuerListDao regRescuerListDao;
	@Autowired
	RescuerListByUserIdDao rescuerListByUserIdDao;
	@Autowired
	SendOtpService sendOtpService;
	
	public Map<String,Object> sosRequest(Sos sos) throws Exception {
		this.sos = sos;
		
		String uuid = UUID.randomUUID().toString();
		String title;
		String body;
		sos.setUuid(uuid);
		
		Map resultMap=new HashMap<String,Object>();

		// here this below snippet is adding sos data when sos button is clicked to a
		// separate table, maintaining sos listt
		log.debug("USER:: " + sos.getFromUser());
		log.debug("SUB Locality: "+ sos.getSubLocality());
		
		sosDao.addSosData(sos);

		ArrayList<String> rescuerUserIDList = new ArrayList<String>();

		
		
		
		ArrayList<String> resStringList;
		if(sos.isOnBehalf()==false){
			resStringList= rescuerListByUserIdDao.GetRescuerByUserId(sos.getUserUid());
			
			if(sos.getType().equals("Missionary") || sos.getType().equals("SuperAdmin") || sos.getType().equals("Rescuer")|| sos.getType().equals("Admin")){
				title=sos.getFromUser() + " in Danger";
				body=sos.getFromUser() + " in Danger . Please check SOSRequestList for more details";
			}else{
				log.debug("SOS TYPE: "+sos.getType());
				title=sos.getFromUser() + " Says His/Her "+ sos.getType() +" was attacked.";
				body=sos.getFromUser() + " Says His/Her "+ sos.getType() +" was attacked. Please check SOSRequestList for more details";
			}
			
			
		}else{
			
			if(sos.getObUserUid()==null || sos.getObUserUid()==""){
				resStringList= rescuerListByUserIdDao.GetRescuerByUserId(sos.getUserUid());
				title=sos.getObUserName() + " in Danger";
				body=sos.getFromUser() + " Says "+sos.getObUserName() +" is in Danger. Please check SOSRequestList for more details";
			}else{
				resStringList= rescuerListByUserIdDao.GetRescuerByUserId(sos.getUserUid());
				title=sos.getFromUser() + " in Danger";
				body= sos.getObUserName() + " Says "+sos.getFromUser() +" is in Danger. Please check SOSRequestList for more details";
			}
		}		
 
		String organization = sos.getUserOrg();
		ArrayList<Rescuer> rescuerList = new ArrayList<Rescuer>();
		String rescuerIds = "";
		for (String registration : resStringList) {
			if(rescuerIds == "" || rescuerIds == null){
				rescuerIds = "'"+registration+"'";
			}else{
				rescuerIds += ",'"+registration+"'";
			}
		}
		rescuerList = regRescuerListDao.GetRescuerByUserIdz(rescuerIds);
		if(organization == "" || organization == null){
			if(rescuerList.size() > 0){				
				organization = rescuerList.get(0).getOrg();
			}			
		}

		if(organization != "" && organization != null){ //Adding Super Admin to send notifications
			if(!sos.getType().equalsIgnoreCase("SuperAdmin")){
				Registration saReg = aprovalDao.getSuperAdminByOrg(organization);
				Rescuer res = new Rescuer();
				res.setUserUid(saReg.getUserUid());
				res.setDeviceId(saReg.getDeviceId());
				res.setEmail(saReg.getEmail());
				res.setFullName(saReg.getFullName());
				res.setMobile(saReg.getMobileNum());
				rescuerList.add(res);
			}			
			
			//get users who are matched the user pincode
			if(sos.getUserPincode() > 0){
				ArrayList<Rescuer> resList = new ArrayList<Rescuer>();
				if(rescuerIds == "" || rescuerIds == null){
					rescuerIds = "'"+sos.getUserUid()+"'";
				}else{
					rescuerIds += ",'"+sos.getUserUid()+"'";
				}
				resList = sosDao.getRescuersByPin(sos.getUserPincode(), organization, rescuerIds);
				if(resList.size()>0){
					rescuerList.addAll(resList);
				}
			}
		}

		String mobileList = "";
		for (Rescuer rescuerz : rescuerList) {
			sosWatchDao.addSosWatchData(new SosWatch(sos.getUuid(), rescuerz.getUserUid(), false));
			if(rescuerz.getMobile() != null && rescuerz.getMobile() != ""){
				if(mobileList == "" || mobileList == null){
					mobileList = rescuerz.getMobile();
				}else{
					mobileList += ","+rescuerz.getMobile();
				}
			}			
		}

		if(mobileList != null && mobileList != ""){
			sendOtpService.sendSMSAlert(mobileList, sos.getFromUser() + " in Danger. Please check Zepho app for more details");
		}
		
		// Push notification Trigger
		// Email Trigger
		EmailService emailService = new EmailService();
		
		for (Rescuer rescuerz : rescuerList) {
			if(rescuerz.getDeviceId()!=null){
				log.debug("DEVICE ID: "+rescuerz.getDeviceId());
				notificationsService.sendNotification(rescuerz.getDeviceId(),title,body, WebKeys.SOS_ALERT);
				
			}
		}	
		
		if(rescuerList.size() > 0){
			emailService.doEmail(rescuerList,sos);
		}
		
		resultMap.put("Success", "true");
		return resultMap;
	}

	// userDeviceIdKey is the device id you will query from your database

	/*public void pushFCMNotification(String userDeviceIdKey,String title,String body) throws Exception {
		
		log.debug("BEFORE PUSH NOTIFICATION");
		
		if(userDeviceIdKey==null){
			return ;
		}
		

		String authKey = WebKeys.AUTH_KEY_FCM; // You FCM AUTH key
		String FCMurl = WebKeys.API_URL_FCM;

		URL url = new URL(FCMurl);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		// System.out.println("sdfsdfsaf2");

		conn.setUseCaches(false);
		conn.setDoInput(true);
		conn.setDoOutput(true);

		conn.setRequestMethod("POST");
		conn.setRequestProperty("Authorization", "key=" + authKey);
		conn.setRequestProperty("Content-Type", "application/json");

		JSONObject json = new JSONObject();
		json.put("to", userDeviceIdKey.trim());
		JSONObject info = new JSONObject();

		info.put("title",title); // Notification title
		info.put("body",body); // Notification
																											// body
		info.put("sound", "default");
		info.put("click_action", "FCM_PLUGIN_ACTIVITY");
		info.put("icon", "icon");

		json.put("notification", info);
		JSONObject data = new JSONObject();
		data.put("title", title); // Notification title
		data.put("body", body); // Notification
																											// body
		json.put("data", data);
		

		OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
		wr.write(json.toString());
		wr.flush();
		conn.getInputStream();
		log.debug("PUSH NOTIFICATION SENT");

	}*/

}
