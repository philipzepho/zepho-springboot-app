package com.blt.zepho.util;

/*   Added by Santosh on 01/08/2018   */

public interface WebKeys {
	
	public final static String GOOGLE_JSON_FILE_PATH = "zepho-aaded-firebase-adminsdk-53t6c-30858a695e.json";
	public final static String AUTH_KEY_FCM = "AAAAb2bCv08:APA91bGulaqdDwuH0fzei6PI_Ukwl9wpOrr5P-JWthI-pt8q8bfEljHomewryR8OmPRnpnGqdF6gWiW-9yG2wn6lY5gl0FidsuoVhQ1LNt4oVMYkHujn7SYOpQj_0wS1ZHQrUzcxlAy0";
	public final static String API_URL_FCM = "https://fcm.googleapis.com/fcm/send";
	public final static String API_KEY = "AIzaSyAbSJbFoPK5ACpIeThdaNrU0OQhz5US6ZM";//"AIzaSyAbSJbFoPK5ACpIeThdaNrU0OQhz5US6ZM";//"AIzaSyCRoahIl-op1dSnO7vaD97o2xYQ4Vo68sk";//
	public static final String SUPPORT_EMAIL = "support@zepho.org";
	public static final String SECRET_SIGNING = "pGsNbGnVJTRTEN_5iP6rJsUpDk8=";//"HnoN1xWl6s7eEWhzpv_ObnSwiY0=";//"Cf3EGZApK48-LFa4LeDYFtg7oCM=";
	
	public static final String RESET_PASSWORD = "Reset Password";
	public static final String STATUS_ACTIVE= "Active";
	public static final String STATUS_INACTIVE= "Inactive";
	public static final String ORIGIN = "http://localhost:8100";
	
	public static final String URL = "jdbc:mysql://localhost:3306/zepho?user=root&password=mysql&useUnicode=true&characterEncoding=utf-8&useSSL=false";
	//public static final String URL = "jdbc:mysql://45.55.233.197:3306/zepho-test?user=dbuser&password=ilMySQL@ZEP007&useUnicode=true&characterEncoding=utf-8&useSSL=true";
	//public static final String URL = "jdbc:mysql://45.55.233.197:3306/zepho-dev?user=dbuser&password=ilMySQL@ZEP007&useUnicode=true&characterEncoding=utf-8&useSSL=true";
	//public static final String USER = "root";
	//public static final String PASSWORD = "root";
	public static final String DRIVER_CLASS = "com.mysql.jdbc.Driver";
	
	public static final String EMAIL_ID="alert@zepho.org ";//"zephoalerts@gmail.com";
	public static final String EMAIL_PASSWORD="mailZepho";
	public static final String EMAIL_PORT="465";
	
	public static final String MSG91_AUTH_KEY="192133A9Kbmfg3oa5be42b2c";
	//public String PATH_EXT = "";
	public static final String PATH_EXT = "/zepho-test";
	
	public static final String USER_ROLE = "User";
	public static final String MISSIONARY = "Missionary";	
	public static final String SUPER_ADMIN = "SuperAdmin";
	public static final String ADMIN = "Admin";
	public static final String MASTER_ADMIN = "MasterAdmin";
	public static final String RESCUER = "Rescuer";
	public static final String CHURCH_MEMBER = "ChurchMember";
	public static final String MAX_ATTEMPTS = "Reached maximum login attempts";
	
	public static final String ZEPHO_TOMCAT_SERVER_PATH = "catalina.base";
	public static final String ZEPHO_TOMCAT_WEBAPPS_PATH = "webapps";
	public static final String ZEPHO_USERS_FILES = "ZephoUserFiles";
	public static final String IMAGES = "images";
	public static final String ATTACHMENT = "ATTACHMENT";
	public static final String APPROVAL = "approval";
	public static final String NOTIFICATION = "notification";
	public static final String SOS_ALERT = "sos";
	public static final String AFTER_VERIFY = "You are successfully verified by Admin and Super Admin , please login to access your account";
}
