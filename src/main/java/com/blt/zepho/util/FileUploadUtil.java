package com.blt.zepho.util;

import java.io.File;

import javax.servlet.http.HttpServletRequest;

import com.blt.zepho.model.DoNotGo;
import com.blt.zepho.model.Registration;


public class FileUploadUtil {
	
    public static String getServerPath()
    {
		String path = System.getProperty( WebKeys.ZEPHO_TOMCAT_SERVER_PATH );
		return path;
    }
    
    public static String getWepAppsPath()
    {
		String webAppsPath =  getServerPath()+ File.separator +  WebKeys.ZEPHO_TOMCAT_WEBAPPS_PATH;
		File f = new File(webAppsPath);
		if (!f.isDirectory()) {
			f.setExecutable(true, false);
            f.mkdir();
             
        } 
		return webAppsPath;
    }
		
    public static String getUploadImagePath(DoNotGo doNotGo)
    {
		String fullPath =  File.separator+ doNotGo.getOrg() +  File.separator+ doNotGo.getFromUserUid() +  File.separator+ WebKeys.IMAGES;
		File f = new File(getCommonFilesPath() + fullPath);
		if (!f.isDirectory()) {
			f.setExecutable(true, false);
            f.mkdirs();             
        }
		File[] directories = f.listFiles(File::isDirectory);
		if(directories != null){
			fullPath = fullPath + File.separator+ WebKeys.ATTACHMENT + "_" + directories.length ;
		}
		File fPath = new File(getCommonFilesPath() + fullPath);
		if (!fPath.isDirectory()) {
			fPath.setExecutable(true, false);
			fPath.mkdirs();             
        }
		return fullPath;
    }
    
    /*public static String getUserPath(UserRegistration user,HttpServletRequest request)
    {
		String fullPath =    WebKeys.ZEPHO_USERS_FILES+ "/"+user.getUser().getUserTypeid()+  "/"+ user.getRegId();
		return fullPath;
    }*/
	
    
	
	
	
	public static String getCommonFilesPath(){
		String fullPath =  getServerPath() +  File.separator+ WebKeys.ZEPHO_USERS_FILES;
		File f = new File(fullPath);
		if (!f.isDirectory()) {
			f.setExecutable(true, false);
            f.mkdirs();
             
        }
		return fullPath;
	}
	
}
