package com.blt.zepho.jdbc;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.blt.zepho.util.WebKeys;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.sql.DataSource;

@Configuration
/*@PropertySource("classpath:application.properties")*/
public class CreateJDBCConnection {
	
	/*@Autowired
	Environment environment;*/
	//static reference to itself
	static Logger log = Logger.getLogger(CreateJDBCConnection.class.getName());
	
	private final String URL = "spring.datasource.url";
	private final String USER = "spring.datasource.username";
	private final String DRIVER = "spring.datasource.driver-class-name";
	private final String PASSWORD = "spring.datasource.password";
	
	private static CreateJDBCConnection instance = new CreateJDBCConnection();
  
  
  //private constructor
  public CreateJDBCConnection() {
      try {
          //Step 2: Load MySQL Java driver
          Class.forName(WebKeys.DRIVER_CLASS);
          
      } catch (ClassNotFoundException e) {	
          log.fatal(e.getMessage());
      }
  }
   
  private Connection createConnection() {

      Connection connection = null;
      try {
          //Step 3: Establish Java MySQL connection
    	  log.debug("BEFORE CONNECTION...");
//          connection = DriverManager.getConnection(URL, USER, PASSWORD);
    	  connection = DriverManager.getConnection(WebKeys.URL);
          log.debug("AFTER CONNECTION...");
         
      } catch (SQLException e) {
    	  log.fatal(e.getMessage());
          log.debug("ERROR: Unable to Connect to Database.");
      }
      
     
      return connection;
  }   
   
  public static Connection getConnection() {
//	  CreateJDBCConnection instance = new CreateJDBCConnection();
      return instance.createConnection();
  }
  
  /*@Bean
  DataSource dataSource() {
	  DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
	  driverManagerDataSource.setUrl(environment.getProperty(URL));
	  driverManagerDataSource.setUsername(environment.getProperty(USER));
	  driverManagerDataSource.setPassword(environment.getProperty(PASSWORD));
	  driverManagerDataSource.setDriverClassName(environment.getProperty(DRIVER));
	  return driverManagerDataSource;
  }*/
}