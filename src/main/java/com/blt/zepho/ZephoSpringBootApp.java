package com.blt.zepho;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZephoSpringBootApp {

	public static void main(String[] args) {
		SpringApplication.run(ZephoSpringBootApp.class, args);
	}
}
