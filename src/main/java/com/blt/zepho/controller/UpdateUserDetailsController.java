package com.blt.zepho.controller;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.blt.zepho.model.UserData;
import com.blt.zepho.model.UserDetails;
import com.blt.zepho.services.DecodeService;
import com.blt.zepho.services.UpdateUserDetailsService;
import com.blt.zepho.util.WebKeys;

@Controller
@SessionAttributes("name")
@CrossOrigin(origins = WebKeys.ORIGIN)
public class UpdateUserDetailsController {
	static Logger log = Logger.getLogger(UpdateUserDetailsController.class.getName());

	@Autowired
	UpdateUserDetailsService updateUserDetailsService;
	@Autowired
	DecodeService decodeService;

	@RequestMapping(value="/updateUser", method = RequestMethod.POST)
	public ResponseEntity<String> updateUser(@RequestBody String userDetails, ModelMap model) {
		Map<String, Object> map = null;
		UserDetails updateUser = decodeService.decodeUserDetails(userDetails);
		map = updateUserDetailsService.updateUser(updateUser);
		String response = decodeService.encodeData(map);		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}	
}