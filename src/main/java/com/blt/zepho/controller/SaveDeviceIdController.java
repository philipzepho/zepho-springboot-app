package com.blt.zepho.controller;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.blt.zepho.model.Registration;
import com.blt.zepho.services.DecodeService;
import com.blt.zepho.services.SaveDeviceIdService;
import com.blt.zepho.util.WebKeys;

@Controller
@SessionAttributes("name")
@CrossOrigin(origins = WebKeys.ORIGIN)
public class SaveDeviceIdController {
	static Logger log = Logger.getLogger(SaveDeviceIdController.class.getName());

	@Autowired
	SaveDeviceIdService saveDeviceIdService;
	@Autowired
	DecodeService decodeService;

	@PreAuthorize("hasAnyAuthority('"+WebKeys.ADMIN+"', '"+WebKeys.SUPER_ADMIN+"', '"+WebKeys.MASTER_ADMIN+"', "
			+ "'"+WebKeys.RESCUER+"', '"+WebKeys.MISSIONARY+"', '"+WebKeys.CHURCH_MEMBER+"', "
					+ "'"+WebKeys.USER_ROLE+"')")
	@RequestMapping(value="/SaveDeviceId", method = RequestMethod.POST)
	public ResponseEntity<String> DoSaveDeviceIdService(@RequestBody String regString, ModelMap model) {
		Map<String, Object> map = null;
		Registration reg = decodeService.decodeRegistration(regString);
		map = saveDeviceIdService.DoSaveDeviceIdService(reg);
		String response = decodeService.encodeData(map);		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAnyAuthority('"+WebKeys.ADMIN+"', '"+WebKeys.SUPER_ADMIN+"', '"+WebKeys.MASTER_ADMIN+"', "
			+ "'"+WebKeys.RESCUER+"', '"+WebKeys.MISSIONARY+"', '"+WebKeys.CHURCH_MEMBER+"', "
					+ "'"+WebKeys.USER_ROLE+"')")
	@RequestMapping(value="/removeDeviceId", method = RequestMethod.POST)
	public ResponseEntity<String> removeDeviceId(@RequestBody String regString, ModelMap model) {
		Map<String, Object> map = null;
		Registration reg = decodeService.decodeRegistration(regString);
		map = saveDeviceIdService.removeDeviceId(reg);
		String response = decodeService.encodeData(map);		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}	
}