package com.blt.zepho.controller;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.blt.zepho.model.UserData;
import com.blt.zepho.services.DecodeService;
import com.blt.zepho.services.GetMissionaryListService;
import com.blt.zepho.util.WebKeys;

@Controller
@SessionAttributes("name")
@CrossOrigin(origins = WebKeys.ORIGIN)
public class GetMissionaryListController {
	static Logger log = Logger.getLogger(GetMissionaryListController.class.getName());
	@Autowired
	GetMissionaryListService getMissionaryListService;
	@Autowired
	DecodeService decodeService;

	@PreAuthorize("hasAnyAuthority('"+WebKeys.RESCUER+"', '"+WebKeys.MISSIONARY+"')")
	@RequestMapping(value="/GetMissionaryList", method = RequestMethod.POST)
	public ResponseEntity<String> doGetMissionaryListService(@RequestBody String userData, ModelMap model) throws Exception {
		Map<String, Object> map = null;
		UserData userdatas = decodeService.decodeUserData(userData);
		map = getMissionaryListService.doGetMissionaryListService(userdatas);
		String response = decodeService.encodeData(map);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('"+WebKeys.RESCUER+"')")
	@RequestMapping(value="/GetMappedMissionaryList", method = RequestMethod.POST)
	public ResponseEntity<String> getRescuerMappedMisListService(@RequestBody String userInfo, ModelMap model) throws Exception {
		Map<String, Object> map = null;
		UserData userdatas = decodeService.decodeUserData(userInfo);
		map = getMissionaryListService.getRescuerMappedMisListService(userdatas);
		String response = decodeService.encodeData(map);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
}
