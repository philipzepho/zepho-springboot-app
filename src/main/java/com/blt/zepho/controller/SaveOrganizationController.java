package com.blt.zepho.controller;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.blt.zepho.model.Organization;
import com.blt.zepho.model.Registration;
import com.blt.zepho.services.DecodeService;
import com.blt.zepho.services.SaveOrganizationService;
import com.blt.zepho.util.WebKeys;

@Controller
@SessionAttributes("name")
@CrossOrigin(origins = WebKeys.ORIGIN)
public class SaveOrganizationController {
	static Logger log = Logger.getLogger(ApprovalController.class.getName());

	@Autowired
	SaveOrganizationService saveOrganizationService;
	@Autowired
	DecodeService decodeService;

	@RequestMapping(value="/SaveOrganizationId", method = RequestMethod.POST)
	public ResponseEntity<String> DoSaveOrganizationIdService(@RequestBody String organization, ModelMap model) {
		Map<String, Object> map = null;
		Organization org = decodeService.decodeOrganization(organization);
		map = saveOrganizationService.DoSaveOrganizationIdService(org);
		String response = decodeService.encodeData(map);		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value="/saveOrganistaionCode", method = RequestMethod.POST)
	public ResponseEntity<String> saveOrganistaionCode(@RequestBody String organization, ModelMap model) {
		Map<String, Object> map = null;
		Organization org = decodeService.decodeOrganization(organization);
		map = saveOrganizationService.saveOrganistaionCode(org);
		String response = decodeService.encodeData(map);		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
}