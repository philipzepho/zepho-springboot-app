package com.blt.zepho.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.annotation.RequestBody;

import com.blt.zepho.model.Registration;
import com.blt.zepho.services.DecodeService;
import com.blt.zepho.services.RegistrationEmailValidationService;
import com.blt.zepho.util.WebKeys;

@Controller
@SessionAttributes("name")
@CrossOrigin(origins = WebKeys.ORIGIN)
public class RegistrationEmailValidationController {
	static Logger log = Logger.getLogger(RegistrationEmailValidationController.class.getName());
	
	@Autowired
	RegistrationEmailValidationService registrationEmailValidationService;
	@Autowired
	DecodeService decodeService;

	@RequestMapping(value="/mailvalidation", method = RequestMethod.POST)
	public int registerUser(@RequestBody String regString, ModelMap model) {
		int status = 0;
		Registration reg = decodeService.decodeRegistration(regString);
		status = registrationEmailValidationService.registerUser(reg);
		return status;
	}
}