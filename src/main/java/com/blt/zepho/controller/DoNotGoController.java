package com.blt.zepho.controller;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Base64;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.blt.zepho.model.AlertsFromOthers;
import com.blt.zepho.model.DoNotGo;
import com.blt.zepho.model.Registration;
import com.blt.zepho.model.UserData;
import com.blt.zepho.services.DecodeService;
import com.blt.zepho.services.DoNotGoService;
import com.blt.zepho.util.FileUploadUtil;
import com.blt.zepho.util.WebKeys;

@Controller
@SessionAttributes("name")
@CrossOrigin(origins = WebKeys.ORIGIN)
public class DoNotGoController {
	static Logger log = Logger.getLogger(DoNotGoController.class.getName());	
	@Autowired
	DoNotGoService doNotGoService;
	@Autowired
	DecodeService decodeService;

	@PreAuthorize("hasAnyAuthority('"+WebKeys.RESCUER+"', '"+WebKeys.SUPER_ADMIN+"', '"+WebKeys.MISSIONARY+"')")
	@RequestMapping(value="/alertMember", method = RequestMethod.POST)
	public ResponseEntity<String> sosRequest(@RequestBody String obj, ModelMap model) throws Exception {
		Map<String, Object> map = null;
		DoNotGo doNotGo = decodeService.decodeDoNotGo(obj);
		map = doNotGoService.sosRequest(doNotGo);
		String response = decodeService.encodeData(map);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAnyAuthority('"+WebKeys.RESCUER+"', '"+WebKeys.ADMIN+"', '"+WebKeys.MISSIONARY+"', '"+WebKeys.CHURCH_MEMBER+"')")
	@RequestMapping(value="/alertList", method = RequestMethod.POST)
	public ResponseEntity<String> alertsFromOthers(@RequestBody String userInfo, ModelMap model) throws Exception {
		Map<String, Object> map = null;
		UserData userdata = decodeService.decodeUserData(userInfo);
		map = doNotGoService.alertsFromOthers(userdata);
		String response = decodeService.encodeData(map);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('"+WebKeys.SUPER_ADMIN+"')")
	@RequestMapping(value="/loadStatesByOrg", method = RequestMethod.POST)
	public ResponseEntity<String> loadStatesByOrg(@RequestBody String org, ModelMap model) throws Exception {
		Map<String, Object> map = null;
		org = decodeService.decodeString(org);
		map = doNotGoService.loadStatesByOrg(org);
		String response = decodeService.encodeData(map);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAnyAuthority('"+WebKeys.RESCUER+"', '"+WebKeys.ADMIN+"', '"+WebKeys.MISSIONARY+"', '"+WebKeys.CHURCH_MEMBER+"')")
	@RequestMapping(value="/getImageData", method = RequestMethod.POST)
	public ResponseEntity<String> getImageData(@RequestBody String alertFromOther, ModelMap model) throws Exception {
		Map<String, Object> map = null;
		AlertsFromOthers afo = decodeService.decodeAlertsFromOthers(alertFromOther);		
		map = doNotGoService.getImageData(afo);
		String response = decodeService.encodeData(map);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAnyAuthority('"+WebKeys.SUPER_ADMIN+"', '"+WebKeys.RESCUER+"', '"+WebKeys.ADMIN+"', '"+WebKeys.MISSIONARY+"', '"+WebKeys.CHURCH_MEMBER+"')")
	@RequestMapping(value="/getNotificationCount", method = RequestMethod.POST)
	public ResponseEntity<String> getNotificationCount(@RequestBody String regInfo, ModelMap model) throws Exception {
		Map<String, Object> map = null;
		Registration userReg = decodeService.decodeRegistration(regInfo);		
		map = doNotGoService.getNotificationCount(userReg);
		String response = decodeService.encodeData(map);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
}
