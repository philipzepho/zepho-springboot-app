package com.blt.zepho.controller;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.blt.zepho.model.Registration;
import com.blt.zepho.model.Sos;
import com.blt.zepho.services.DecodeService;
import com.blt.zepho.services.RegistrationService;
import com.blt.zepho.services.SosService;
import com.blt.zepho.util.WebKeys;

@Controller
@SessionAttributes("name")
@CrossOrigin(origins = WebKeys.ORIGIN)
public class SosController {
	static Logger log = Logger.getLogger(RegistrationController.class.getName());

	@Autowired
	SosService sosService;
	@Autowired
	DecodeService decodeService;
	
	@PreAuthorize("hasAnyAuthority('"+WebKeys.ADMIN+"', '"+WebKeys.SUPER_ADMIN+"', '"+WebKeys.MASTER_ADMIN+"', "
			+ "'"+WebKeys.RESCUER+"', '"+WebKeys.MISSIONARY+"', '"+WebKeys.CHURCH_MEMBER+"')")
	@RequestMapping(value="/sos", method = RequestMethod.POST)
	public ResponseEntity<String> sosRequest(@RequestBody String sosObj, ModelMap model) throws Exception {
		Map<String, Object> map = null;
		Sos sos = decodeService.decodeSos(sosObj);
		map = sosService.sosRequest(sos);
		String response = decodeService.encodeData(map);		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
}