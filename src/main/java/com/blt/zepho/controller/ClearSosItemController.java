package com.blt.zepho.controller;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.blt.zepho.model.ClearSosListItem;
import com.blt.zepho.model.Registration;
import com.blt.zepho.services.ClearSosItemService;
import com.blt.zepho.services.DecodeService;
import com.blt.zepho.util.WebKeys;

@Controller
@SessionAttributes("name")
@CrossOrigin(origins = WebKeys.ORIGIN)
public class ClearSosItemController {	
		static Logger log = Logger.getLogger(ClearSosItemController.class.getName());
		@Autowired
		ClearSosItemService clearSosItemService;  
		@Autowired
		DecodeService decodeService; 
		
		@PreAuthorize("hasAnyAuthority('"+WebKeys.RESCUER+"','"+WebKeys.SUPER_ADMIN+"')")
		@RequestMapping(value="/watched", method = RequestMethod.POST)
		public ResponseEntity<String> registerUser(@RequestBody String clearSosListItem, ModelMap model) {
			Map<String, Object> map = null;
			 ClearSosListItem clearSosListItems  = decodeService.decodeClearSosListItem(clearSosListItem);
			map = clearSosItemService.clearSosItem(clearSosListItems);
			String response = decodeService.encodeData(map);
			return new ResponseEntity<>(response, HttpStatus.OK);
		}

}
