package com.blt.zepho.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.blt.zepho.model.NewTrip;
import com.blt.zepho.model.Registration;
import com.blt.zepho.model.UserData;
import com.blt.zepho.services.DecodeService;
import com.blt.zepho.services.LoginService;
import com.blt.zepho.services.TripService;
import com.blt.zepho.util.WebKeys;



@Controller
@SessionAttributes("name")
@CrossOrigin(origins = WebKeys.ORIGIN)
public class TripController {

	@Autowired
	TripService tripService;
	@Autowired
	DecodeService decodeService;
	
	@RequestMapping(value="/trip", method = RequestMethod.GET)
	public String missionaryTrip(ModelMap model){
		return "missionary/trip";
	}
	
	@RequestMapping(value="/trip-end", method = RequestMethod.GET)
	public String missionaryTripEnd(ModelMap model){
		return "missionary/trip-end";
	}
	
	@PreAuthorize("hasAuthority('"+WebKeys.MISSIONARY+"')")
	@RequestMapping(value="/addTrip", method = RequestMethod.POST)
	public ResponseEntity<String> newTripRequest(@RequestBody String firstTrip, ModelMap model) {
		Map<String, Object> map = null;
		NewTrip newTrip = decodeService.decodeNewTrip(firstTrip); 
		map = tripService.newTripRequest(newTrip);
		String response = decodeService.encodeData(map);		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('"+WebKeys.MISSIONARY+"')")
	@RequestMapping(value="/endTrip", method = RequestMethod.POST)
	public ResponseEntity<String> endCurrentTrip(@RequestBody String userTrip, ModelMap model) {		
		Map<String, Object> map = null;
		UserData endTrip = decodeService.decodeUserData(userTrip);
		map = tripService.endCurrentTrip(endTrip);
		String response = decodeService.encodeData(map);		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('"+WebKeys.MISSIONARY+"')")
	@RequestMapping(value="/checkRunningTrip", method = RequestMethod.POST)
	public ResponseEntity<String> checkCurrentTrip(@RequestBody String userUid, ModelMap model) {		
		Map<String, Object> map = null;
		UserData checkTrip = decodeService.decodeUserData(userUid);
		map = tripService.checkCurrentTrip(checkTrip);
		String response = decodeService.encodeData(map);		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	

}
