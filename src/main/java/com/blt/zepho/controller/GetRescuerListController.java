package com.blt.zepho.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.blt.zepho.dao.UserUidFullNameDao;
import com.blt.zepho.model.Registration;
import com.blt.zepho.model.Rescuer;
import com.blt.zepho.services.DecodeService;
import com.blt.zepho.services.GetRescuerListService;
import com.blt.zepho.util.WebKeys;

@Controller
@SessionAttributes("name")
@CrossOrigin(origins = WebKeys.ORIGIN)
public class GetRescuerListController {
	static Logger log = Logger.getLogger(GetRescuerListController.class.getName());
	@Autowired
	GetRescuerListService getRescuerListService;
	@Autowired
	DecodeService decodeService;
	
	@PreAuthorize("hasAnyAuthority('"+WebKeys.ADMIN+"', '"+WebKeys.SUPER_ADMIN+"', '"+WebKeys.MASTER_ADMIN+"')")
	@RequestMapping(value="/getRescuerList", method = RequestMethod.POST)
	public ResponseEntity<String> getRescuerList(@RequestBody String org) throws Exception {
		Map<String, Object> map = null;
		org = decodeService.decodeString(org);
		map = getRescuerListService.getRescuerList(org);
		String response = decodeService.encodeData(map);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

}
