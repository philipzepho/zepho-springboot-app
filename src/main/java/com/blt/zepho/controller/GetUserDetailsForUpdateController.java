package com.blt.zepho.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.blt.zepho.dao.UserNameUidUpdateDao;
import com.blt.zepho.dao.UserUidFullNameDao;
import com.blt.zepho.model.Registration;
import com.blt.zepho.model.Rescuer;
import com.blt.zepho.model.UidandRole;
import com.blt.zepho.model.UserData;
import com.blt.zepho.model.UserDetails;
import com.blt.zepho.services.ApprovalService;
import com.blt.zepho.services.DecodeService;
import com.blt.zepho.services.GetUserDetailsForUpdateService;
import com.blt.zepho.util.WebKeys;

@Controller
@SessionAttributes("name")
@CrossOrigin(origins = WebKeys.ORIGIN)
public class GetUserDetailsForUpdateController {
	static Logger log = Logger.getLogger(GetUserDetailsForUpdateController.class.getName());
	
	@Autowired
	GetUserDetailsForUpdateService getUserDetailsForUpdateService;
	@Autowired
	DecodeService decodeService;
	
	@RequestMapping(value="/GetUsersList", method = RequestMethod.POST)
	public ResponseEntity<String> doGetRescuerListService(@RequestBody String userData, ModelMap model) {
		Map<String, Object> map = null;
		UidandRole uidandrole = decodeService.decodeUidandRole(userData);
		map = getUserDetailsForUpdateService.doGetRescuerListService(uidandrole);
		String response = decodeService.encodeData(map);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

}


