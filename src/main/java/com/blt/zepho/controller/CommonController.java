package com.blt.zepho.controller;

import java.util.Base64;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.blt.zepho.model.OtpInfo;
import com.blt.zepho.model.Registration;
import com.blt.zepho.services.DecodeService;
import com.blt.zepho.services.LoginService;
import com.blt.zepho.services.SendOtpService;
import com.blt.zepho.util.WebKeys;
import com.google.gson.Gson;



@Controller
@SessionAttributes("name")
@CrossOrigin(origins = WebKeys.ORIGIN)
public class CommonController {
	
	@Autowired
	LoginService loginService;	
	
	@Autowired
	SendOtpService sendOtpService;
	
	@Autowired
	DecodeService decodeService;
	
	
	@RequestMapping(value="/saUsersList", method = RequestMethod.GET)
	public String saUserListPage(ModelMap model){
		return "/updated-users/sa-users-list";
	}
	
	@RequestMapping(value="/approvalList", method = RequestMethod.GET)
	public String approvalListPage(ModelMap model){
		return "/approval-list/approval-list";
	}
	
	@RequestMapping(value="/createNewAccount", method = RequestMethod.GET)
	public String createNewAccount(ModelMap model){
		return "/registration/create-new-account";
	}
	
	@RequestMapping(value="/adminUsersList", method = RequestMethod.GET)
	public String adminUsersListPage(ModelMap model){
		return "/updated-users/admin-users-list";
	}
	
	@RequestMapping(value="/adminApprovalList", method = RequestMethod.GET)
	public String adminApprovalListPage(ModelMap model){
		return "/approval-list/admin-approval-list";
	}
	
	@RequestMapping(value="/sosAlertList", method = RequestMethod.GET)
	public String sosAlertList(ModelMap model){
		return "/rescuer/sos-alert-list";
	}
	
	@RequestMapping(value="/alertMap", method = RequestMethod.GET)
	public String alertMapPage(ModelMap model){
		return "/rescuer/alert-map";
	}

	@RequestMapping(value="/verifyOTP", method = RequestMethod.POST)
	public ResponseEntity<String> verifyOTP(@RequestBody String otpInfo, ModelMap model) {
		Map<String, Object> map = null; 
		OtpInfo otpInfoObj = decodeService.decodeOtpInfo(otpInfo);
		map = sendOtpService.verifyOTP(otpInfoObj);	
		String response = decodeService.encodeData(map);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}	
	
	@RequestMapping(value="/retryOTP", method = RequestMethod.POST)
	public ResponseEntity<String> retryOTP(@RequestBody String otpInfo, ModelMap model) {
		Map<String, Object> map = null;
		OtpInfo otpInfoObj = decodeService.decodeOtpInfo(otpInfo);
		map = sendOtpService.retryOTP(otpInfoObj);
		String response = decodeService.encodeData(map);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}	
}
