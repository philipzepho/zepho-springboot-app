package com.blt.zepho.controller;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.blt.zepho.model.ItemId;
import com.blt.zepho.model.RegistrationList;
import com.blt.zepho.services.ClearDoNotGoService;
import com.blt.zepho.services.DecodeService;
import com.blt.zepho.util.WebKeys;

@Controller
@SessionAttributes("name")
@CrossOrigin(origins = WebKeys.ORIGIN)
public class ClearDoNotGoController {	

	static Logger log = Logger.getLogger(ClearDoNotGoController.class.getName());
	@Autowired
	ClearDoNotGoService clearDoNotGoService;
	@Autowired
	DecodeService decodeService; 
	
	@PreAuthorize("hasAnyAuthority('"+WebKeys.RESCUER+"','"+WebKeys.ADMIN+"',"
			+ "'"+WebKeys.MISSIONARY+"','"+WebKeys.CHURCH_MEMBER+"')")
	@RequestMapping(value="/othersWatched", method = RequestMethod.POST)
	public ResponseEntity<String>  othersWatched(@RequestBody String itemIds, ModelMap model) {
		Map<String, Object> map = null;
		ItemId itemId = decodeService.decodeItemId(itemIds);
		map = clearDoNotGoService.othersWatched(itemId);
		String response = decodeService.encodeData(map);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	   
}


