package com.blt.zepho.controller;

import java.util.Collection;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.blt.zepho.model.Registration;
import com.blt.zepho.services.DecodeService;
import com.blt.zepho.services.SosAlertListService;
import com.blt.zepho.util.WebKeys;

@Controller
@SessionAttributes("name")
@CrossOrigin(origins = WebKeys.ORIGIN)
public class SosAlertListController {
	static Logger log = Logger.getLogger(SosAlertListController.class.getName());
	
	@Autowired
	SosAlertListService sosAlertListService;
	@Autowired
	DecodeService decodeService;

	@PreAuthorize("hasAnyAuthority('"+WebKeys.RESCUER+"','"+WebKeys.SUPER_ADMIN+"')")
	@RequestMapping(value="/sosList", method = RequestMethod.POST)
	public ResponseEntity<String> sosAlertListService(@RequestBody String regString, ModelMap model) throws Exception {
		Map<String, Object> map = null;
		Collection<? extends GrantedAuthority> auth = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
		Registration reg = decodeService.decodeRegistration(regString);
		map = sosAlertListService.sosAlertListService(reg);
		String response = decodeService.encodeData(map);		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
}