package com.blt.zepho.controller;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.blt.zepho.services.DecodeService;
import com.blt.zepho.services.MasterAdminService;
import com.blt.zepho.util.WebKeys;

@Controller
@SessionAttributes("name")
@CrossOrigin(origins = WebKeys.ORIGIN)
public class MasterAdminController {
	static Logger log = Logger.getLogger(MasterAdminController.class.getName());
	@Autowired
	MasterAdminService masterAdminService;
	@Autowired
	DecodeService decodeService;
	
	@PreAuthorize("hasAuthority('"+WebKeys.MASTER_ADMIN+"')")
	@RequestMapping(value="/getSuperAdminList", method = RequestMethod.POST)
	public ResponseEntity<String> getSuperAdminList(@RequestBody String organization, ModelMap model) {
		Map<String, Object> map = null;
		organization = decodeService.decodeString(organization);
		map = masterAdminService.getSuperAdminList(organization);
		String response = decodeService.encodeData(map);		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('"+WebKeys.MASTER_ADMIN+"')")
	@RequestMapping(value="/getActivityList", method = RequestMethod.POST)
	public ResponseEntity<String> getActivityList(@RequestBody String superAdmin, ModelMap model) {
		Map<String, Object> map = null;
		superAdmin = decodeService.decodeString(superAdmin);
		map = masterAdminService.getActivityList(superAdmin);
		String response = decodeService.encodeData(map);		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
}
