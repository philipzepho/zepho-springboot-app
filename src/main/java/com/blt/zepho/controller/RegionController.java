package com.blt.zepho.controller;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.blt.zepho.model.Registration;
import com.blt.zepho.services.DecodeService;
import com.blt.zepho.services.RegionService;
import com.blt.zepho.util.WebKeys;

@Controller
@SessionAttributes("name")
@CrossOrigin(origins = WebKeys.ORIGIN)
public class RegionController {
	static Logger log = Logger.getLogger(RegionController.class.getName());
	
	@Autowired
	RegionService regionService;
	@Autowired
	DecodeService decodeService;


	@RequestMapping(value="/getStateNames", method = RequestMethod.POST)
	public ResponseEntity<String> getStateNameList(ModelMap model) {
		Map<String, Object> map = null;
		map = regionService.getStateNameList();
		String response = decodeService.encodeData(map);		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value="/getDistrictNames", method = RequestMethod.POST)
	public ResponseEntity<String> getDistrictNameList(@RequestBody String stateID, ModelMap model) {
		Map<String, Object> map = null;
		int stateIdNumber = decodeService.decodeInteger(stateID);
		map = regionService.getDistrictNameList(stateIdNumber);
		String response = decodeService.encodeData(map);		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value="/getCityNames", method = RequestMethod.POST)
	public ResponseEntity<String> getCityNameList(@RequestBody String districtId, ModelMap model) {
		Map<String, Object> map = null;
		int districtIdNumber = decodeService.decodeInteger(districtId);
		map = regionService.getCityNameList(districtIdNumber);
		String response = decodeService.encodeData(map);		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('"+WebKeys.USER_ROLE+"')")
	@RequestMapping(value="/updateUserProfile", method = RequestMethod.POST)
	public ResponseEntity<String> updateUserProfile(@RequestBody String userProfile, ModelMap model) {
		Map<String, Object> map = null;
		Registration reg = decodeService.decodeRegistration(userProfile);
		map = regionService.updateUserProfile(reg);
		String response = decodeService.encodeData(map);		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	

}