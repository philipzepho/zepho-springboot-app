package com.blt.zepho.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.blt.zepho.model.Registration;
import com.blt.zepho.services.DecodeService;
import com.blt.zepho.services.LoginService;
import com.blt.zepho.util.WebKeys;



@Controller
@SessionAttributes("name")
@CrossOrigin(origins = WebKeys.ORIGIN)
public class LoginController {
	
	@Autowired
	LoginService loginService;
	@Autowired
	DecodeService decodeService;
	
	@RequestMapping(value="/login", method = RequestMethod.GET)
	public String showLoginPage(ModelMap model){
		return "login";
	}
	
	@RequestMapping(value="/loginSubmit", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> loginSubmit(@RequestBody String regString, ModelMap model) {
		Map<String, Object> map = null;
		Registration reg = decodeService.decodeRegistration(regString);
		map = loginService.loginUser(reg);
		return new ResponseEntity<>(map, HttpStatus.OK);
	}
	
	@RequestMapping(value="/verifyUser", method = RequestMethod.POST)
	public ResponseEntity<String> verifyUser(@RequestBody String regString, ModelMap model) {
		Map<String, Object> map = null;
		Registration reg = decodeService.decodeRegistration(regString);
		map = loginService.verifyUser(reg);
		String response = decodeService.encodeData(map);		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value="/resetPassword", method = RequestMethod.POST)
	public ResponseEntity<String> resetPassword(@RequestBody String regString, ModelMap model) {
		Map<String, Object> map = null;
		Registration reg = decodeService.decodeRegistration(regString);
		map = loginService.resetPassword(reg);
		String response = decodeService.encodeData(map);		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value="/verifySecurityQuestions", method = RequestMethod.POST)
	public ResponseEntity<String> verifySecurityQuestions(@RequestBody String regString, ModelMap model) {
		Map<String, Object> map = null;
		Registration reg = decodeService.decodeRegistration(regString);
		map = loginService.verifySecurityQuestions(reg);
		String response = decodeService.encodeData(map);		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value="/superAdmin", method = RequestMethod.GET)
	public String showSUPage(ModelMap model){
		return "super-admin/super-admin";
	}
	
	@RequestMapping(value="/missionary", method = RequestMethod.GET)
	public String showMissionaryPage(ModelMap model){
		return "missionary/missionary";
	}
	
	@RequestMapping(value="/admin", method = RequestMethod.GET)
	public String showAdminPage(ModelMap model){
		return "admin/admin";
	}
	
	@RequestMapping(value="/rescuer", method = RequestMethod.GET)
	public String showRescuerPage(ModelMap model){
		return "rescuer/rescuer";
	}	
	
	@RequestMapping(value="/masterAdmin", method = RequestMethod.GET)
	public String showMasterAdminPage(ModelMap model){
		return "master-admin/master-admin";
	}
	
	@RequestMapping(value="/privacy", method = RequestMethod.GET)
	public String privacy(ModelMap model){
		return "privacy";
	}

}
