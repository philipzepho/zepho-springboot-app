package com.blt.zepho.controller;


import java.util.Base64;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.blt.zepho.model.Registration;
import com.blt.zepho.services.DecodeService;
import com.blt.zepho.services.RegistrationService;
import com.blt.zepho.util.WebKeys;

@Controller
@SessionAttributes("name")
@CrossOrigin(origins = WebKeys.ORIGIN)
public class RegistrationController {
	static Logger log = Logger.getLogger(RegistrationController.class.getName());

	@Autowired
	RegistrationService registrationService;
	@Autowired
	DecodeService decodeService;

	@PreAuthorize("hasAnyAuthority('"+WebKeys.ADMIN+"', '"+WebKeys.SUPER_ADMIN+"', '"+WebKeys.MASTER_ADMIN+"')")
	@RequestMapping(value="/registration", method = RequestMethod.POST)
	public ResponseEntity<String> registerUserz(@RequestBody String regString, ModelMap model) {
		Map<String, Object> map = null;
		Registration reg = decodeService.decodeRegistration(regString);
		map = registrationService.registerUserz(reg);
		String response = decodeService.encodeData(map);		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
		
	@RequestMapping(value="/publicRegistration", method = RequestMethod.POST)
	public ResponseEntity<String> publicRegistration(@RequestBody String regString, ModelMap model) {
		Map<String, Object> map = null;
		Registration reg = decodeService.decodeRegistration(regString);
		map = registrationService.publicRegistration(reg);
		String response = decodeService.encodeData(map);		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value="/checkMobileNumberExists", method = RequestMethod.POST)
	public ResponseEntity<String> checkMobileNumberExists(@RequestBody String phNum, ModelMap model) {
		Map<String, Object> map = null;
		phNum = decodeService.decodeString(phNum);
		map = registrationService.checkMobileNumberExists(phNum);
		/*Encode Response data*/
		String response = decodeService.encodeData(map);		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('"+WebKeys.USER_ROLE+"')")
	@RequestMapping(value="/getUserDataByUID", method = RequestMethod.POST)
	public ResponseEntity<String> getUserDataByUID(@RequestBody String UID, ModelMap model) {
		Map<String, Object> map = null;
		UID = decodeService.decodeString(UID);
		map = registrationService.getUserDataByUID(UID);
		String response = decodeService.encodeData(map);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('"+WebKeys.USER_ROLE+"')")
	@RequestMapping(value="/saveVerificationInfo", method = RequestMethod.POST)
	public ResponseEntity<String> saveVerificationInfo(@RequestBody String regString, ModelMap model) {
		Map<String, Object> map = null;
		Registration reg = decodeService.decodeRegistration(regString);
		map = registrationService.saveVerificationInfo(reg);
		String response = decodeService.encodeData(map);		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('"+WebKeys.USER_ROLE+"')")
	@RequestMapping(value="/saveConfirmationInfo", method = RequestMethod.POST)
	public ResponseEntity<String> saveConfirmationInfo(@RequestBody String regString, ModelMap model) {
		Map<String, Object> map = null;
		Registration reg = decodeService.decodeRegistration(regString);
		map = registrationService.saveConfirmationInfo(reg);
		String response = decodeService.encodeData(map);		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('"+WebKeys.USER_ROLE+"')")
	@RequestMapping(value="/updateEmailVerify", method = RequestMethod.POST)
	public ResponseEntity<String> updateEmailVerify(@RequestBody String regString, ModelMap model) {
		Map<String, Object> map = null;
		Registration reg = decodeService.decodeRegistration(regString);
		map = registrationService.updateEmailVerify(reg);
		String response = decodeService.encodeData(map);		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value="/checkEmailExists", method = RequestMethod.POST)
	public ResponseEntity<String> checkEmailExists(@RequestBody String email, ModelMap model) {
		Map<String, Object> map = null;
		email = decodeService.decodeString(email);
		map = registrationService.checkEmailExists(email);
		/*Encode Response data*/
		String response = decodeService.encodeData(map);		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

}