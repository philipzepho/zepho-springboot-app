package com.blt.zepho.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.blt.zepho.dao.ApprovalDao;
import com.blt.zepho.dao.SaveDeviceIdDao;
import com.blt.zepho.model.NewTrip;
import com.blt.zepho.model.Registration;
import com.blt.zepho.model.RegistrationList;
import com.blt.zepho.services.ApprovalService;
import com.blt.zepho.services.DecodeService;
import com.blt.zepho.services.SosService;
import com.blt.zepho.util.WebKeys;

@Controller
@SessionAttributes("name")
@CrossOrigin(origins = WebKeys.ORIGIN)
public class ApprovalController {
	static Logger log = Logger.getLogger(ApprovalController.class.getName());
	@Autowired
	ApprovalService approvalService;
	@Autowired
	DecodeService decodeService; 
	
	@PreAuthorize("hasAnyAuthority('"+WebKeys.ADMIN+"', '"+WebKeys.SUPER_ADMIN+"', '"+WebKeys.MASTER_ADMIN+"')")
	@RequestMapping(value="/doApprovalService", method = RequestMethod.POST)
	public ResponseEntity<String> doApprovalService(@RequestBody String regString, ModelMap model) {
		Map<String, Object> map = null;
		Registration reg = decodeService.decodeRegistration(regString);
		map = approvalService.doApprovalService(reg);
		String response = decodeService.encodeData(map);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
		
	@PreAuthorize("hasAnyAuthority('"+WebKeys.ADMIN+"', '"+WebKeys.SUPER_ADMIN+"', '"+WebKeys.MASTER_ADMIN+"')")
	@RequestMapping(value="/approveUser", method = RequestMethod.POST)
	public ResponseEntity<String> doApprove(@RequestBody String regString, ModelMap model) {
		Map<String, Object> map = null;
		Registration reg = decodeService.decodeRegistration(regString);
		map = approvalService.doApprove(reg);
		String response = decodeService.encodeData(map);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAnyAuthority('"+WebKeys.ADMIN+"', '"+WebKeys.SUPER_ADMIN+"', '"+WebKeys.MASTER_ADMIN+"')")
	@RequestMapping(value="/rejectUser", method = RequestMethod.POST)
	public ResponseEntity<String> doReject(@RequestBody String regString, ModelMap model) {
		Map<String, Object> map = null;
		Registration reg = decodeService.decodeRegistration(regString);
		map = approvalService.doReject(reg);
		String response = decodeService.encodeData(map);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('"+WebKeys.SUPER_ADMIN+"')")
	@RequestMapping(value="/approveAll", method = RequestMethod.POST)
	public ResponseEntity<String>approveAll(@RequestBody String registrations, ModelMap model) {
		Map<String, Object> map = null;
		RegistrationList registration = decodeService.decodeRegistrationList(registrations);
		map = approvalService.approveAll(registration);
		String response = decodeService.encodeData(map);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

}