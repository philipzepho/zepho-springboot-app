package com.blt.zepho.controller;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.blt.zepho.services.DecodeService;
import com.blt.zepho.services.GetOrganizationListService;
import com.blt.zepho.util.WebKeys;

@Controller
@SessionAttributes("name")
@CrossOrigin(origins = WebKeys.ORIGIN)
public class GetOrganizationListController {
	static Logger log = Logger.getLogger(GetOrganizationListController.class.getName());
	@Autowired
	GetOrganizationListService  getOrganizationListService;
	@Autowired
	DecodeService decodeService;
	
	@RequestMapping(value="/organizations", method = RequestMethod.POST)
	public ResponseEntity<String> GetOrganizationList() throws Exception {
		Map<String, Object> map = null;
		map = getOrganizationListService.GetOrganizationList();
		String response = decodeService.encodeData(map);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	

}
