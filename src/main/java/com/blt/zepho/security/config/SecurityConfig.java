package com.blt.zepho.security.config;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.firewall.DefaultHttpFirewall;
import org.springframework.security.web.firewall.HttpFirewall;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import com.blt.zepho.security.filter.StatelessAuthenticationFilter;
import com.blt.zepho.security.filter.StatelessLoginFilter;
import com.blt.zepho.security.service.TokenAuthenticationService;
import com.blt.zepho.security.service.UserServiceImpl;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@Order(1)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserServiceImpl userService;

	@Autowired
	private TokenAuthenticationService tokenAuthenticationService;

	public SecurityConfig() {
		//super(true);
		
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		//http.headers().frameOptions().sameOrigin();
		
		http.csrf().disable();
		http.cors();
		http.exceptionHandling().
				and().anonymous().
				and().servletApi().
				and().headers().cacheControl();

		http.authorizeRequests()
		.antMatchers("/bootstrap/**","/css/**", "/js/**", "/assets/**", "**/favicon.ico").anonymous()
		.antMatchers("/login", "/logout", "/error", "/checkMobileNumberExists", "/verifyOTP", 
				"/verifyUser", "/verifySecurityQuestions", "/organizations", "/publicRegistration",
				"/retryOTP", "/resetPassword", "/getStateNames", "/getDistrictNames", "/getCityNames","/checkEmailExists", "/privacy").permitAll()  
		.antMatchers(HttpMethod.GET, "/**").permitAll()
		.antMatchers("/**").authenticated();

		http.addFilterBefore(
				new StatelessLoginFilter("/loginSubmit", tokenAuthenticationService, userService, authenticationManager()),
				UsernamePasswordAuthenticationFilter.class);

		http.addFilterBefore(new StatelessAuthenticationFilter(tokenAuthenticationService),
				UsernamePasswordAuthenticationFilter.class);
	}

	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userService).passwordEncoder(new BCryptPasswordEncoder());
	}

	@Override
	protected UserDetailsService userDetailsService() {
		return userService;
	}
	
	@Bean
	public HttpFirewall defaultHttpFirewall() {
	   return new DefaultHttpFirewall();
	}
	
	@Bean 
	CorsConfigurationSource corsConfigurationSource() { 
		CorsConfiguration config = new CorsConfiguration();
		config.setAllowCredentials(true);
		config.addAllowedOrigin("*");
		config.addAllowedHeader("*");
		config.addAllowedMethod("*");
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource(); 
		source.registerCorsConfiguration("/**", config);
		return source; 
	}

}