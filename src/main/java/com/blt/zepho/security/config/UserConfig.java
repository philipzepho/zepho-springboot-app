package com.blt.zepho.security.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

@Configuration
@PropertySource("classpath:security.properties")
public class UserConfig {
	
	 @Autowired
	 private Environment env;
	 
	 public String getUserPassword(String username){
		 return env.getProperty(username);
	 }

}
