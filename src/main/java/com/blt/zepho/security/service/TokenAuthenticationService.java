package com.blt.zepho.security.service;

import java.io.IOException;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.blt.zepho.dao.LoginDao;
import com.blt.zepho.model.Registration;
import com.blt.zepho.services.DecodeService;
import com.blt.zepho.services.LoginService;
import com.blt.zepho.util.WebKeys;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class TokenAuthenticationService {

    private static final String AUTH_HEADER_NAME = "x-auth-token";
    private static final String USER_PERMISSIONS = "permissions";
    private static final String DATA = "data";
    private static final String ADMIN="admin";
    private static final String EDIT = "edit";
    private static final String READONLY = "readonly";

    private final JwtTokenHandler jwtTokenHandler;  
    
    @Autowired
	LoginService loginService;
    @Autowired
   	LoginDao loginDao;
    @Autowired
	DecodeService decodeService;

    @Autowired
    public TokenAuthenticationService(JwtTokenHandler jwtTokenHandler) {
        this.jwtTokenHandler = jwtTokenHandler;        
    }

    public void addJwtTokenToHeader(HttpServletResponse response,
                             UserAuthentication authentication) {
        final UserDetails user = authentication.getDetails();    
        
        Map<String, Object> map = null;
        ObjectMapper mapperObj = new ObjectMapper();
        String jsonResp = null;
        Registration reg = new Registration();
        reg.setEmail(user.getUsername());
        reg.setPassword(user.getPassword());
		map = loginService.loginUser(reg);
		String errorMessage = (String) map.get("ErrorMessage");
		if(errorMessage !=  WebKeys.MAX_ATTEMPTS){
			response.addHeader(AUTH_HEADER_NAME, jwtTokenHandler.createTokenForUser(user));
		}else{
			response.addHeader(AUTH_HEADER_NAME, "");
		}
		
		try {
            jsonResp = decodeService.encodeData(map);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
		response.addHeader(DATA, jsonResp);
    }

    public Authentication generateAuthenticationFromRequest(HttpServletRequest request) {
        final String token = request.getHeader(AUTH_HEADER_NAME);
        if (token == null || token.isEmpty()) return null;
        return jwtTokenHandler
                .parseUserFromToken(token)
                .map(UserAuthentication::new)
                .orElse(null);
    }
}
