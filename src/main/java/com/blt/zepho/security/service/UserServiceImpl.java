package com.blt.zepho.security.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.blt.zepho.dao.LoginDao;
import com.blt.zepho.model.Registration;
import com.blt.zepho.model.SSUser;
import com.blt.zepho.security.config.UserConfig;
import com.blt.zepho.services.LoginService;
import com.blt.zepho.util.WebKeys;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserConfig userConfig;
	
	@Autowired
	LoginDao loginDao;
	
	@Autowired
	LoginService loginService;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		SSUser user = new SSUser();
		
		Registration resreg = loginDao.loginAuthByUserName(username);
		if (resreg != null) {
			user.setUsername(resreg.getEmail());
			user.setPassword(resreg.getPassword());
			if(resreg.getIsApproved())
				user.setRoleName(resreg.getRole());
			else
				user.setRoleName(WebKeys.USER_ROLE);
		}
		
		UserDetails userDetails = new User(user.getUsername(), user.getPassword(), user.isEnabled(), true, true, true,
				user.getAuthorities());

		return userDetails;
	}

	@Override
	public void updateLoginAttempts(String username) {
		if(username != null && username != ""){
			loginDao.updateLoginAttempts(username);
		}
		
	}

}
