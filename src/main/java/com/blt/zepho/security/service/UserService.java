package com.blt.zepho.security.service;





import java.util.Optional;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.blt.zepho.model.SSUser;

public interface UserService extends org.springframework.security.core.userdetails.UserDetailsService {

    //User update(User user, UserDTO params);
    //Optional<SSUser> findUser(Long id);
    //User createUser(UserDTO userDTO);
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException;
    public void updateLoginAttempts(String username);
}