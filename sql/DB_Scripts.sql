-- Added by Santosh on 26-01-2018

ALTER TABLE `zepho`.`registration` 
	ADD COLUMN `dob` DATE NULL DEFAULT NULL;

-- Added by Santosh on 29-01-2018
ALTER TABLE `zepho`.`registration` 
	ADD COLUMN `emailVerified` TINYINT NULL DEFAULT 1;

-- Added By Judah on 30-01-2018
CREATE TABLE `zepho`.`states` (
 `state_id` INT NOT NULL AUTO_INCREMENT,
 `state_name` VARCHAR(45) NULL,
 PRIMARY KEY (`state_id`));

CREATE TABLE `zepho`.`district` (
 `district_id` INT NOT NULL AUTO_INCREMENT,
 `district_name` VARCHAR(45) NULL,
 `state_id` INT NOT NULL,
 PRIMARY KEY (`district_id`),
 INDEX `state_idFK_idx` (`state_id` ASC),
 CONSTRAINT `state_idFK`
   FOREIGN KEY (`state_id`)
   REFERENCES `zepho`.`states` (`state_id`)
   ON DELETE NO ACTION
   ON UPDATE NO ACTION);

CREATE TABLE `zepho`.`city` (
 `city_id` INT NOT NULL AUTO_INCREMENT,
 `city_name` VARCHAR(45) NULL,
 `district_id` INT NULL,
 PRIMARY KEY (`city_id`),
 INDEX `district_idFK_idx` (`district_id` ASC),
 CONSTRAINT `district_idFK`
   FOREIGN KEY (`district_id`)
   REFERENCES `zepho`.`district` (`district_id`)
   ON DELETE NO ACTION
   ON UPDATE NO ACTION);
   
   -- Added by Santosh on 31-01-2018
ALTER TABLE `zepho`.`registration` 
	ADD COLUMN `fatherName` VARCHAR(45) NULL,
	ADD COLUMN `primaryRelation` VARCHAR(45) NULL,
	ADD COLUMN `secondaryRelation` VARCHAR(45) NULL,	
	ADD COLUMN `willingToHelp` VARCHAR(45) NULL;
	
ALTER TABLE `zepho`.`organizations` 
	ADD COLUMN `inviteCode` VARCHAR(45) NULL;
	
update `zepho`.`organizations` set `inviteCode`='ZIM857' where `orgShortName` = 'IEM';

update `zepho`.`organizations` set `inviteCode`='ZPR354' where `orgShortName` = 'PR';

update `zepho`.`organizations` set `inviteCode`='ZIF453' where `orgShortName` = 'IFM';

update `zepho`.`organizations` set `inviteCode`='ZRZ527' where `orgShortName` = 'RZIM';

-- Added by Judah o 02-02-2018
ALTER TABLE `zepho`.`registration` 
ADD COLUMN `approveByAdminUid` VARCHAR(45) NULL AFTER `approvedByAdmin`;


-- Added by Santosh on 02-02-2018
ALTER TABLE `zepho`.`registration` 
	ADD COLUMN `completeReg` TINYINT NULL DEFAULT 1;
	
-- Added by Judah o 13-04-2018	
ALTER TABLE `zepho`.`organizations` 
ADD COLUMN `orgId` INT(11) NOT NULL AUTO_INCREMENT FIRST,
ADD PRIMARY KEY (`orgId`),
ADD UNIQUE INDEX `orgId_UNIQUE` (`orgId` ASC);
	
-- Added by Christopher on 08-08-2018	
CREATE TABLE `user_log` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `previous_val` varchar(100) DEFAULT NULL,
  `updated_val` varchar(100) DEFAULT NULL,
  `module_name` varchar(100) DEFAULT NULL,
  `status` varchar(45) NOT NULL,
  `comment` varchar(500) DEFAULT NULL,
  `changed_by` varchar(100) NOT NULL,
  `previous_date` datetime DEFAULT NULL,
  `changed_date` datetime NOT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--Added by santosh on 5-12-2018
ALTER TABLE `zepho`.`registration` 
	ADD COLUMN `loginAttempts` INT(11) NULL DEFAULT 0;

--Added by santosh on 13-12-2018
ALTER TABLE `zepho`.`alertsfromothers` 
	ADD COLUMN `attachmentPath` TEXT NULL;

